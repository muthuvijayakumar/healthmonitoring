import React, { useState } from "react";
import { Tabs, Tab, AppBar } from "@material-ui/core";
import { MuiThemeProvider, createMuiTheme } from "@material-ui/core/styles";
import { HistoryCard } from "./HistoryCard";

export const TopNavBar = (props) => {
  const [value, setValue] = useState(0);
  const handleTabs = (e, val) => {
    setValue(val);
    props.SetItemValue(val);
  };

  const theme = createMuiTheme({
    palette: {
      secondary: {
        main: "#4D59FC",
      },
    },
  });

  return (
    <div>
      <MuiThemeProvider theme={theme}>
        <AppBar style={{ backgroundColor: "white" }} position="static">
          <Tabs
            indicatorColor="secondary"
            textColor="secondary"
            value={value}
            onChange={handleTabs}
            centered
            variant="fullWidth"
          >
            <Tab
              style={{
                fontFamily: "Inter",
                fontStyle: "bold",
                fontWeight: "600",
                fontSize: "13px",
                textTransform: "none",
              }}
              label={props.item1}
            ></Tab>
            <Tab
              style={{
                fontFamily: "Inter",
                fontStyle: "bold",
                fontWeight: "600",
                fontSize: "13px",
                textTransform: "none",
              }}
              label={props.item2}
            ></Tab>
            <Tab
              style={{
                fontFamily: "Inter",
                fontStyle: "bold",
                fontWeight: "600",
                fontSize: "13px",
                textTransform: "none",
              }}
              label={props.item3}
            ></Tab>
          </Tabs>
        </AppBar>
      </MuiThemeProvider>
    </div>
  );
};

function TabPanel(props) {
  const { data, value, index } = props;
  return (
    <div>
      {value === index && (
        <div>
          {data
            ? data.map((el) => (
                <HistoryCard
                  key={el.id}
                  status={el.Status}
                  userName={el.Name}
                  report={el.Report}
                ></HistoryCard>
              ))
            : "No data to show"}
        </div>
      )}
    </div>
  );
}
