import React from "react";
import { IMAGES } from "../utils/constants";
import "./HistoryCard.css";
import { useDispatch, useSelector } from "react-redux";
import { useHistory } from "react-router";

export function HistoryCard(props) {
  const roles = useSelector((state) => state.roles);

  return (
    <div
      style={{
        marginTop: 2,
        width: "100%",
        height: "16%",
        display: "flex",
        flexDirection: "row",
        backgroundColor: "white",
        borderBottom: "0.5px solid #E8EDF1",
      }}
    >
      <div
        onClick={props.onPress}
        style={{
          display: "flex",
          flexDirection: "row",
          width: "75%",
          height: "16%",
        }}
      >
        {props.status === "true" ? (
          <img
            style={{
              width: "16px",
              height: "16px",
              alignSelf: "center",
              float: "left",
              padding: "5px",
            }}
            src={IMAGES.GREYIndicator}
          />
        ) : (
          <img
            style={{
              width: "16px",
              height: "16px",
              alignSelf: "center",
              float: "left",
              padding: "5px",
            }}
            src={IMAGES.GREYIndicator}
          />
        )}

        <div
          style={{
            display: "flex",
            flexDirection: "column",
            width: "70%",
            height: "16%",
            padding: "10px 0px",
          }}
        >
          <div className="wb_evnt1">{props.userName}</div>
          <div className="wb_evnt2">{props.report}</div>
        </div>
      </div>

      {props.CheckInShow ? (
        roles.includes("HRBP") ? (
          <div onClick={props.CheckIn} className="whitelistbox_ctnt3">
            <span className="check_edit_btn float_r">Check-in</span>
          </div>
        ) : (
          <div>{null}</div>
        )
      ) : (
        <div>{null}</div>
      )}
    </div>
  );
}
