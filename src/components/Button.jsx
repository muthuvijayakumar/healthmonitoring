import React from "react";
import "./button.css";


const STYLES=[
    "btn--primary--solid",
    "btn--warning--solid",
    "btn--danger--solid",
    "btn--success--solid",
    "btn--primary--outline",
    "btn--warning--outline",
    "btn--danger--outline",
    "btn--success--outline",
];

const SIZES=["btn--medium","btn--small"];

export const Button=({children,type,onClick,buttonStyle,buttonSize})=>{


    const checkBtnStyle=STYLES.includes(buttonStyle) ? buttonStyle : STYLES[0];
    const checkBtnSize=SIZES.includes(buttonSize) ? buttonSize : SIZES[0];

    return(
        <button 
            className={`btn ${checkBtnStyle} ${checkBtnSize}`}         
            onClick={onClick}
            style={{
                backgroundColor:"#4D59FC",
                color:"#fff",
                fontFamily:"Inter",
                fontStyle:"normal",
                fontWeight:"600px",
                width:"100%",
                height:"44px",
                fontSize:"15px",
                lineHeight:"18px",
                justifyContent:"center",
                alignContent:"center",
                alignItems:"center",
                borderRadius:"5px"
            }}
           
            type={type}> {children}</button>
    )


}

export const Button1=({children,type,onClick,buttonStyle,buttonSize})=>{


    const checkBtnStyle=STYLES.includes(buttonStyle) ? buttonStyle : STYLES[0];
    const checkBtnSize=SIZES.includes(buttonSize) ? buttonSize : SIZES[0];

    return(
        <button 
            className={`btn ${checkBtnStyle} ${checkBtnSize}`}         
            onClick={onClick}
            style={{
                backgroundColor:"#4D59FC",
                color:"#fff",
                fontFamily:"Inter",
                fontStyle:"normal",
                fontWeight:"600px",
                width:"100%",
                height:"44px",
                fontSize:"15px",
                lineHeight:"18px",
                justifyContent:"center",
                alignContent:"center",
                alignItems:"center",
                borderRadius:"20px"
            }}
           
            type={type}> {children}</button>
    )


}