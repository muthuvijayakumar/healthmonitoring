import React, { useState, useEffect } from "react";
import "./Notes.css";

export function Notes(props) {
  const [MAX_LENGTH, setMAX_LENGTH] = useState(25);

  function OnClickCard() {
   // props.onPress();

    if (MAX_LENGTH === 25) {
      setMAX_LENGTH(500);
    } else {
      setMAX_LENGTH(25);
    }
  }

  useEffect(() => {}, [MAX_LENGTH]);

  return (
    <div
      style={{
        width: "94%",
        height: "16%",
        display: "flex",
        flexDirection: "row",
        backgroundColor: "white",
        borderBottom: "0.5px solid #E8EDF1",
        padding: "5px 3%",
      }}
    >
      <div
        onClick={() => {
          OnClickCard();
        }}
        style={{
          display: "flex",
          flexDirection: "row",
          width: "85%",
          padding: "5px 0px",
          height: "16%",
        }}
      >
        <div
          style={{
            display: "flex",
            flexDirection: "column",
            width: "100%",
            height: "16%",
           
          }}
        >
          <div className="wb_evnt1">{props.header}</div>
          {props.description.length > MAX_LENGTH ? (
            <div className="wb_evnt2">{`${props.description.substring(
              0,
              MAX_LENGTH
            )}...`}</div>
          ) : (
            <div className="wb_evnt2">{props.description}</div>
          )}
        </div>
      </div>

      {MAX_LENGTH === 500 ? (
        <div onClick={props.onEditClick} className="editBtnContainer">
          <span className="notes_edit_btn float_r">Edit</span>
        </div>
      ) : (
        <div>{null}</div>
      )}
    </div>
  );
}
