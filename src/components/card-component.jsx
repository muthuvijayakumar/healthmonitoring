import React from "react";
import "./card-component.css";
export const Card = (props) => (
  <div className="column" style={{ backgroundColor: props.backgroundColor }}>
    <div>
      <div
        style={{
          fontSize: "20px",
          color: "#1A1C22",
          fontFamily: "Inter",
          fontStyle: "normal",
          fontWeight: "800",
        }}
      >
        {props.value}
      </div>
      <div
        style={{
          fontSize: "11px",
          height: "30px",
          lineHeight: "16.5px",
          paddingTop: "5px",
          fontWeight: "400",
          fontStyle: "normal",
          fontFamily: "Inter",
        }}
      >
        {props.label}
      </div>
    </div>
  </div>

  // <div
  //   style={{
  //     width: "98%",
  //     height: "100px",
  //     backgroundColor: props.backgroundColor,
  //     borderRadius: "5px",

  //   }}
  // >
  // <div  style={{
  //     width: "110px",
  //     height: "100px",
  //     display: "flex",
  //     flexDirection: "column",
  //     justifyContent: "center",
  //     alignItems: "center",
  //   }}>
  //   <h3>{props.value}</h3>
  //   <h4 style={{textAlign:"center"}}>{props.label}</h4></div>

  // </div>
);
