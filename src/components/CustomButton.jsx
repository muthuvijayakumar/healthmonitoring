import React from "react";
import "./button.css";

export const CustomButton = ({
  children,
  type,
  onPress,
  backgroundColor,
  textColor,
  buttonDisability,
}) => {
  return (
    <button
      onClick={onPress}
      style={{
        backgroundColor: backgroundColor,
        color: textColor,
        fontFamily: "Inter",
        fontStyle: "normal",
        fontWeight: "600",
        width: "100%",
        height: "44px",
        fontSize: "15px",
        lineHeight: "18px",
        justifyContent: "center",
        alignContent: "center",
        alignItems: "center",
        borderRadius: "5px",
        borderColor: "transparent",
      }}
      type={type}
      disabled={buttonDisability}
    >
      {" "}
      {children}
    </button>
  );
};
