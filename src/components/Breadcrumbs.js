import React from "react";
import { Link } from "react-router-dom";
import useBreadcrumbs from "use-react-router-breadcrumbs";
import "./Breadcrumbs.css";

const Breadcrumbs = () => {
  const routes = [{ path: '/', breadcrumb: null }];
  const breadcrumbs = useBreadcrumbs(routes);

  return (
    <div className="breadcrumbs">
      {breadcrumbs.map(({ breadcrumb, match }, index) => (
        <div className="bc" key={match.url}>
          <Link className="linking" to={match.url || ""}>
            <div className="text"> 
              {breadcrumb}
              <span style={{color:"black",margin:"5px"}}>{index < breadcrumbs.length - 1 && ">"}
              {''}</span>
            </div>
          </Link>
        </div>
      ))}
    </div>
  );
};

export default Breadcrumbs;