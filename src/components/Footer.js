import React from "react";


const Footer=()=>{

    return(
        <div style={{textAlign:"center",alignItems:"center",justifyContent:"center"}}>
            <h5 style={{color:"#000000",
                fontFamily:"Inter", 
                fontStyle:"normal",
                fontWeight:"normal",
                fontSize:"13px",
                backgroundColor:"#fff",
                padding:"10px 0px"
                }}>
                &copy; 2021 Altimetrik Corp.
            </h5>
        </div>
    )

}

export default Footer;