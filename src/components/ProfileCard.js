import React from "react";
import "./ProfileCard.css";

export const ProfileCard = (props) => (
    <div >
      <div className="label_head pad-t10">Health Assessment of</div>
    <div className="nopad_whitebox bg_white">
      <div className="wb_evnt1">
        {props.Header}
      </div>
      <div className="wb_evnt2 pad-t5">
        {props.subHeader}
      </div>
    </div>
  </div>  
);

export const ProfileCardSelection = (props) => (
  <div className="column2"style={{backgroundColor:"white"}}>
  <div style={{paddingLeft:"25px"}}>
    <div
      style={{
        fontSize: "20px",
        color: "#1A1C22",
        fontFamily: "Inter",
        fontStyle: "normal",
        fontWeight: "800",
      }}
    >
      Hi
    </div>
    <div
      style={{
        fontSize: "11px",
        lineHeight: "16.5px",
        fontWeight: "400",
        fontStyle: "normal",
        fontFamily: "Inter",
      }}
    >
      Hello
    </div>
    <div
        style={{
          fontSize: "11px",
          lineHeight: "16.5px",
          fontWeight: "400",
          fontStyle: "normal",
          fontFamily: "Inter",
        }}
      >
        Date
      </div>
  </div>
</div>
);