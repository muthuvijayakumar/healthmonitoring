import React, { useState, useEffect } from "react";
import "./Notes.css";
import Modal from "react-modal";
Modal.setAppElement("#root");
import { STRING, IMAGES, COLORS } from "../utils/constants";
import { Button } from "../components/Button";

export function AddNotes(props) {
  function OnClickAdd() {
    props.onClickAdd();
    props.onclickClose();
  }

  const customStyles = {
    content: {
      top: "80%",
      left: "50%",
      right: "auto",
      bottom: "-40%",
      transform: "translate(-50%, -50%)",
      borderRadius: "10px",
      width: "82%",
    },
  };

  return (
    <Modal
      isOpen={props.modalOpen}
      //onAfterOpen={afterOpenModal}
      //onRequestClose={CloseNewNoteModal}
      style={customStyles}
      shouldCloseOnOverlayClick={false}
    >
      <div id="contactInfo">
        <div className="backbtn_block">
          <div className="backbtn_txt">Add New Note</div>
          <div className="float_r">
            <img
              onClick={props.onclickClose}
              src="/images/CloseBtn.svg"
              alt="back btn"
            />
          </div>
        </div>

        <div
            style={{
              margin:"20px 0px",
              height: "0px",
              border: "0.5px solid rgba(0, 0, 0, 0.1)",
              marginLeft: "-16px",
              marginRight: "-16px",
            }}>
        </div>

        <div>
          <textarea
            style={{
              borderWidth: 1,
              borderRadius: 3,
              fontSize: "15px",
              backgroundColor: COLORS.WHITE,
              paddingLeft: "15px",
              width: "98%",
              height: "157px",
              marginBottom: "20px",
              textAlign: "start",
            }}
            maxLength={500}
            multiple={true}
            //value={currentLoc}
            placeholder="Enter Notes here"
            type="text"
            onChange={props.getNotesDescription}
          ></textarea>
        </div>

        <Button onClick={OnClickAdd} children="Add"></Button>
      </div>
    </Modal>
  );
}
