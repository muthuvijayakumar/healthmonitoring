import React, { Component } from "react";
import Switch from "react-switch";

export default class ToggleSwitch extends Component {
  constructor() {
    super();
    // this.state = { checked: false };
    // this.handleChange = this.handleChange.bind(this);
  }

//   handleChange(checked) {
//     this.setState({ checked });
//   }

  render() {
    return (
      <div style={{
          display : "flex",
          flexDirection : "row",
          backgroundColor : "white",
          height : "44px"
      }}> 
          <span 
          style={{
              fontFamily : "Inter",
              fontWeight : "500",
              fontStyle : "normal",
              fontSize : "17px",
              lineHeight : "20px",
              color : "#252A31",
              paddingLeft: "15px",
              paddingTop : "12px"
          }}>Family Members</span>
          <div style={{
              paddingTop : "8px",
              paddingLeft : "430px"
          }}>
          <Switch
            checked={this.props.checked}
            id={this.props.id}
            onChange={this.props.onChange}
            onColor="#4D59FC"
            onHandleColor="#FFFFFF"
            handleDiameter={30}
            uncheckedIcon={false}
            checkedIcon={false}
            boxShadow="0px 1px 5px rgba(0, 0, 0, 0.6)"
            activeBoxShadow="rgba(0, 0, 0, 0.2)"
            height={32}
            width={52}
            className="toggleSwitch"
          />
          </div>
      </div>
    )
  }
}