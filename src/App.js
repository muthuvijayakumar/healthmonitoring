import React from "react";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import BasicInformationScreen from "./screens/BasicInformationScreen";
import QuizScreen from "./screens/QuizScreen";
import WelcomeScreen from "./screens/WelcomeScreen";
import CategorySelection from "./screens/CategorySelection";
import ThankYouScreen from "./screens/ThankYouScreen";
import HelpDeskScreen from "./screens/HelpDeskScreen";
import CoronaPolicyScreen from "./screens/CoronaPolicyScreen";
import DashboardHomeScreen from "./screens/DashboardHomeScreen";
import Dashboard_HealthAssessment from "./screens/Dashboard_HealthAssessment";
import UserProfileDashboard from "./screens/UserProfileDashboard";
import HealthAlertScreen from "./screens/HealthAlertScreen";
import DashboardSumary from "./screens/DashboardSumary";
import GeneralInsuranceScreen from "./screens/GeneralInsuranceScreen";
import RtpcrScreen from "./screens/RtpcrScreen";
import QuizSummary from "./screens/QuizSummary";
import { useDispatch, useSelector } from "react-redux";
import DashboardGeneralInfo from "./screens/DashboardGeneralInfo";
import DashboardContactInfo from "./screens/DashboardContactInfo";
import Breadcrumbs from "../src/components/Breadcrumbs";
import ProtocolScreen from "./screens/ProtocolScreen";
import HRBP_EmployeeHealthStatus from "./screens/HRBP_EmployeeHealthStatus";
import CreateFamilyMemberScreen from "./screens/CreateFamilyMemberScreen";
import TextTest from "./screens/TextTest";
import ProfileNotesScreen from "./screens/ProfileNotesScreen";
import OxygenScreen from "./screens/OxygenScreen";
// This site has 3 pages, all of which are rendered
// dynamically in the browser (not server rendered).
//
// Although the page does not ever refresh, notice how
// React Router keeps the URL up to date as you navigate
// through the site. This preserves the browser history,
// making sure things like the back button and bookmarks
// work properly.

if (
  process.env.REACT_APP_BASE_URL ===
  "https://covidsupport.altimetrik.com//healthapi/api/v1/"
) {
  amplitude.getInstance().init("24c87b4b59847c4d717b67bc7dcf962d");
} else {
  amplitude.getInstance().init("71331e85a1538b21d1f2d8e4dc896974");
}

export default function App(props) {
  return (
    <Router>
      <div>
        {/*
          A <Switch> looks through all its children <Route>
          elements and renders the first one whose path
          matches the current URL. Use a <Switch> any time
          you have multiple routes, but you want only one
          of them to render at a time
        */}

        {console.log(process.env)}

        <Switch>
          <Route exact path="/">
            <Login />
          </Route>
          <Route exact path="/home">
            <Home />
          </Route>
          <Route exact path="/home/healthStatus">
            <Category />
          </Route>
          <Route exact path="/home/healthStatus/questions">
            <Quiz />
          </Route>
          <Route path="/home/healthStatus/questions/summary">
            <QuizSum />
          </Route>
          <Route path="/thankyou">
            <ThankYou />
          </Route>
          <Route path="/home/CoronaKavachPolicy">
            <Policy />
          </Route>
          <Route path="/home/helpdesk">
            <HelpDesk />
          </Route>

          <Route path="/home/rtpcrTesting">
            <Rtcpr />
          </Route>

          <Route path="/home/generalInsurance">
            <GeneralInsurance />
          </Route>
          <Route path="/home/protocol">
            <Protocols />
          </Route>
          <Route path="/home/oxygen">
            <Oxygen />
          </Route>

          <Route path="/dashboard">
            <Dashboard />
          </Route>

          <Route path="/dashboardOverview">
            <Dashboard_Health />
          </Route>

          <Route path="/dashboardHealth">
            <Dashboard_Alert />
          </Route>

          <Route path="/dashboardUserProfile">
            <Dashboard_UserProfile />
          </Route>

          <Route path="/dashboardSummary">
            <Dashboard_Summary />
          </Route>

          <Route path="/dashboardGeneralInfo">
            <Dashboard_GeneralInfo />
          </Route>

          <Route path="/dashboardContactInfo">
            <Dashboard_ContactInfo />
          </Route>

          <Route path="/home/hrbp_EmployeeSts">
            <HRBP_EmplHealthSts />
          </Route>

          <Route path="/HRBP Notes">
            <HRBP_UserNotes />
          </Route>

          
          <Route path="/family">
            <FamilyMembersAdd />
          </Route>

        </Switch>
      </div>
    </Router>
  );
}

// You can think of these components as "pages"
// in your app.

function Login() {
  // amplitude.getInstance().logEvent("Accessed landing page");
  return <WelcomeScreen></WelcomeScreen>;
}

function Home() {
  const empMail = useSelector((state) => state.empMail);
  amplitude.getInstance().setUserId(`${empMail}`);
  amplitude.getInstance().logEvent("Successfully Logged-in");
  return <BasicInformationScreen></BasicInformationScreen>;
}

function Quiz() {
  return (
    <>
      <QuizScreen></QuizScreen>
    </>
  );
}

function QuizSum() {
  return (
    <>
      <QuizSummary></QuizSummary>
    </>
  );
}

function Category() {
  amplitude.getInstance().logEvent("Initiated Health Assessment");
  return (
    <>
      <CategorySelection></CategorySelection>
    </>
  );
}

function ThankYou() {
  amplitude.getInstance().logEvent("Completed Health Assessment");
  return <ThankYouScreen></ThankYouScreen>;
}

function HelpDesk() {
  return (
    <>
      <HelpDeskScreen></HelpDeskScreen>
    </>
  );
}
function Policy() {
  return (
    <>
      <CoronaPolicyScreen></CoronaPolicyScreen>
    </>
  );
}

function Rtcpr() {
  return (
    <>
      <RtpcrScreen></RtpcrScreen>
    </>
  );
}

function GeneralInsurance() {
  return (
    <>
      <GeneralInsuranceScreen></GeneralInsuranceScreen>
    </>
  );
}

function Protocols() {
  return (
    <>
      <ProtocolScreen></ProtocolScreen>
    </>
  );
}

function Oxygen() {
  return (
    <>
      <OxygenScreen></OxygenScreen>
    </>
  );
}

function FamilyMembersAdd(){
  return (
    <>
      <CreateFamilyMemberScreen></CreateFamilyMemberScreen>
    </>
  );
}


function Dashboard() {
  return (
    <>
      <DashboardHomeScreen></DashboardHomeScreen>
    </>
  );
}

function Dashboard_Health() {
  return <Dashboard_HealthAssessment></Dashboard_HealthAssessment>;
}

function Dashboard_Alert() {
  return <HealthAlertScreen></HealthAlertScreen>;
}

function Dashboard_UserProfile() {
  return <UserProfileDashboard></UserProfileDashboard>;
}

function Dashboard_Summary() {
  return <DashboardSumary></DashboardSumary>;
}

function Dashboard_GeneralInfo() {
  return <DashboardGeneralInfo></DashboardGeneralInfo>;
}

function Dashboard_ContactInfo() {
  return <DashboardContactInfo></DashboardContactInfo>;
}

function HRBP_EmplHealthSts() {
  return <HRBP_EmployeeHealthStatus></HRBP_EmployeeHealthStatus>;
}

function HRBP_UserNotes() {
  return <ProfileNotesScreen></ProfileNotesScreen>;
}
