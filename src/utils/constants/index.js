import IMAGES from "./images";
import STRING from "./string";
import {COLORS} from "./theme";

export { STRING, IMAGES, COLORS};
