

 const BackBtn="/images/Black Back Button.png"
 const CheckBox="/images/Checkbox (Active).png"
 const Logout="/images/logout.svg"
 const Thankyou="/images/thankyou.svg"
 const PATH="/images/Path.png"
 const Radio="/images/Radio.png"
 const LOGO="/images/logo.svg"
 const DB_User_ICON="/images/DB_User_Icon.svg"
 const Button_Arrow="/images/ButtonArrow.svg"
 const AvailableIndicator="/images/Available.svg"
 const UnAvailableIndicator="/images/UnAvailable.svg"
const GREYIndicator="/images/Grey.svg"


export default {
    BackBtn,
    CheckBox,
    Logout,
    Thankyou,
    PATH,
    Radio,
    LOGO,
    DB_User_ICON,
    Button_Arrow,
    AvailableIndicator,
    UnAvailableIndicator,
    GREYIndicator
}

