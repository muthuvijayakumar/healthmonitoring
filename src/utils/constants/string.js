
// Placeholder
 const EMAIL_PLACEHOLDER_TEXT="xyz@altimetrik.com";
 const MOBILE_NO_PLACEHOLDER_TEXT="Enter Number Without Country Code";
 const NAME_PLACEHOLDER_TEXT="Enter Name";
 const RELATIONSHIP_PLACEHOLDER_TEXT="Enter Relationship";

// Disclaimer

const Disclaimer={
        DISCLAIMER_TEXT:"Terms of Use :",
        DISCLAIMER_TEXT_1:"It is an employee welfare service brought by Altimetrik India Private Limited to monitor the health status of its employees and support them in fighting against COVID-19.",
        DISCLAIMER_TEXT_2:"By using the service, the employee provides his/her consent and authorizes Altimetrik to store personal information and medical records shared by the employee voluntarily and to use it for the employee welfare initiatives.",
        DISCLAIMER_TEXT_3:"The service is voluntary, but the company strongly encourages all employees to support the initiative by sharing health status and supporting information in regular intervals so that the company can extend the best possible support.",
        DISCLAIMER_TEXT_4:"Safeguarding the information shared by you is our priority. We use systems, policies, and procedures to protect information from loss, misuse, or alteration.",
        DISCLAIMER_TEXT_5:"On a need basis, the required information may be shared with service partners engaged by Altimetrik to execute COVID-19 support programs and with government agencies if requested or mandated.",
}


//Button 
 const NEXT_BTN="Next";
 const SUBMIT_BTN="SUBMIT";

 export default{
    EMAIL_PLACEHOLDER_TEXT,
    MOBILE_NO_PLACEHOLDER_TEXT,
    NAME_PLACEHOLDER_TEXT,
    RELATIONSHIP_PLACEHOLDER_TEXT,
    Disclaimer,
    NEXT_BTN,
    SUBMIT_BTN
 }