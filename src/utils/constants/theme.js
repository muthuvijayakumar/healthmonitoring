export const COLORS = {

    ERR_CLR:"#eb3f27",
    WHITE:"#fff",
    TEXT_CLR:"#505A73",
    DARK_TEXT_CLR:"#1A1C22"
};
