import React, { useState, useEffect } from "react";
import { useHistory } from "react-router";
import { STRING, IMAGES, COLORS } from "../utils/constants";
import { useDispatch, useSelector } from "react-redux";
import API from "../utils/api/API";
import DatePicker from "react-datepicker";
import "./css/CreateFamilyMemberScreen.css";
import { Button } from "../components/Button";
import { CustomButton } from "../components/CustomButton";
import Modal from "react-modal";
Modal.setAppElement("#root");
import moment from "moment";

export default function CreateFamilyMemberScreen() {
  const history = useHistory();

  const [screenIndex, setScreenIndex] = useState(0);
  const [listOfFamilyDetails, setListOfFamilyDetails] = useState("");

  const data = useSelector((state) => state.dashboardSum);
  const [err, setERR] = useState("");

  const empID = useSelector((state) => state.empID);
  const Id = useSelector((state) => state.uniqueID);
  const _Id = useSelector((state) => state._uniqueID);

  const empName = useSelector((state) => state.empName);
  const empMail = useSelector((state) => state.empMail);
  const empACName = useSelector((state) => state.empAccountName);
  const empGender = useSelector((state) => state.gender);
  const empdob = useSelector((state) => state.dob);
  const empHrbpName = useSelector((state) => state.hrbpName);
  const empWorkLocation = useSelector((state) => state.workLocation);
  const isLocationUpdated = useSelector((state) => state.isLocationUpdated);
  const jwt = useSelector((state) => state.jwt);
  const roles = useSelector((state) => state.roles);
  const initialEmpData = useSelector((state) => state.initalEmpData);

  function OnClickBackBtn() {
    if (screenIndex > 0) {
      setScreenIndex(screenIndex - 1);
    } else {
      history.push("/dashboardUserProfile");
    }
  }

  const customStyles = {
    content: {
      top: "88%",
      left: "50%",
      right: "auto",
      bottom: "-40%",
      transform: "translate(-50%, -50%)",
      borderRadius: "10px",
      width: "82%",
    },
  };

  function OnClickSaveBtn() {
    if (
      currentLoc !== null &&
      currentLoc !== "" &&
      emergencyConName !== null &&
      emergencyConName !== "" &&
      emergencyConNo !== null &&
      emergencyConNo !== "" &&
      emergencyConRelation !== null &&
      emergencyConRelation !== "" &&
      PrimaryConNo !== null &&
      PrimaryConNo !== "" &&
      PrimaryConNo !== undefined
    ) {
      let tempAltNo, tempAltNo2;
      console.log(altPhnNo1);

      if (altPhnNo1 === "" && altPhnNo1 !== null) {
        tempAltNo = 0;
      } else {
        tempAltNo = altPhnNo1;
      }

      if (altPhnNo2 === "" && altPhnNo2 !== null) {
        tempAltNo2 = 0;
      } else {
        tempAltNo2 = altPhnNo2;
      }

      API.put(
        process.env.REACT_APP_EMPLOYEE_DETAILS + Id,
        {
          _id: `${_Id}`,
          accountName: `${empACName}`,
          alternativeContactNo1: tempAltNo,
          alternativeContactNo2: tempAltNo2,
          currentLocation: `${currentLoc}`,
          dob: `${empdob}`,
          emailAddr: `${empMail}`,
          emergencyContactName: `${emergencyConName}`,
          emergencyContactNumber: emergencyConNo,
          emergencyContactRelation: `${emergencyConRelation}`,
          empId: empID,
          empName: `${empName}`,
          gender: `${empGender}`,
          hrbpName: `${empHrbpName}`,
          id: `${Id}`,
          isLocationUpdated: true,
          primaryContactNo: PrimaryConNo,
          workLocation: `${empWorkLocation}`,
          roles: `${roles}`,
        },
        {
          headers: {
            Authorization: `Bearer ${jwt}`,
          },
        }
      )
        .then((res) => {
          console.log(res);
          setIsEditBtnClicked(false);
          history.push("/dashboardUserProfile");
        })
        .catch((err) => {
          history.push("/");

          console.log(err.res);
          setERR("Internal Error");
          console.log(err);
        });
    } else {
      setERR("Please provide all the details");
    }
  }

  function SetDetailsForFirstScreen(name, relationship, dob, gender) {
    setListOfFamilyDetails({
      FName: name,
      FRelation: relationship,
      FDoB: dob,
      FGender: gender,
    });

    console.log(listOfFamilyDetails);
  }

  function SetDetailsForSecondScreen(curLocationVal, isCoLocatedVal) {
    console.log(isCoLocatedVal);

    setListOfFamilyDetails({
      ...listOfFamilyDetails,
      curLocation: curLocationVal,
      isCoLocated: isCoLocatedVal,
    });
  }

  function SetDetailsForThirdScreen(
    primaryNo,
    altNo1,
    altNo2,
    contName,
    emergencyRelation,
    contactNo,
    isSameContactInfoVal
  ) {
    console.log(isSameContactInfoVal);

    setListOfFamilyDetails({
      ...listOfFamilyDetails,
      FPrimaryNo: primaryNo,
      FAltNo1: altNo1,
      FAltNo2: altNo2,
      FContName: contName,
      FEmergencyRelation: emergencyRelation,
      FContactNo: contactNo,
      FSameContactInfo: isSameContactInfoVal,
    });
  }

  useEffect(() => {
    console.log(listOfFamilyDetails);
  }, [listOfFamilyDetails]);

  return (
    <div className="page-container">
      <div className="content-wrap">
        <div style={{ height: "94%" }} className="contactInfo_container">
          <div id="contactInfo">
            <div className="backbtn_block">
              <div onClick={OnClickBackBtn} className="backbtn_img">
                <img src="/images/Black Back Button.png" alt="back btn" />
              </div>
              <div className="backbtn_txt">Add family member</div>
            </div>

            <div className="contactInfo_body">
              <div className="label_head pad-t10">Employee Name</div>

              <div className="nopad_whitebox bg_white">
                <div className="wb_evnt1">{empName}</div>
                <div className="wb_evnt2 pad-t5">{empMail}</div>
              </div>
            </div>
            {screenIndex === 0 ? (
              <FirstScreen></FirstScreen>
            ) : (
              <div>{null}</div>
            )}
            {screenIndex === 1 ? (
              <SecondScreen></SecondScreen>
            ) : (
              <div>{null}</div>
            )}
            {screenIndex === 2 ? (
              <ThirdScreen></ThirdScreen>
            ) : (
              <div>{null}</div>
            )}

            {screenIndex === 3 ? <Summary></Summary> : <div>{null}</div>}
          </div>
        </div>
      </div>
    </div>
  );

  function FirstScreen() {
    const [relativeName, setRelativeName] = useState("");
    const [relationship, setRelationship] = useState("");
    const [relativeDob, setRelativeDob] = useState("");
    const [relativeGender, setRelativeGender] = useState("");
    const [nxtBtnDisability, setNxtBtnDisability] = useState(true);
    const [discardModal, setDiscardModal] = useState(false);

    function onFocus() {
      setERR("");
    }

    function SetRelName(val) {
      setRelativeName(val.target.value);
    }

    function SetDOB(val) {
      setRelativeDob(val);
    }

    function SetRelationType(e) {
      setRelationship(e.target.value);
    }
    const SetGender = (e) => {
      setRelativeGender(e.target.value);
    };

    function OnClickModalOpen() {
      setDiscardModal(true);
    }

    function OnClickCancelModal() {
      setDiscardModal(false);
    }

    function OnClickChange() {}

    useEffect(() => {
      console.log(listOfFamilyDetails.length);
      console.log(listOfFamilyDetails);

      // if (listOfFamilyDetails) {
      //   console.log("iocasicas ");

      //   var tempDate = moment(listOfFamilyDetails.FDoB).format("DD/MM/yyyy");
      //   console.log(tempDate);

      //   setRelativeName(listOfFamilyDetails.FName);
      //   setRelativeDob(tempDate);
      // }

      if (
        relativeName != "" &&
        relationship != "" &&
        relativeDob != "" &&
        relativeGender != ""
      ) {
        setNxtBtnDisability(false);
      }

      // action on update of movies
    }, [relativeGender, relationship, relativeDob, relativeName]);

    function OnClickNextBtn() {
      // setScreenIndex(1);

      if (
        relativeName != "" &&
        relationship != "" &&
        relativeDob != "" &&
        relativeGender != ""
      ) {
        SetDetailsForFirstScreen(
          relativeName,
          relationship,
          relativeDob,
          relativeGender
        );
        setScreenIndex(1);
      }
    }

    function OnClickCancel() {
      OnClickModalOpen();
      console.log("cancel");
    }

    return (
      <div>
        {" "}
        <div className="frm_head pad-t10">
          Relationship with Employee{" "}
          <span style={{ color: COLORS.ERR_CLR }}> *</span>
        </div>
        <div className="head_content head_content_edit">
          <select onChange={SetRelationType} className="frm_dropdown_select">
            <option className="frm_dropdown_select" selected="selected">
              Mother
            </option>
            <option className="head_content">Father</option>
            <option className="head_content">sister</option>{" "}
            <option className="head_content">son</option>{" "}
            <option className="head_content">daughter</option>{" "}
            <option className="head_content">wife</option>{" "}
            <option className="head_content">father-in law</option>{" "}
            <option className="head_content">mother-in law</option>
          </select>
        </div>
        <div className="frm_head pad-t10">
          Name of the Family members
          <span style={{ color: COLORS.ERR_CLR }}> *</span>
        </div>
        <input
          className="frm_input_sml"
          onFocus={onFocus}
          value={relativeName}
          placeholder="Enter Name Here"
          type="text"
          onChange={SetRelName}
        ></input>
        <div className="frm_head pad-t10">
          Date of birth <span style={{ color: COLORS.ERR_CLR }}> *</span>
        </div>
        <DatePicker
          className="frm_input_sml"
          placeholderText="Select a date"
          value={relativeDob}
          selected={relativeDob}
          onChange={(date) => {
            SetDOB(date);
          }}
        />
        <div className="frm_head pad-t10">
          Gender<span style={{ color: COLORS.ERR_CLR }}> *</span>
        </div>
        <div className="head_content head_content_edit">
          <select onChange={SetGender} className="frm_dropdown_select">
            <option className="head_content" selected="selected">
              Male
            </option>
            <option className="head_content">Female</option>
            <option className="head_content">Others</option>{" "}
          </select>
        </div>
        <h5
          style={{
            textAlign: "left",
            paddingTop: "5px",
            color: COLORS.ERR_CLR,
            fontFamily: "Inter",
            fontStyle: "normal",
            fontSize: "12px",
            margin: "5px",
          }}
        >
          {err}
        </h5>
        <div className="pad-t10">
          {nxtBtnDisability ? (
            <CustomButton
              buttonDisability={nxtBtnDisability}
              backgroundColor="#DDDDDD"
              textColor="#888888"
              onPress={OnClickNextBtn}
              children="Next"
            ></CustomButton>
          ) : (
            <Button onClick={OnClickNextBtn} children="Next"></Button>
          )}
        </div>
        <div className="pad-t10">
          <CustomButton
            buttonDisability={false}
            backgroundColor="#EBEDFF"
            textColor="#4D59FC"
            onPress={OnClickCancel}
            children="Cancel"
          ></CustomButton>
        </div>
        <Modal
          isOpen={discardModal}
          style={customStyles}
          shouldCloseOnOverlayClick={false}
        >
          <div>
            <div className="popup_txt pad-t10">
              You are almost there. Do you really want to cancel?
            </div>

            <div style={{ padding: "20px 0px" }}>
              <Button
                style={{ width: "100%" }}
                onClick={OnClickChange}
                children="Yes,I do"
              ></Button>
            </div>

            <div>
              <a
                href="#"
                className="change_button"
                style={{ width: "100%" }}
                children="Discard"
                onClick={OnClickCancelModal}
              >
                No,I don’t
              </a>
            </div>
          </div>
        </Modal>
      </div>
    );
  }
  function SecondScreen() {
    const [onClickYes, setOnClickYes] = useState(false);
    const [onClickNo, setOnClickNo] = useState(false);
    const [currentLoc, setCurrentLoc] = useState("");
    const [discardModal, setDiscardModal] = useState(false);
    const [nxtBtnDisability, setNxtBtnDisability] = useState(true);
    const [isCoLocated, setIsCoLocated] = useState(false);

    const employeeData = useSelector((state) => state.initalEmpData);

    function OnClickNextBtn() {
      if (onClickNo) {
        if (currentLoc != "" && isCoLocated != "") {
          SetDetailsForSecondScreen(currentLoc, isCoLocated);
          setScreenIndex(2);
        }
      } else if (onClickYes) {
        SetDetailsForSecondScreen(currentLoc, isCoLocated);
        setScreenIndex(2);
      }
    }

    function getLocation(val) {
      setCurrentLoc(val.target.value);
    }

    function OnClickYes() {
      setIsCoLocated(true);
      setCurrentLoc(initialEmpData.employeedetails.currentLocation);
      setOnClickYes(true);
      setOnClickNo(false);
    }

    function OnClickNo() {
      setIsCoLocated(false);

      setOnClickYes(false);
      setOnClickNo(true);
    }

    function OnClickModalOpen() {
      setDiscardModal(true);
    }

    function OnClickCancelModal() {
      setDiscardModal(false);
    }

    function OnClickChange() {}
    function OnClickCancel() {
      OnClickModalOpen();
      console.log("cancel");
    }

    useEffect(() => {
      if (onClickNo) {
        if (currentLoc != "") {
          setNxtBtnDisability(false);
        } else {
          setNxtBtnDisability(true);
        }
      }
      if (onClickYes) {
        setNxtBtnDisability(false);
      }

      // action on update of movies
    }, [currentLoc, onClickYes, onClickNo, isCoLocated]);

    return (
      <div id="SecondScren">
        <div className="frm_head pad-t10">
          Co-Located with the Employee?
          <span style={{ color: COLORS.ERR_CLR }}> *</span>
        </div>
        <div>
          <div style={{ flexDirection: "row", display: "flex" }}>
            <div>
              <input type="radio" id="1" name="a" onChange={OnClickYes}></input>{" "}
              Yes
            </div>
            <div>
              <input type="radio" id="1" name="a" onChange={OnClickNo}></input>{" "}
              No
            </div>
          </div>

          {onClickYes ? (
            <>
              {" "}
              <div className="frm_sub_qst pad-t10"> Location</div>
              <div className="frm_sub_ans">
                {initialEmpData.employeedetails.currentLocation}
              </div>
            </>
          ) : (
            <div>{null}</div>
          )}

          {onClickNo ? (
            <>
              {" "}
              <div className="frm_sub_qst pad-t10">Current Location *</div>
              <div className="frm_sub_ans">
                <textarea
                  style={{
                    borderWidth: 1,
                    borderRadius: 3,
                    fontSize: "15px",
                    backgroundColor: COLORS.WHITE,
                    paddingLeft: "15px",
                    width: "96%",
                    height: "76px",
                    marginBottom: "20px",
                    textAlign: "start",
                  }}
                  maxLength={500}
                  multiple={true}
                  value={currentLoc}
                  placeholder="Enter Location here"
                  type="text"
                  onChange={getLocation}
                ></textarea>
              </div>
            </>
          ) : (
            <div>{null}</div>
          )}
        </div>
        <div className="pad-t10">
          {nxtBtnDisability ? (
            <CustomButton
              buttonDisability={nxtBtnDisability}
              backgroundColor="#DDDDDD"
              textColor="#888888"
              onPress={OnClickNextBtn}
              children="Next"
            ></CustomButton>
          ) : (
            <Button onClick={OnClickNextBtn} children="Next"></Button>
          )}
        </div>
        <div className="pad-t10">
          <CustomButton
            buttonDisability={false}
            backgroundColor="#EBEDFF"
            textColor="#4D59FC"
            onPress={OnClickCancel}
            children="Cancel"
          ></CustomButton>
        </div>
        <Modal
          isOpen={discardModal}
          //onAfterOpen={afterOpenModal}
          //onRequestClose={editModalOpen}
          style={customStyles}
          shouldCloseOnOverlayClick={false}
        >
          <div>
            <div className="popup_txt pad-t10">
              You are almost there. Do you really want to cancel?
            </div>

            <div style={{ padding: "20px 0px" }}>
              <Button
                style={{ width: "100%" }}
                onClick={OnClickChange}
                children="Yes,I do"
              ></Button>
            </div>

            <div>
              <a
                href="#"
                className="change_button"
                style={{ width: "100%" }}
                children="Discard"
                onClick={OnClickCancelModal}
              >
                No,I don’t
              </a>
            </div>
          </div>
        </Modal>
      </div>
    );
  }

  function ThirdScreen() {
    const [onClickYes, setOnClickYes] = useState(true);
    const [onClickNo, setOnClickNo] = useState(false);
    const [nxtBtnDisability, setNxtBtnDisability] = useState(true);
    const [discardModal, setDiscardModal] = useState(false);
    const [isSameContactInfo, setIsSameContactInfo] = useState(false);

    const [altPhnNo1, setAltPhnNo1] = useState("");
    const [altPhnNo2, setAltPhnNo2] = useState("");
    const [emergencyConName, setEmergencyConName] = useState("");
    const [emergencyConNo, setEmergencyConNo] = useState("");
    const [emergencyConRelation, setEmergencyConRelation] = useState("");
    const [PrimaryConNo, setPrimaryConNo] = useState("");

    function OnClickYes() {
      setIsSameContactInfo(true);
      setOnClickYes(true);
      setOnClickNo(false);
    }

    function OnClickNo() {
      setIsSameContactInfo(false);

      setOnClickYes(false);
      setOnClickNo(true);
    }

    function SetRelationType(e) {
      setEmergencyConRelation(e.target.value);
    }

    function OnClickNextBtn() {
      if (
        altPhnNo1 != "" &&
        altPhnNo2 != "" &&
        emergencyConName != "" &&
        emergencyConNo != "" &&
        PrimaryConNo != "" &&
        emergencyConRelation != ""
      ) {
        SetDetailsForThirdScreen(
          PrimaryConNo,
          altPhnNo1,
          altPhnNo2,
          emergencyConName,
          emergencyConRelation,
          emergencyConNo,
          isSameContactInfo
        );
        setScreenIndex(3);
      } else {
        SetDetailsForThirdScreen(
          PrimaryConNo,
          altPhnNo1,
          altPhnNo2,
          emergencyConName,
          emergencyConRelation,
          emergencyConNo,
          isSameContactInfo
        );
        setScreenIndex(3);
      }
    }

    function OnClickModalOpen() {
      setDiscardModal(true);
    }

    function OnClickCancelModal() {
      setDiscardModal(false);
    }

    function OnClickChange() {}

    function getAltPhnNo(val) {
      const re = /^[0-9\b]+$/;
      if (val.target.value === "" || re.test(val.target.value)) {
        setAltPhnNo1(val.target.value);
      }
    }
    function getAltPhnNo2(val) {
      const re = /^[0-9\b]+$/;
      if (val.target.value === "" || re.test(val.target.value)) {
        setAltPhnNo2(val.target.value);
      }
    }
    function getEmergencyConName(val) {
      setEmergencyConName(val.target.value);
    }
    function getEmergencyConNo(val) {
      const re = /^[0-9\b]+$/;
      if (val.target.value === "" || re.test(val.target.value)) {
        setEmergencyConNo(val.target.value);
      }
    }

    function getPrimaryConNo(val) {
      const re = /^[0-9\b]+$/;
      if (val.target.value === "" || re.test(val.target.value)) {
        setPrimaryConNo(val.target.value);
      }
    }

    useEffect(() => {
      console.log(onClickYes);

      setAltPhnNo1(initialEmpData.employeedetails.alternativeContactNo1);
      setAltPhnNo2(initialEmpData.employeedetails.alternativeContactNo2);
      setEmergencyConName(initialEmpData.employeedetails.emergencyContactName);
      setEmergencyConNo(initialEmpData.employeedetails.emergencyContactNumber);
      setEmergencyConRelation(
        initialEmpData.employeedetails.emergencyContactRelation
      );
      setPrimaryConNo(initialEmpData.employeedetails.primaryContactNo);

      if (onClickNo) {
        if (
          altPhnNo1 != "" &&
          altPhnNo2 != "" &&
          emergencyConName != "" &&
          emergencyConNo != "" &&
          PrimaryConNo != "" &&
          emergencyConRelation != ""
        ) {
          setNxtBtnDisability(false);
        } else {
          setNxtBtnDisability(true);
        }
      } else if (onClickYes) {
        setNxtBtnDisability(false);
      }

      // action on update of movies
    }, [
      onClickYes,
      onClickNo,
      altPhnNo1,
      altPhnNo2,
      emergencyConName,
      emergencyConNo,
      PrimaryConNo,
      emergencyConRelation,
    ]);

    function OnClickCancel() {
      OnClickModalOpen();
      console.log("cancel");
    }

    const employeeData = useSelector((state) => state.initalEmpData);

    return (
      <div id="ThirdScreen">
        <div className="frm_head pad-t10">
          Contact information same as Employee?
        </div>
        <div style={{ flexDirection: "row", display: "flex" }}>
          <div>
            <input
              type="radio"
              id="1"
              name="a"
              checked={onClickYes}
              onChange={OnClickYes}
            ></input>{" "}
            Yes
          </div>
          <div>
            <input type="radio" id="1" name="a" onChange={OnClickNo}></input> No
          </div>
        </div>
        {onClickYes ? (
          <>
            {" "}
            <div className="frm_sub_qst pad-t10">
              Primary Contact Number
              <span style={{ color: COLORS.ERR_CLR }}> *</span>
            </div>
            <div className="frm_sub_ans">
              {initialEmpData.employeedetails.primaryContactNo}
            </div>
            <div className="frm_sub_qst pad-t10">
              Alternate Contact Number 1
            </div>
            <div className="frm_sub_ans">
              {initialEmpData.employeedetails.alternativeContactNo1 === "0"
                ? initialEmpData.employeedetails.alternativeContactNo1
                : "-"}
            </div>
            <div className="frm_sub_qst pad-t10">
              Alternate Contact Number 2
            </div>
            <div className="frm_sub_ans">
              {initialEmpData.employeedetails.alternativeContactNo2 === "0"
                ? initialEmpData.employeedetails.alternativeContactNo1
                : "-"}
            </div>
            <div className="frm_sub_qst pad-t10">Emergency Contact Person</div>
            <div className="frm_sub_ans">
              {" "}
              {initialEmpData.employeedetails.emergencyContactName}
            </div>
            <div className="frm_sub_qst pad-t10">
              Emergency Contact Relation
            </div>
            <div className="frm_sub_ans">
              {" "}
              {initialEmpData.employeedetails.emergencyContactRelation}
            </div>
            <div className="frm_sub_qst pad-t10">Emergency Contact Number</div>
            <div className="frm_sub_ans">
              {initialEmpData.employeedetails.emergencyContactNumber}
            </div>
          </>
        ) : (
          <div>{null}</div>
        )}

        {onClickNo ? (
          <>
            <div className="contactInfo_body">
              <div className="label_head pad-t10">
                Primary Contact Number{" "}
                <span style={{ color: COLORS.ERR_CLR }}>*</span>
              </div>

              <input
                className="head_content head_content_edit"
                style={{
                  borderWidth: 1,
                  borderRadius: 3,
                  fontSize: "15px",
                  backgroundColor: COLORS.WHITE,
                  paddingLeft: "15px",
                  width: "98%",
                  height: "30px",
                }}
                value={PrimaryConNo}
                placeholder="Enter Primary Contact Number"
                type="text"
                onChange={getPrimaryConNo}
              ></input>

              <div className="label_head pad-t10">
                Alternate Contact Number 1
              </div>

              <input
                className="head_content head_content_edit"
                style={{
                  borderWidth: 1,
                  borderRadius: 3,
                  fontSize: "15px",
                  backgroundColor: COLORS.WHITE,
                  paddingLeft: "15px",
                  width: "98%",
                  height: "30px",
                }}
                value={altPhnNo1}
                placeholder="Enter Alternate Contact Number"
                type="text"
                onChange={getAltPhnNo}
              ></input>

              <div className="label_head pad-t10">
                Alternate Contact Number 2
              </div>
              <input
                className="head_content head_content_edit"
                style={{
                  borderWidth: 1,
                  borderRadius: 3,

                  fontSize: "15px",
                  backgroundColor: COLORS.WHITE,
                  paddingLeft: "15px",
                  width: "98%",
                  height: "30px",
                }}
                value={altPhnNo2}
                placeholder="Enter Alternate Contact Number"
                type="text"
                onChange={getAltPhnNo2}
              ></input>
              <div className="label_head pad-t10">
                Emergency Contact Person
                <span style={{ color: COLORS.ERR_CLR }}>*</span>
              </div>
              <input
                className="head_content head_content_edit"
                style={{
                  borderWidth: 1,
                  borderRadius: 3,
                  fontSize: "15px",
                  backgroundColor: COLORS.WHITE,
                  paddingLeft: "15px",
                  width: "98%",
                  height: "30px",
                }}
                value={emergencyConName}
                placeholder="Enter contact person name"
                type="text"
                onChange={getEmergencyConName}
              ></input>
              <div className="label_head pad-t10">
                Relationship*
                <span style={{ color: COLORS.ERR_CLR }}>*</span>
              </div>
              <div className="head_content head_content_edit">
                <select
                  onChange={SetRelationType}
                  className="frm_dropdown_select"
                >
                  <option className="frm_dropdown_select" selected="selected">
                    Mother
                  </option>
                  <option className="head_content">Father</option>
                  <option className="head_content">sister</option>{" "}
                  <option className="head_content">son</option>{" "}
                  <option className="head_content">daughter</option>{" "}
                  <option className="head_content">wife</option>{" "}
                  <option className="head_content">father-in law</option>{" "}
                  <option className="head_content">mother-in law</option>
                </select>
              </div>

              <div className="label_head pad-t10">
                Contact Number*
                <span style={{ color: COLORS.ERR_CLR }}>*</span>
              </div>
              <input
                className="head_content head_content_edit"
                style={{
                  borderWidth: 1,
                  borderRadius: 3,
                  fontSize: "15px",
                  backgroundColor: COLORS.WHITE,
                  paddingLeft: "15px",
                  width: "98%",
                  height: "30px",
                }}
                value={emergencyConNo}
                placeholder="Enter contact number here"
                type="text"
                onChange={getEmergencyConNo}
              ></input>
            </div>
          </>
        ) : (
          <div>{null}</div>
        )}

        <div className="pad-t10">
          {nxtBtnDisability ? (
            <CustomButton
              buttonDisability={nxtBtnDisability}
              backgroundColor="#DDDDDD"
              textColor="#888888"
              onPress={OnClickNextBtn}
              children="Next"
            ></CustomButton>
          ) : (
            <Button onClick={OnClickNextBtn} children="Next"></Button>
          )}
        </div>
        <div className="pad-t10">
          <CustomButton
            buttonDisability={false}
            backgroundColor="#EBEDFF"
            textColor="#4D59FC"
            onPress={OnClickCancel}
            children="Cancel"
          ></CustomButton>
        </div>
        <Modal
          isOpen={discardModal}
          style={customStyles}
          shouldCloseOnOverlayClick={false}
        >
          <div>
            <div className="popup_txt pad-t10">
              You are almost there. Do you really want to cancel?
            </div>

            <div style={{ padding: "20px 0px" }}>
              <Button
                style={{ width: "100%" }}
                onClick={OnClickChange}
                children="Yes,I do"
              ></Button>
            </div>

            <div>
              <a
                href="#"
                className="change_button"
                style={{ width: "100%" }}
                children="Discard"
                onClick={OnClickCancelModal}
              >
                No,I don’t
              </a>
            </div>
          </div>
        </Modal>
      </div>
    );
  }

  function Summary() {
    const [discardModal, setDiscardModal] = useState(false);
    const [isAddClick, setIsAddClick] = useState(false);

    function OnClickCancel() {
      OnClickModalOpen();
      console.log("cancel");
    }

    function OnClickModalOpen() {
      setDiscardModal(true);
    }

    function OnClickCancelModal() {
      setDiscardModal(false);
    }

    function OnClickChange() {}

    function OnClickAdd() {
      setIsAddClick(true);
      API.post(
        process.env.REACT_APP_EMPLOYEE_FAMILY_ADD,
        {
          alternativeContactNo1: listOfFamilyDetails.FAltNo1,
          alternativeContactNo2: listOfFamilyDetails.FAltNo2,
          currentLocation: listOfFamilyDetails.curLocation,
          dob: listOfFamilyDetails.FDoB,
          emergencyContactName: listOfFamilyDetails.FContName,
          emergencyContactNumber: listOfFamilyDetails.FContactNo,
          emergencyContactRelation: listOfFamilyDetails.FEmergencyRelation,
          empName: listOfFamilyDetails.FName,
          employeeName: empName,
          gender: listOfFamilyDetails.FGender,
          isCoLocated: listOfFamilyDetails.isCoLocated,
          isLocationUpdated: true,
          isSameContactInfo: listOfFamilyDetails.FSameContactInfo,
          memberType: "FAMILY",
          parentId: empID,
          primaryContactNo: listOfFamilyDetails.FPrimaryNo,
          relationship: listOfFamilyDetails.FRelation,
          roles: "GLOBAL",
        },
        {
          headers: {
            Accept: "application/json",
            "Content-Type": "application/json",
            Authorization: `Bearer ${jwt}`,
          },
        }
      )
        .then((res) => {
          history.push("/dashboardUserProfile");
          console.log(res.data);
        })
        .catch((err) => {
          console.log(err.response.status);
          if (err.response.status === 500) {
            setERR("Please try again later.");
          }
          if (err.response.status === 404) {
            setERR(
              "Oops. We could not find your Employee Email ID.please try again"
            );
            throw new Error(`${err.config.url} not found`);
          }

          if (err.response.status === 400) {
            setERR("400");
            throw new Error(`${err.config.url} not found`);
          }

          if (err.response.status === 401) {
            setERR(
              "Contact admin, employee details not updated in covid support application"
            );
            throw new Error(`${err.config.url} not found`);
          }

          throw err;
        });
    }

    return (
      <div id="Summaryscreen">
        <div className="frm_head pad-t10">
          <span>Profile Information</span>
          <span className="float_r frm_edit_link">Edit</span>
        </div>
        <div className="frm_sub_qst pad-t10">Employee Name</div>
        <div className="frm_sub_ans">{empName}</div>
        <div className="frm_sub_qst pad-t10">Relationship with Employee</div>
        <div className="frm_sub_ans">{listOfFamilyDetails.FRelation}</div>
        <div className="frm_sub_qst pad-t10">Name of the Family member</div>
        <div className="frm_sub_ans">{listOfFamilyDetails.FName}</div>
        <div className="frm_sub_qst pad-t10">Date of birth</div>
        <div className="frm_sub_ans">
          {moment(listOfFamilyDetails.FDoB).format("DD/MM/yyyy")}
        </div>
        <div className="frm_sub_qst pad-t10">Gender</div>
        <div className="frm_sub_ans">{listOfFamilyDetails.FGender}</div>
        <div className="frm_sub_qst pad-t10">Relationship</div>
        <div className="frm_sub_ans">{listOfFamilyDetails.FRelation}</div>

        <div
          style={{
            margin: "10px -16px",
            height: "0px",
            border: "0.5px solid rgba(0, 0, 0, 0.1)",
          }}
        ></div>

        <div className="frm_head">
          <span>Current Location</span>
          <span className="float_r frm_edit_link">Edit</span>
        </div>
        <div className="frm_sub_qst pad-t10">Location</div>
        <div className="frm_sub_ans">{listOfFamilyDetails.curLocation}</div>

        <div
          style={{
            margin: "10px -16px",
            height: "0px",
            border: "0.5px solid rgba(0, 0, 0, 0.1)",
          }}
        ></div>

        <div className="frm_head">
          <span>Contact Information</span>
          <span className="float_r frm_edit_link">Edit</span>
        </div>

        <div className="frm_sub_qst pad-t10">
          Primary Contact Number
          <span style={{ color: COLORS.ERR_CLR }}> *</span>
        </div>
        <div className="frm_sub_ans"> {listOfFamilyDetails.FPrimaryNo}</div>
        <div className="frm_sub_qst pad-t10">Alternate Contact Number 1</div>
        <div className="frm_sub_ans"> {listOfFamilyDetails.FAltNo1}</div>
        <div className="frm_sub_qst pad-t10">Alternate Contact Number 2</div>
        <div className="frm_sub_ans"> {listOfFamilyDetails.FAltNo2}</div>
        <div className="frm_sub_qst pad-t10">Emergency Contact Person</div>
        <div className="frm_sub_ans"> {listOfFamilyDetails.FContName}</div>
        <div className="frm_sub_qst pad-t10">Emergency Contact Relation</div>
        <div className="frm_sub_ans">
          {" "}
          {listOfFamilyDetails.FEmergencyRelation}
        </div>
        <div className="frm_sub_qst pad-t10">Emergency Contact Number</div>
        <div className="frm_sub_ans"> {listOfFamilyDetails.FContactNo}</div>

        {isAddClick ? (
          <div>{null}</div>
        ) : (
          <div className="pad-t10">
            <Button onClick={OnClickAdd} children="Add"></Button>
          </div>
        )}
        <div className="pad-t10">
          <CustomButton
            buttonDisability={false}
            backgroundColor="#EBEDFF"
            textColor="#4D59FC"
            onPress={OnClickCancel}
            children="Cancel"
          ></CustomButton>
        </div>
        <Modal
          isOpen={discardModal}
          style={customStyles}
          shouldCloseOnOverlayClick={false}
        >
          <div>
            <div className="popup_txt pad-t10">
              You are almost there. Do you really want to cancel?
            </div>

            <div style={{ padding: "20px 0px" }}>
              <Button
                style={{ width: "100%" }}
                onClick={OnClickChange}
                children="Yes,I do"
              ></Button>
            </div>

            <div>
              <a
                href="#"
                className="change_button"
                style={{ width: "100%" }}
                children="Discard"
                onClick={OnClickCancelModal}
              >
                No,I don’t
              </a>
            </div>
          </div>
        </Modal>
      </div>
    );
  }
}
