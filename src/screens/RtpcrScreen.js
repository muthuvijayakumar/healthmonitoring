import React, { useEffect, useRef } from "react";
import "./css/RtpcrScreen.css";
import { useHistory } from "react-router";
import { useSelector } from "react-redux";
import Breadcrumbs from "../components/Breadcrumbs";

function RtpcrScreen() {
  const history = useHistory();
  const jwt = useSelector((state) => state.jwt);

  function OnClickBackBtn() {
    history.push("/home");
  }

  useEffect(() => {
    
    window.scrollTo(0, 0);
    
    if (jwt === "") {
      history.push("/");
    }
  }, []);

  return (
    <div className="page-container">
      <div className="content-wrap">
        <div style={{ height: "94%" }} className="App">
          <div
            style={{
              flexDirection: "row",
              display: "flex",
              paddingTop: "50px",
            }}
          >
            <img
              onClick={OnClickBackBtn}
              src="/images/Black Back Button.png"
              alt="back btn"
              style={{ width: "32px", height: "32px" }}
            ></img>
            <h3
              style={{
                color: "#1A1C22",
                fontFamily: "Inter",
                fontStyle: "normal",
                fontWeight: "600px",
                fontSize: "17px",
                paddingLeft: "10px",
                paddingTop: "5px",
                margin: "1px",
              }}
            >
              Back
            </h3>
          </div>
          <Breadcrumbs></Breadcrumbs>

          <div class="pagebg_img">
            <div className="bg_img_responsive" style={{backgroundImage: 'url(/images/rtpcrbg.png)'}}>
              <div className="bg_inner_img" style={{backgroundImage: 'url(/images/rtpcrinner.svg)'}}></div>
            </div>
            <div class="top-left1">Company-paid service</div>
            <div class="top-left2">RAT & RT-PCR Testing Facility</div>
            <div class="top-left3">In partnership with Ekincare</div>
            <div class="top-left4">
              Home testing or refer to a hospital / laboratory near to your
              location
            </div>
          </div>

          <div id="rtpcr">
            <div id="section1">
              <div className="txt_styel">
              We are providing this service in association with Ekincare, a leading wellness provider with a network of 170+ hospitals across India. On registration, Ekincare will either enable home testing or refer to a hospital / laboratory near to your location.
              </div>
              <div className="txt_styel pad-t20">
              <span style={{fontWeight:"500",color: "#505A73"}}>Rapid Antigen Test (RAT)</span> gives results rapidly, in just 15 minutes. It mainly detects viral proteins. It is considered less reliable as it reveals the presence of the coronavirus at the peak of the infection when a human has the highest concentration of its proteins. It is recommended to consult a doctor before opting for any tests.
              </div>
            </div>

            <div className="label_head pad-t20">Who can avail this service</div>
            <div className="head_content">
              Employees and their dependent family members having symptoms{" "}
            </div>

            <div
              style={{
                margin: "10px 0px",
                height: "0px",
                border: "0.5px solid rgba(0, 0, 0, 0.1)",
              }}
            ></div>

            <div id="section2">
              <div className="policy_head_grn">
                Steps to avail RT-PCR Testing
              </div>
              <div>
                <div className="policy_cnt1">
                  <p>
                    Visit Ekincare portal through this link {" "}
                    <a href="https://app.ekincare.com/login" target="_blank">
                      https://app.ekincare.com/login
                    </a>{" "} or through their mobile application available in Google Play store and Apple app store.
                  </p>
                  <p>
                  Login to Ekincare Application/Portal through your official email ID
                  </p>
                  <p>Click on ‘Sponsored RT-PCR Test’</p>
                  <p>
                  Click ‘Start’ and fill the generated form for whom the test has to be taken.
                  </p>
                  <p>
                  Ekincare will either enable home testing or refer to a hospital / laboratory near to your location.
                  </p>
                  <p>
                  Take up the test as per the allocated date & time
                  </p>
                  <p style={{ border: "0px" }}>The report can be directly viewed on the app</p>
                  <div className="txt_styel pad-tb5">
                    **Based on test results further steps can be decided.
                  </div>
                </div>
              </div>
            </div>

            <div id="section3">
              <div className="policy_head_grn">
                Steps to avail  RAT Testing
              </div>
              <div>
                <div className="policy_cnt1">
                  <p>
                  Scan the QR Code or click on the link to access the registration portal. {" "}
                    <a href="https://rxcognohelp.itshastra.info/Webpages/PatientRegistration.aspx" target="_blank">
                    https://rxcognohelp.itshastra.info/Webpages/PatientRegistration.aspx
                    </a>{" "} 
                    <img src="/images/ratscan.svg" alt="Image" className="pad-tb5" />
                  </p>
                  <p>
                  Fill the following details on opening the registration link:  Name, Contact Details, Employee ID, and E-mail ID
                  </p>
                  <p>If you are booking the Test for:
                  <div className="head_content">
                    <ul>
                      <li>Self: Fill employee details, proceed to test details, register.</li>
                      <li>Dependent: Fill employee details, add dependent and test details, register.       </li>
                    </ul>
                  </div>
                  </p>
                  <p style={{ border: "0px" }}>
                  You will receive a confirmation with your registration ID on completion via e-mail. Raphacure’s executive will contact you within two hours and proceed further.
                  </p>
                  <div style={{color:"#505A73"}}>For any queries, please write to: <span style={{color:"#818AFD"}}>wellness@raphacure.com</span> or call Toll Free Number: <span style={{color:"#818AFD"}}>95551-66000</span>.</div>
                </div>
              </div>
            </div>

            <div
              style={{
                margin: "10px 0px",
                height: "0px",
                border: "0.5px solid rgba(0, 0, 0, 0.1)",
              }}
            ></div>

            <div id="section4">
              <div className="sub_head pad-b5">Frequently Asked Questions</div>
              <div>
                <div className="policy_head"> When do I take the RTPCR test?</div>
                <div className="policy_cnt2 pad-b5">
                  <div className="txt_styel">
                  If you are showing symptoms of COVID-19 or have met a
                  person who was positive for COVID-19, or if you have already
                  been positive and wish to confirm the absence of the virus.
                  </div>
                </div>

                <div className="policy_head">
                  {" "}
                  What is the difference between RTPCR and RAT?
                </div>
                <div className="policy_cnt2 pad-b5">
                  <div className="txt_styel">
                  RTPCR test can detect the virus even before the emergence of
                  symptoms and aid in isolation, preventing the further spread of
                  infection. If the test turns out positive, the patient is required to
                  follow all COVID-19 protocols, including self-isolation, taking
                  recommended medications, eating healthy, checking for vitals
                  including oxygen saturation.
                  The RAT comes in handy only while detecting patients who are
                  quite symptomatic. It is also done by collecting samples from the
                  nose or throat with a swab. Though the results can be revealed
                  quickly, this test is not considered accurate, as it may not read
                  into vital proteins that are part of Coronavirus at the initial stages.
                  Doctors opine that in certain cases, the rapid antigen test needs
                  to be backed by RT-PCR to completely rule out the possibility of
                  infection.
                  </div>
                </div>

                <div className="policy_head">
                Will I be able to book for both Self and dependent at the same time?
                </div>
                <div className="policy_cnt2 pad-b5">
                  <div className="txt_styel">
                  No, you will have to book the test separately for the dependent.
                  </div>
                </div>

                <div className="policy_head">
                  {" "}
                  Will I be able to add multiple dependents?
                </div>
                <div className="policy_cnt2 pad-b5">
                  <div className="txt_styel">
                  Yes, provided the dependents bear the same address
                  </div>
                </div>

                <div className="policy_head"> If I have two dependents in separate locations, how do I register?</div>
                <div className="policy_cnt2 pad-b5">
                  <div className="txt_styel">
                  You will have to submit separate registrations based on the addresses.
                  </div>
                </div>
              </div>
            </div>    

            <div id="section3">
              <div className="sub_head pad-tb10">Links</div>

              <div>
                <a
                  target="_blank"
                  href="https://app.ekincare.com/login"
                  id="link_button"
                  style={{ width: "95%" }}
                >
                  Ekincare Website
                  <img
                    src="/images/link.png"
                    style={{ textAlign: "center",verticalAlign:"middle" }}
                    align="center"
                  />
                </a>
              </div>

              <div
                style={{
                  width: "100%",
                  padding: "10px 0px",
                  textAlign: "center",
                }}
              >
                <div className="App_linkimg">
                  <a
                    target="_blank"
                    href="https://apps.apple.com/us/app/ekincare/id973037249?ls=1"
                  >
                    <img src="/images/appleimg.png" alt="Image" />
                  </a>
                </div>
                <div className="App_linkimg">
                  <a
                    target="_blank"
                    href="https://play.google.com/store/apps/details?id=com.ekincare"
                  >
                    <img src="/images/playimg.png" alt="Image" />
                  </a>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}

export default RtpcrScreen;