import React, { useState, useEffect } from "react";
import { useHistory } from "react-router";
import Footer from "../components/Footer";
import API from "../utils/api/API";
import { STRING, IMAGES, COLORS } from "../utils/constants";
import "./css/HealthAlertScreen.css";
import { useDispatch, useSelector } from "react-redux";
import { SetDashboardSummary } from "../redux/action";
import ToggleSwitch from "../components/ToggleSwitch";

export default function HealthAlertScreen(props) {
  const history = useHistory();
  const [data, setData] = useState();
  const [loading, setLoading] = useState(true);
  const [dataHealthAlerts, setDataHealthAlerts] = useState(false);
  const jwt = useSelector((state) => state.jwt);

  function OnClickBackBtn() {
    history.push("/dashboard");
  }

  const alertCategory = useSelector((state) => state.healthAlertsCategory);
  const alertCategoryTopic = useSelector(
    (state) => state.healthAlertsCategoryTopic
  );

  useEffect(() => {
    setLoading(true);

    API.get("dashboard/highalerts?type=" + alertCategory, {
      headers: {
        Authorization: `Bearer ${jwt}`,
      },
    })
      .then((res) => {
        console.log(res);
        setData(res.data);
        console.log(data);
        setLoading(false);
      })
      .catch((err) => {
        switch (err.response.status) {
          case 500:
            break;
          case 400:
            break;

          case 401:
            history.push("/");

            break;
          case 403:
            history.push("/");
            break;

          default:
            break;
        }
        console.log(err);

        throw err;
      });
    return () => {};
  }, []);

  return (
    <div className="page-container">
      <div className="content-wrap">
        <div style={{ height: "94%" }} className="HealthAlert">
          <div
            style={{
              flexDirection: "row",
              display: "flex",
              paddingTop: "50px",
            }}
          >
            <img
              src={IMAGES.BackBtn}
              alt="back btn"
              onClick={OnClickBackBtn}
              style={{ width: "32px", height: "32px" }}
            ></img>
            <h3
              style={{
                color: "#1A1C22",
                fontFamily: "Inter",
                fontStyle: "normal",
                fontWeight: "600px",
                fontSize: "17px",
                paddingLeft: "10px",
                paddingTop: "5px",
              }}
            >
              Health Alerts
            </h3>
          </div>

          <div style={{paddingTop:"10px"}}>
          <ToggleSwitch 
            checked = {dataHealthAlerts}
            onChange = {(checked) => setDataHealthAlerts(checked) }/>
          </div>

          <h5
            style={{
              color: "#1A1C22",
              fontFamily: "Inter",
              fontStyle: "normal",
              fontWeight: "600px",
              fontSize: "15px",
              paddingTop: "25px",
            }}
          >
            {alertCategoryTopic}
          </h5>
        </div>

        {loading ? (
          "Loading..."
        ) : (
          <div className="HealthAlertWithoutPadding">
            {alertCategory === "TEMPERATURE" ? (
              <HighTempCard data={data}></HighTempCard>
            ) : (
              <div>{null}</div>
            )}

            {alertCategory === "REQ_HELP" ? (
              <TempSupportCard data={data}></TempSupportCard>
            ) : (
              <div>{null}</div>
            )}

            {alertCategory === "SPO2" ? (
              <SPCard data={data}></SPCard>
            ) : (
              <div>{null}</div>
            )}

            {alertCategory === "FAMILY_INFECTED" ? (
              <TempSupportCard data={data}></TempSupportCard>
            ) : (
              <div>{null}</div>
            )}
          </div>
        )}
      </div>
      <Footer></Footer>
    </div>
  );
}

function TempSupportCard(props) {
  const { data } = props;

  const [loading, setLoading] = useState(false);
  const dispatch = useDispatch();
  const history = useHistory();
  const jwt = useSelector((state) => state.jwt);

  function GetData(empID, cardDate) {
    setLoading(true);

    API.get(process.env.REACT_APP_EMPLOYEE_DASHBOARD_EMPLOYEE_ID + empID + "&date=" + cardDate, {
      headers: {
        Authorization: `Bearer ${jwt}`,
      },
    })
      .then((res) => {
        console.log(res.data);
        setLoading(false);

        dispatch(SetDashboardSummary(res.data));
        history.push("/dashboardSummary");
      })
      .catch((err) => {
        switch (err.response.status) {
          case 500:
            break;
          case 400:
            break;

          case 401:
            history.push("/");

            break;
          case 403:
            history.push("/");
            break;

          default:
            break;
        }
        console.log(err);

        throw err;
      });
  }

  function OnClickReport(id, date) {
    GetData(id, date);
  }

  return (
    <div>
      {loading
        ? "Loading..."
        : data.map((el, id) => (
            <div
              onClick={() => {
                OnClickReport(el.empId, el.date);
              }}
              key={id}
              style={{
                padding: "10px 0px 10px 16px",
                marginTop: 3,
                width: "98%",
                height: "16%",
                display: "flex",
                flexDirection: "column",
                backgroundColor: "white",
                borderBottom: "0.5px solid #E8EDF1",
              }}
            >
              <h5
                style={{
                  color: "#252A31",
                  fontFamily: "Inter",
                  fontStyle: "normal",
                  fontWeight: "600",
                  fontSize: "14px",
                }}
              >
                {el.empName}
              </h5>
              <h5
                style={{
                  color: "#5F738C",
                  fontFamily: "Inter",
                  fontStyle: "normal",
                  fontWeight: "400",
                  fontSize: "12px",
                }}
              >
                {"Last reported on " + new Date(el.date).toLocaleString()}{" "}
              </h5>
            </div>
          ))}
    </div>
  );
}

function HighTempCard(props) {
  const { data } = props;
  const [loading, setLoading] = useState(false);
  const dispatch = useDispatch();
  const history = useHistory();
  const jwt = useSelector((state) => state.jwt);

  function GetData(empID, cardDate) {
    setLoading(true);

    API.get(process.env.REACT_APP_EMPLOYEE_DASHBOARD_EMPLOYEE_ID + empID + "&date=" + cardDate, {
      headers: {
        Authorization: `Bearer ${jwt}`,
      },
    })
      .then((res) => {
        console.log(res.data);
        setLoading(false);

        dispatch(SetDashboardSummary(res.data));
        history.push("/dashboardSummary");
      })
      .catch((err) => {
        switch (err.response.status) {
          case 500:
            break;
          case 400:
            break;

          case 401:
            history.push("/");

            break;
          case 403:
            history.push("/");
            break;

          default:
            break;
        }
        console.log(err);

        throw err;
      });
  }

  function OnClickReport(id, date) {
    GetData(id, date);
  }

  return (
    <div>
      {loading
        ? "Loading..."
        : data.map((el, id) => (
            <div
              onClick={() => OnClickReport(el.empId, el.date)}
              key={id}
              style={{
                padding: "10px 0px 10px 16px",
                margin: 2,
                width: "98%",
                height: "16%",
                display: "flex",
                flexDirection: "column",
                backgroundColor: "white",
                borderBottom: "0.5px solid #E8EDF1",
              }}
            >
              <h5
                style={{
                  color: "#252A31",
                  fontFamily: "Inter",
                  fontStyle: "normal",
                  fontWeight: "600",
                  fontSize: "14px",
                }}
              >
                {el.empName}
              </h5>
              <h5
                style={{
                  color: "#5F738C",
                  fontFamily: "Inter",
                  fontStyle: "normal",
                  fontWeight: "400",
                  fontSize: "12px",
                }}
              >
                {"Last reported on " + new Date(el.date).toLocaleString()}{" "}
              </h5>
              <h5
                style={{
                  color: "#5F738C",
                  fontFamily: "Inter",
                  fontStyle: "normal",
                  fontWeight: "400",
                  fontSize: "12px",
                }}
              >
                High Temperature: {el.surveyresponse.answer}
              </h5>
            </div>
          ))}
    </div>
  );
}


function SPCard(props) {
  const { data } = props;
  const [loading, setLoading] = useState(false);
  const dispatch = useDispatch();
  const history = useHistory();
  const jwt = useSelector((state) => state.jwt);

  function GetData(empID, cardDate) {
    setLoading(true);

    API.get(process.env.REACT_APP_EMPLOYEE_DASHBOARD_EMPLOYEE_ID + empID + "&date=" + cardDate, {
      headers: {
        Authorization: `Bearer ${jwt}`,
      },
    })
      .then((res) => {
        console.log(res.data);
        setLoading(false);

        dispatch(SetDashboardSummary(res.data));
        history.push("/dashboardSummary");
      })
      .catch((err) => {
        switch (err.response.status) {
          case 500:
            break;
          case 400:
            break;

          case 401:
            history.push("/");

            break;
          case 403:
            history.push("/");
            break;

          default:
            break;
        }
        console.log(err);

        throw err;
      });
  }

  function OnClickReport(id, date) {
    GetData(id, date);
  }

  return (
    <div>
      {loading
        ? "Loading..."
        : data.map((el, id) => (
            <div
              onClick={() => OnClickReport(el.empId, el.date)}
              key={id}
              style={{
                padding: "10px 0px 10px 16px",
                margin: 2,
                width: "98%",
                height: "16%",
                display: "flex",
                flexDirection: "column",
                backgroundColor: "white",
                borderBottom: "0.5px solid #E8EDF1",
              }}
            >
              <h5
                style={{
                  color: "#252A31",
                  fontFamily: "Inter",
                  fontStyle: "normal",
                  fontWeight: "600",
                  fontSize: "14px",
                }}
              >
                {el.empName}
              </h5>
              <h5
                style={{
                  color: "#5F738C",
                  fontFamily: "Inter",
                  fontStyle: "normal",
                  fontWeight: "400",
                  fontSize: "12px",
                }}
              >
                {"Last reported on " + new Date(el.date).toLocaleString()}{" "}
              </h5>
              <h5
                style={{
                  color: "#5F738C",
                  fontFamily: "Inter",
                  fontStyle: "normal",
                  fontWeight: "400",
                  fontSize: "12px",
                }}
              >
                Sp02 Percentage: {el.surveyresponse.answer}
              </h5>
            </div>
          ))}
    </div>
  );
}

