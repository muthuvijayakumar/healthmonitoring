import React, { useEffect, useState } from "react";
import "./css/CoronaPolicyScreen.css";
import { useHistory } from "react-router";
import { useSelector } from "react-redux";
import Breadcrumbs from "../components/Breadcrumbs";

function CoronaPolicyScreen() {
  const history = useHistory();
  const [isClickedPlanned, setIsClickedPlanned] = useState(false);
  const [isClickedEmergency, setIsClickedEmergency] = useState(false);
  const [isClickedReimbursement, setIsClickedReimbursement] = useState(false);
  const [isClickedHomeCare, setIsClickedHomeCare] = useState(false);
  const [isClickedHC, setIsClickedHC] = useState(false);
  const [isClickedHospitalization, setIsClickedHospitalization] =
    useState(false);

  const jwt = useSelector((state) => state.jwt);

  function OnClickBackBtn() {
    history.push("/home");
  }

  useEffect(() => {
    window.scrollTo(0, 0);

    if (jwt === "") {
      history.push("/");
    }
  }, []);

  return (
    <div className="page-container">
      <div className="content-wrap">
        <div style={{ height: "94%" }} className="App">
          <div
            style={{
              flexDirection: "row",
              display: "flex",
              paddingTop: "50px",
            }}
          >
            <img
              onClick={() => OnClickBackBtn()}
              src="/images/Black Back Button.png"
              alt="back btn"
              style={{ width: "32px", height: "32px" }}
            ></img>
            <h3
              style={{
                color: "#1A1C22",
                fontFamily: "Inter",
                fontStyle: "normal",
                fontWeight: "600px",
                fontSize: "17px",
                paddingLeft: "10px",
                paddingTop: "5px",
                margin: "1px",
              }}
            >
              Back
            </h3>
          </div>

          <Breadcrumbs></Breadcrumbs>
          <div class="pagebg_img">
            <div className="bg_img_responsive" style={{backgroundImage: 'url(/images/kavachbg.png)'}}>
              <div className="bg_inner_img" style={{backgroundImage: 'url(/images/kavachinner.svg)'}}></div>
            </div>
            <div class="top-left1">Company-paid service</div>
            <div class="top-left2">Corona kavach Policy</div>
            <div class="top-left3">Insured with Oriental Insurance</div>
            <div class="top-left4">
              Policy covers Hospitalization and approved Home care treatment
            </div>
          </div>

          <div id="kavach_policy">
            <div className="quick_link">
              <div id="sidebar-left">
                <div>
                  <a className="quick_head sub_menu_head" href="#section1">
                    Overview
                  </a>
                </div>
                <div>
                  <a className="quick_head sub_menu_head" href="#section2">
                    How Corona Kavach Policy works?
                  </a>
                </div>
                <div>
                  <a className="quick_head sub_menu_head" href="#section3">
                    Coverage Details
                  </a>
                </div>
                <div>
                  <a className="quick_head sub_menu_head" href="#section4">
                    Contact Details
                  </a>
                </div>
              </div>
            </div>

            <div
              style={{
                margin: "10px 0px",
                height: "0px",
                border: "0.5px solid rgba(0, 0, 0, 0.1)",
              }}
            ></div>

            <div id="section1">
              <div className="sub_head pad-t10">Overview</div>
              <div className="txt_styel pad-tb10">
                The Corona Kavach policy covers medical expenses incurred by the
                insured or family members due to treatment towards positive
                COVID 19. This policy is applicable for the treatments taken
                within India only.
              </div>
              <div className="txt_styel pad-tb10">
                This is an exclusive policy for Covid19 treatments and comes
                with a host of added benefits in comparison to our existing
                Group Mediclaim Policy.
              </div>

              <div className="label_head">Eligible Members</div>
              <div className="head_content">
                Employee (Maximum age 65 Years)
              </div>
              <div className="head_content">Spouse (Maximum age 65 Years)</div>
              <div className="head_content">Parents (Maximum age 65 Years)</div>
              <div className="head_content">
                Children (Maximum age 25 Years)
              </div>

              <div className="label_head">Sum Insured</div>
              <div className="head_content">INR 3,00,000</div>

              <div className="label_head">Treatment Covers</div>
              <div className="head_content">
                <ul>
                  <li>Hospitalized and Treatment taken in hospital</li>
                  <li>
                    Home care treatment advised by duly Qualified Medical
                    Practitioners
                  </li>
                </ul>
              </div>

              <div className="label_head">Third Party Administrator</div>
              <div className="head_content">
                FHPL Health Insurance TPA Pvt. Ltd.
              </div>
            </div>

            <div
              style={{
                margin: "10px 0px",
                height: "0px",
                border: "0.5px solid rgba(0, 0, 0, 0.1)",
              }}
            ></div>

            <div id="section2">
              <div className="sub_head pad-t5">
                How Corona Kavach Policy works?
              </div>

              <div>
                <div
                  className="policy_head_org"
                  onClick={() => setIsClickedPlanned(!isClickedPlanned)}
                >
                  Planned Hospitalization
                </div>
                {isClickedPlanned ? (
                  <div className="policy_cnt1">
                    <p>
                      <div style={{ fontWeight: "bold", color: "black" }}>
                        Get started{" "}
                      </div>
                      Employee Approaches Network Hospital with E-card (along
                      with valid photo ID) well in advance.
                    </p>
                    <p>
                      Hospital intimates FHPL TPA and sends Pre-Authorization
                      request with approximate cost of the treatment for
                      approval
                    </p>
                    <p>
                      FHPL TPA issues letter of credit (for cashless) with
                      approval for partial / full amount as per eligibility and
                      coverage to the hospital
                    </p>
                    <p>
                      At the time of Discharge hospital sends the final bill and
                      the discharge summary to FHPL TPA for the final approval
                    </p>
                    <p>
                      Hospital sends complete set of claims documents for
                      processing to FHPL TPA
                    </p>
                    <p style={{ border: "0px" }}>
                      FHPL TPA sends the final approval to hospital, which
                      allows the employee to get discharged by paying all
                      non-medical / non-payable expenses
                    </p>
                    <div className="note_txt pad-tb5">
                      **Follow reimbursement process in case of any non approval
                      from FHPL TPA
                    </div>
                  </div>
                ) : (
                  <div>{null}</div>
                )}
              </div>
              <div>
                <div
                  className="policy_head_org"
                  onClick={() => setIsClickedEmergency(!isClickedEmergency)}
                >
                  Emergency Hospitalization
                </div>

                {isClickedEmergency ? (
                  <div className="policy_cnt2">
                    <p>
                      Member get admitted in the hospital in case of emergency
                      by showing his E-Card (along with valid photo ID).
                      Treatment starts
                    </p>
                    <p>
                      Member / Hospital applies for pre-authorization to FHPL
                      TPA within 24 hrs of hospitalization
                    </p>
                    <p>
                      FHPL TPA verifies admissibility of the claim to be
                      registered and issues pre-authorization
                    </p>
                    <p>
                      Member gets treated and discharged after paying all
                      non-medical / non-payable expenses
                    </p>
                    <p>
                      Hospital sends complete set of claims documents for
                      processing to FHPL TPA
                    </p>
                    <p>Claims Processing by FHPL TPA & Insurer</p>
                    <p style={{ border: "0px" }}>
                      Release of payments to the hospital
                    </p>
                    <div className="note_txt pad-tb5">
                      **Follow reimbursement process if pre-authorization fail
                      on FHPL
                    </div>
                  </div>
                ) : (
                  <div>{null}</div>
                )}
              </div>
              <div>
                <div
                  className="policy_head_org"
                  onClick={() =>
                    setIsClickedReimbursement(!isClickedReimbursement)
                  }
                >
                  Hospitalization Reimbursement
                </div>

                {isClickedReimbursement ? (
                  <div className="policy_cnt3">
                    <p>
                      Employee submits all the documents to FHPL TPA within 30
                      days from discharge from the hospital along with a
                      cancelled cheque
                    </p>
                    <p>
                      FHPL TPA will check for the eligibility and admissibility
                      of the claim as per policy terms
                    </p>
                    <p>FHPL TPA will ask for the documents</p>
                    <p>Employee submits the documents to Medi Assist</p>
                    <p>
                      FHPL TPA will assess the complete claim documents for
                      admissibility as per policy terms
                    </p>
                    <p style={{ border: "0px" }}>
                      HPL TPA will process the claim and send the reimbursement
                      via EFT to employee account
                    </p>
                  </div>
                ) : (
                  <div>{null}</div>
                )}
              </div>
              <div>
                <div
                  className="policy_head_org"
                  onClick={() => setIsClickedHomeCare(!isClickedHomeCare)}
                >
                  Home Care Treatment - Positive COVID19
                </div>
                {isClickedHomeCare ? (
                  <div className="policy_cnt4">
                    <p>
                      The Medical Practitioner advises the Insured Person to
                      undergo treatment at home
                    </p>
                    <p>
                      There is a continuous active line of treatment with
                      monitoring of the health status by a medical practitioner
                      for each day through the duration of the home care
                      treatment
                    </p>
                    <p>
                      Daily monitoring chart including records of treatment
                      administered duly signed by the treating doctor is
                      maintained
                    </p>
                    <p>
                      Insured shall be permitted to avail the services as
                      prescribed by the Medical Practitioner Cashless facility
                      shall be offered under home care expenses if the treatment
                      is through a network provider
                    </p>
                    <p style={{ border: "0px" }}>
                      In case the insured intends to avail the services of
                      non-network provider claim shall be subject to
                      reimbursement, a prior approval from the Insurer / TPA
                      needs to be taken before availing such services.
                    </p>
                    <div className="note_txt pad-tb5">
                      **Home quarantine will not be covered under policy where
                      normal medication like only medicines taken without any
                      active line of treatment.
                    </div>
                  </div>
                ) : (
                  <div>{null}</div>
                )}
              </div>
            </div>

            <div
              style={{
                margin: "10px 0px",
                height: "0px",
                border: "0.5px solid rgba(0, 0, 0, 0.1)",
              }}
            ></div>

            <div id="section3">
              <div className="sub_head pad-b10">Coverage Details</div>

              <div className="label_head">Treatment Type covers</div>
              <div className="head_content">
                Allopathy, Ayurveda, Yoga, Naturopathy, Unani, Siddha,
                Homeopathy and Ayush treatments as in-patient care under Govt.
                authorized hospitals only
              </div>

              <div className="label_head">Policy Cancellation</div>
              <div className="head_content">
                Yes, if material facts not disclosed
              </div>

              <div className="label_head">
                Time Line for Documents Submission
              </div>
              <div className="head_content">
                <ul>
                  <li>
                    Within 15 days from completion of treatment for
                    Reimbursement of Post-Hospitalization expenses
                  </li>
                  <li>
                    Within 30 days from completion of treatment for
                    Reimbursement of Home Care expenses
                  </li>
                </ul>
              </div>

              <div className="label_head">Co-Morbidities</div>
              <div className="head_content">
                Covered (Diabetes, Respiratory Diseases like Asthma, COPD etc..,
                Cancer, Hypertension, Lung Fibrosis, HIV, Cardiovascular
                Diseases (Heart related ailments), Stroke and Circulatory
                Diseases, Auto Immune Diseases
              </div>

              <div>
                <div
                  className="policy_head_grn"
                  onClick={() => setIsClickedHC(!isClickedHC)}
                >
                  Homecare
                </div>

                {isClickedHC ? (
                  <div>
                    <div className="label_head">Home Care Treatment</div>
                    <div className="head_content">Maximum of 14 Days</div>

                    <div className="label_head">
                      Home Care Treatment includes
                    </div>
                    <div className="head_content">
                      <ul>
                        <li>
                          Diagnostic tests undergone at home or at diagnostics
                          centre
                        </li>
                        <li>Medicines prescribed in writing</li>
                        <li>
                          Consultation charges either online or offline of the
                          medical practitioner
                        </li>
                        <li>Nursing charges related to medical staff</li>
                        <li>
                          Medical procedures limited to parenteral
                          administration of medicines
                        </li>
                        <li>
                          Cost of Pulse Oximeter, Oxygen cylinder and Nebulizer
                        </li>
                      </ul>
                    </div>
                  </div>
                ) : (
                  <div>{null}</div>
                )}
              </div>

              <div>
                <div
                  className="policy_head_grn"
                  onClick={() =>
                    setIsClickedHospitalization(!isClickedHospitalization)
                  }
                >
                  Hospitalization
                </div>
                {isClickedHospitalization ? (
                  <div>
                    <div className="label_head">
                      Pre & Post Hospitalization Expenses Covered?
                    </div>
                    <div className="head_content">
                      Yes, Upto 15 days & 30 days
                    </div>

                    <div className="label_head">Road Ambulance</div>
                    <div className="head_content">Covered up to INR 2,000</div>
                  </div>
                ) : (
                  <div>{null}</div>
                )}
              </div>
            </div>

            <div
              style={{
                margin: "10px 0px",
                height: "0px",
                border: "0.5px solid rgba(0, 0, 0, 0.1)",
              }}
            ></div>

            <div id="section4">
              <div className="sub_head pad-b10">Contact Details</div>

              <div className="cnt_head">Toll Free Number</div>
              <div className="cnt_txt">18001024033</div>

              <div className="cnt_head pad-t10 pad-b5">Broker Details - 1</div>
              <div className="cnt_txt">Ravi Kumar B</div>
              <div className="cnt_txt">Manager-Employee benefits</div>
              <div className="cnt_txt">Ravikumar.B@willistowerswatson.co</div>
              <div className="cnt_txt">Mobile : 9739993444</div>

              <div className="cnt_head pad-t10 pad-b5">Broker Details - 1</div>
              <div className="cnt_txt">Raviraj Bhat</div>
              <div className="cnt_txt">Raviraj.bhat@willistowerswatson.com</div>
              <div className="cnt_txt">Mobile : 9900101541</div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}

export default CoronaPolicyScreen;