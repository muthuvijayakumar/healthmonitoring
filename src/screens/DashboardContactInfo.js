import React, { useState, useEffect } from "react";
import "./css/DashboardContactInfo.css";
import { useHistory } from "react-router";
import { STRING, IMAGES, COLORS } from "../utils/constants";
import { useDispatch, useSelector } from "react-redux";
import API from "../utils/api/API";

export default function DashboardContactInfo() {
  const history = useHistory();
  const [currentLoc, setCurrentLoc] = useState("");
  const [altPhnNo1, setAltPhnNo1] = useState("");
  const [altPhnNo2, setAltPhnNo2] = useState("");
  const [emergencyConName, setEmergencyConName] = useState("");
  const [emergencyConNo, setEmergencyConNo] = useState("");
  const [emergencyConRelation, setEmergencyConRelation] = useState("");
  const [PrimaryConNo, setPrimaryConNo] = useState("");
  const [isEditBtnClicked, setIsEditBtnClicked] = useState(false);
  const data = useSelector((state) => state.dashboardSum);
  const [err, setERR] = useState("");

  const empID = useSelector((state) => state.empID);
  const Id = useSelector((state) => state.uniqueID);
  const _Id = useSelector((state) => state._uniqueID);

  const empName = useSelector((state) => state.empName);
  const empMail = useSelector((state) => state.empMail);
  const empACName = useSelector((state) => state.empAccountName);
  const empGender = useSelector((state) => state.gender);
  const empdob = useSelector((state) => state.dob);
  const empHrbpName = useSelector((state) => state.hrbpName);
  const empWorkLocation = useSelector((state) => state.workLocation);
  const jwt = useSelector((state) => state.jwt);
  const roles = useSelector((state) => state.roles);
  const initialEmpData = useSelector((state) => state.initalEmpData);

  function OnClickBackBtn() {
    history.push("/dashboardUserProfile");
  }
  function getLocation(val) {
    setCurrentLoc(val.target.value);
  }

  function getAltPhnNo(val) {
    const re = /^[0-9\b]+$/;
    if (val.target.value === "" || re.test(val.target.value)) {
      setAltPhnNo1(val.target.value);
    }
  }
  function getAltPhnNo2(val) {
    const re = /^[0-9\b]+$/;
    if (val.target.value === "" || re.test(val.target.value)) {
      setAltPhnNo2(val.target.value);
    }
  }
  function getEmergencyConName(val) {
    setEmergencyConName(val.target.value);
  }
  function getEmergencyConNo(val) {
    const re = /^[0-9\b]+$/;
    if (val.target.value === "" || re.test(val.target.value)) {
      setEmergencyConNo(val.target.value);
    }
  }
  function getEmergencyConRelation(val) {
    setEmergencyConRelation(val.target.value);
  }
  function getPrimaryConNo(val) {
    const re = /^[0-9\b]+$/;
    if (val.target.value === "" || re.test(val.target.value)) {
      setPrimaryConNo(val.target.value);
    }
  }

  function onFocus() {
    setERR("");
  }

  function OnClickEditBtn() {
    setIsEditBtnClicked(true);
  }

  function OnClickCancelBtn() {
    setIsEditBtnClicked(false);
    setCurrentLoc(data.employeeDetails.currentLocation);

    console.log(data.employeeDetails.alternativeContactNo1 === 0);
    if (data.employeeDetails.alternativeContactNo1 === 0) {
      setAltPhnNo1("");
    } else {
      setAltPhnNo1(data.employeeDetails.alternativeContactNo1);
    }
    if (data.employeeDetails.alternativeContactNo2 === 0) {
      setAltPhnNo2("");
    } else {
      setAltPhnNo1(data.employeeDetails.alternativeContactNo2);
    }

    setEmergencyConName(data.employeeDetails.emergencyContactName);
    setEmergencyConNo(data.employeeDetails.emergencyContactNumber);
    setPrimaryConNo(data.employeeDetails.primaryContactNo);
    setEmergencyConRelation(data.employeeDetails.emergencyContactRelation);
  }
  function OnClickSaveBtn() {
    if (
      currentLoc !== null &&
      currentLoc !== "" &&
      emergencyConName !== null &&
      emergencyConName !== "" &&
      emergencyConNo !== null &&
      emergencyConNo !== "" &&
      emergencyConRelation !== null &&
      emergencyConRelation !== "" &&
      PrimaryConNo !== null &&
      PrimaryConNo !== "" &&
      PrimaryConNo !== undefined
    ) {
      let tempAltNo, tempAltNo2;
      console.log(altPhnNo1);

      if (altPhnNo1 === "" && altPhnNo1 !== null) {
        tempAltNo = 0;
      } else {
        tempAltNo = altPhnNo1;
      }

      if (altPhnNo2 === "" && altPhnNo2 !== null) {
        tempAltNo2 = 0;
      } else {
        tempAltNo2 = altPhnNo2;
      }

      if (data.employeeDetails.empName === empName) {
        API.put(
          process.env.REACT_APP_EMPLOYEE_DETAILS + Id,
          {
            _id: `${_Id}`,
            accountName: `${empACName}`,
            alternativeContactNo1: tempAltNo,
            alternativeContactNo2: tempAltNo2,
            currentLocation: `${currentLoc}`,
            dob: `${empdob}`,
            emailAddr: `${empMail}`,
            emergencyContactName: `${emergencyConName}`,
            emergencyContactNumber: emergencyConNo,
            emergencyContactRelation: `${emergencyConRelation}`,
            empId: empID,
            empName: `${empName}`,
            gender: `${empGender}`,
            hrbpName: `${empHrbpName}`,
            id: `${Id}`,
            isLocationUpdated: true,
            primaryContactNo: PrimaryConNo,
            workLocation: `${empWorkLocation}`,
            roles: `${roles}`,
          },
          {
            headers: {
              Authorization: `Bearer ${jwt}`,
            },
          }
        )
          .then((res) => {
            console.log(res);
            setIsEditBtnClicked(false);
            history.push("/dashboardUserProfile");
          })
          .catch((err) => {
            history.push("/");

            console.log(err.res);
            setERR("Internal Error");
            console.log(err);
          });
      } else {
        API.put(
          process.env.REACT_APP_EMPLOYEE_DETAILS + data.employeeDetails.id,
          {
            _id: data.employeeDetails._id,
            accountName: data.employeeDetails.accountName,
            alternativeContactNo1: tempAltNo,
            alternativeContactNo2: tempAltNo2,
            currentLocation: `${currentLoc}`,
            dob: data.employeeDetails.dob,
            emailAddr: data.employeeDetails.emailAddr,
            emergencyContactName: `${emergencyConName}`,
            emergencyContactNumber: emergencyConNo,
            emergencyContactRelation: `${emergencyConRelation}`,
            empId: data.employeeDetails.empId,
            empName: data.employeeDetails.empName,
            gender: data.employeeDetails.gender,
            hrbpName: data.employeeDetails.hrbpName,
            id: data.employeeDetails.id,
            isLocationUpdated: true,
            primaryContactNo: PrimaryConNo,
            workLocation: data.employeeDetails.workLocation,
            roles: data.employeeDetails.roles,
          },
          {
            headers: {
              Authorization: `Bearer ${jwt}`,
            },
          }
        )
          .then((res) => {
            console.log(res);
            setIsEditBtnClicked(false);
            history.push("/dashboardUserProfile");
          })
          .catch((err) => {
            history.push("/");

            console.log(err.res);
            setERR("Internal Error");
            console.log(err);
          });
      }
    } else {
      setERR("Please provide all the details");
    }
  }

  useEffect(() => {
    setCurrentLoc(data.employeeDetails.currentLocation);

    console.log(data.employeeDetails.alternativeContactNo1 === 0);
    if (data.employeeDetails.alternativeContactNo1 === 0) {
      setAltPhnNo1("");
    } else {
      setAltPhnNo1(data.employeeDetails.alternativeContactNo1);
    }
    if (data.employeeDetails.alternativeContactNo2 === 0) {
      setAltPhnNo2("");
    } else {
      setAltPhnNo1(data.employeeDetails.alternativeContactNo2);
    }

    setEmergencyConName(data.employeeDetails.emergencyContactName);
    setEmergencyConNo(data.employeeDetails.emergencyContactNumber);
    setPrimaryConNo(data.employeeDetails.primaryContactNo);
    setEmergencyConRelation(data.employeeDetails.emergencyContactRelation);
    console.log(data.employeeDetails);

    return () => {};
  }, []);

  return (
    <div className="page-container">
      <div className="content-wrap">
        <div style={{ height: "94%" }} className="contactInfo_container">
          <div id="contactInfo">
            <div className="backbtn_block">
              <div onClick={OnClickBackBtn} className="backbtn_img">
                <img src="/images/Black Back Button.png" alt="back btn" />
              </div>
              <div className="backbtn_txt">Contact Info</div>

              <div>
                {isEditBtnClicked ? (
                  <div>{null}</div>
                ) : (
                  <div onClick={OnClickEditBtn} className="backbtn_link">
                    <span className="check_edit_btn pad-lr10 float_r">
                      Edit
                    </span>
                  </div>
                )}
              </div>
            </div>

            <div className="contactInfo_body">
              <div className="label_head pad-t10">
                Current Location{" "}
                <span style={{ color: COLORS.ERR_CLR }}>*</span>
              </div>

              <input
                className="head_content head_content_edit"
                style={{
                  borderWidth: 1,
                  borderRadius: 3,
                  fontSize: "15px",
                  backgroundColor: COLORS.WHITE,
                  paddingLeft: "15px",
                  width: "98%",
                  height: "30px",
                }}
                disabled={!isEditBtnClicked}
                onFocus={onFocus}
                value={currentLoc}
                placeholder="Enter Current Location"
                type="text"
                onChange={getLocation}
              ></input>

              <div className="label_head pad-t10">
                Primary Contact Number{" "}
                <span style={{ color: COLORS.ERR_CLR }}>*</span>
              </div>

              <input
                className="head_content head_content_edit"
                style={{
                  borderWidth: 1,
                  borderRadius: 3,
                  fontSize: "15px",
                  backgroundColor: COLORS.WHITE,
                  paddingLeft: "15px",
                  width: "98%",
                  height: "30px",
                }}
                disabled={!isEditBtnClicked}
                onFocus={onFocus}
                value={PrimaryConNo}
                placeholder="Enter Primary Contact Number"
                type="text"
                onChange={getPrimaryConNo}
              ></input>

              <div className="label_head pad-t10">Alternate Contact Number</div>

              <input
                className="head_content head_content_edit"
                style={{
                  borderWidth: 1,
                  borderRadius: 3,
                  fontSize: "15px",
                  backgroundColor: COLORS.WHITE,
                  paddingLeft: "15px",
                  width: "98%",
                  height: "30px",
                }}
                disabled={!isEditBtnClicked}
                onFocus={onFocus}
                value={altPhnNo1}
                placeholder="Enter Alternate Contact Number"
                type="text"
                onChange={getAltPhnNo}
              ></input>

              <div className="label_head pad-t10">
                Alternate Contact Number 2
              </div>
              <input
                className="head_content head_content_edit"
                style={{
                  borderWidth: 1,
                  borderRadius: 3,

                  fontSize: "15px",
                  backgroundColor: COLORS.WHITE,
                  paddingLeft: "15px",
                  width: "98%",
                  height: "30px",
                }}
                disabled={!isEditBtnClicked}
                onFocus={onFocus}
                value={altPhnNo2}
                placeholder="Enter Alternate Contact Number"
                type="text"
                onChange={getAltPhnNo2}
              ></input>
              <div className="label_head pad-t10">
                Emergency Contact Name{" "}
                <span style={{ color: COLORS.ERR_CLR }}>*</span>
              </div>
              <input
                className="head_content head_content_edit"
                style={{
                  borderWidth: 1,
                  borderRadius: 3,
                  fontSize: "15px",
                  backgroundColor: COLORS.WHITE,
                  paddingLeft: "15px",
                  width: "98%",
                  height: "30px",
                }}
                disabled={!isEditBtnClicked}
                onFocus={onFocus}
                value={emergencyConName}
                placeholder="Enter Emergency Contact Name"
                type="text"
                onChange={getEmergencyConName}
              ></input>
              <div className="label_head pad-t10">
                Emergency Contact Relation{" "}
                <span style={{ color: COLORS.ERR_CLR }}>*</span>
              </div>
              <input
                className="head_content head_content_edit"
                style={{
                  borderWidth: 1,
                  borderRadius: 3,
                  fontSize: "15px",
                  backgroundColor: COLORS.WHITE,
                  paddingLeft: "15px",
                  width: "98%",
                  height: "30px",
                }}
                disabled={!isEditBtnClicked}
                onFocus={onFocus}
                placeholder="Enter Emergency Contact Relation"
                type="text"
                value={emergencyConRelation}
                onChange={getEmergencyConRelation}
              ></input>

              <div className="label_head pad-t10">
                Emergency Contact Number{" "}
                <span style={{ color: COLORS.ERR_CLR }}>*</span>
              </div>
              <input
                className="head_content head_content_edit"
                style={{
                  borderWidth: 1,
                  borderRadius: 3,
                  fontSize: "15px",
                  backgroundColor: COLORS.WHITE,
                  paddingLeft: "15px",
                  width: "98%",
                  height: "30px",
                }}
                disabled={!isEditBtnClicked}
                onFocus={onFocus}
                value={emergencyConNo}
                placeholder="Enter Emergency Contact Number"
                type="text"
                onChange={getEmergencyConNo}
              ></input>
            </div>

            <h5
              style={{
                textAlign: "left",
                paddingTop: "5px",
                color: COLORS.ERR_CLR,
                fontFamily: "Inter",
                fontStyle: "normal",
                fontSize: "12px",
                margin: "5px",
              }}
            >
              {err}
            </h5>

            {isEditBtnClicked ? (
              <div
                style={{
                  display: "flex",
                  flexDirection: "row",
                  justifyContent: "center",
                  paddingTop: "20px",
                }}
              >
                <div style={{ margin: "10px" }} onClick={OnClickCancelBtn}>
                  <span
                    style={{ backgroundColor: "#95979F" }}
                    className="check_edit_btn pad-lr10 "
                  >
                    Cancel
                  </span>
                </div>
                <div style={{ margin: "10px" }} onClick={OnClickSaveBtn}>
                  <span className="check_edit_btn pad-lr10 ">Save</span>
                </div>
              </div>
            ) : (
              <div>{null}</div>
            )}
          </div>
        </div>
      </div>
    </div>
  );
}
