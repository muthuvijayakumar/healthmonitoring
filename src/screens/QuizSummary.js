import React, { useEffect, useRef, useState } from "react";
import "./css/QuizSummary.css";
import { Button } from "../components/Button";
import { useDispatch, useSelector } from "react-redux";
import { useHistory } from "react-router";
import "react-datepicker/dist/react-datepicker.css";
import DatePicker from "react-datepicker";
import Checkbox from "react-custom-checkbox";
import { STRING, IMAGES, COLORS } from "../utils/constants";
import Modal from "react-modal";
import API from "../utils/api/API";
import Breadcrumbs from "../components/Breadcrumbs";
import Footer from "../components/Footer";
import moment from "moment";
import { Notes } from "../components/Notes";
import { AddNotes } from "../components/AddNotes";
import { EditNotes } from "../components/EditNotes";
import { ProfileCard } from "../components/ProfileCard";

Modal.setAppElement("#root");

export default function QuizSummary() {
  const [modalOpen, setmodalOpen] = useState(false);
  const [loading, setLoading] = useState(true);
  const [editModalOpen, setEditModalOpen] = useState(false);
  const [dummyData, setDummyData] = useState("");
  const [editNotesModalOpen, setEditNotesModalOpen] = useState(false);
  const [newNoteModalOpen, setNewNoteModalOpen] = useState(false);
  const [notesFromServer, setNotesFromServer] = useState([]);
  const [notesDescription, setNotesDescription] = useState("");
  const [notesData, setNotesData] = useState([]);

  const [id, setID] = useState([]);
  const [clicked, setClicked] = useState(false);

  const data = useSelector((state) => state.quizSum);
  const history = useHistory();
  const [questionIndex, setQuestionIndex] = useState(0);
  const [dataIndex, setDataIndex] = useState(0);
  const [err, setERR] = useState("");
  const empID = useSelector((state) => state.empID);
  const empName = useSelector((state) => state.empName);
  const role = useSelector((state) => state.roles);
  const assessmentType = useSelector((state) => state.assessmentType);
  const otherEmpDetail = useSelector((state) => state.otherEmpDetail);

  const jwt = useSelector((state) => state.jwt);

  function OpenEditModal(desc, data) {
    setNotesData(data);

    setNotesDescription(desc);
    setEditNotesModalOpen(true);
  }

  function OpenNewNoteModal() {
    setNewNoteModalOpen(true);
  }

  function SetNotesDes(val) {
    console.log(val.target.value);
    setNotesDescription(val.target.value);
  }

  function AddNotesToServer() {
    API.post(
      process.env.REACT_APP_EMPLOYEE_HRBP_NOTES_POST,
      {
        personId: otherEmpDetail.empId,
        createdBy: empID,
        updatedBy: empID,
        notes: notesDescription,
      },
      {
        headers: {
          Authorization: `Bearer ${jwt}`,
        },
      }
    )
      .then((res) => {
        console.log(res.data);
      })
      .catch((err) => {
        switch (err.response.status) {
          case 500:
            break;
          case 400:
            break;

          case 401:
            history.push("/");

            break;
          case 403:
            history.push("/");
            break;

          default:
            break;
        }

        throw err;
      });
  }

  function EditNotesToServer() {
    API.put(
      process.env.REACT_APP_EMPLOYEE_HRBP_NOTES_POST + "/" + notesData._id,
      {
        _id: notesData._id,
        personId: otherEmpDetail.empId,
        createdBy: notesData.createdBy,
        createdAt: notesData.createdAt,
        updatedBy: empID,
        notes: notesDescription,
      },
      {
        headers: {
          Authorization: `Bearer ${jwt}`,
        },
      }
    )
      .then((res) => {
        console.log(res.data);
      })
      .catch((err) => {
        switch (err.response.status) {
          case 500:
            break;
          case 400:
            break;

          case 401:
            history.push("/");

            break;
          case 403:
            history.push("/");
            break;

          default:
            break;
        }

        throw err;
      });
  }

  useEffect(() => {
    setLoading(false);
    console.log("comin ");

    if (role.includes("DASHBOARD") || role.includes("HRBP")) {
      API.get(
        process.env.REACT_APP_EMPLOYEE_HRBP_NOTES_GET + otherEmpDetail.empId,
        {
          headers: {
            Authorization: `Bearer ${jwt}`,
          },
        }
      )
        .then((res) => {
          console.log(res.data);
          setNotesFromServer(res.data);
          //setData(res.data[0].options);
        })
        .catch((err) => {
          switch (err.response.status) {
            case 500:
              break;
            case 400:
              break;

            case 401:
              history.push("/");

              break;
            case 403:
              history.push("/");
              break;

            default:
              break;
          }

          throw err;
        });
    }
    if (jwt === "") {
      history.push("/");
    }
    return () => {};
  }, []);

  function OnClickEdit(dataID) {
    setDataIndex(dataID);
    if (JSON.parse(data[dataID].data).quesType === "DATE") {
      setDummyData(new Date(data[dataID].answer));
    } else {
      setDummyData(data[dataID].answer);
    }

    setEditModalOpen(true);
  }

  function getData(val) {
    setDummyData(val.target.value);
  }

  function OnClickChangeCategoryButton() {
    setEditModalOpen(false);
    setmodalOpen(true);
  }

  function OnClickDiscardButton() {
    setEditModalOpen(false);
    setmodalOpen(false);
  }

  function OnClickChangeButton() {
    history.push("/home/healthStatus");
  }

  function OnClickSaveButton() {
    setERR("");

    console.log(JSON.parse(data[dataIndex].data).required);

    if (JSON.parse(data[dataIndex].data).required) {
      if (dummyData !== "" && dummyData !== null) {
        var regex = new RegExp(JSON.parse(data[dataIndex].data).regex);
        if (JSON.parse(data[dataIndex].data).regex !== null) {
          if (regex.test(dummyData)) {
            data[dataIndex].answer = dummyData;
            setDummyData("");
            setEditModalOpen(false);
            setmodalOpen(false);
          } else {
            setERR(JSON.parse(data[dataIndex].data).errorMessage);
          }
        } else {
          data[dataIndex].answer = dummyData;
          setDummyData("");
          setEditModalOpen(false);
          setmodalOpen(false);
        }
      } else {
        setERR("Cannot leave field empty");
      }
    } else {
      if (dummyData !== "" && dummyData !== null) {
        if (JSON.parse(data[dataIndex].data).regex !== null) {
          var regex = new RegExp(JSON.parse(data[dataIndex].data).regex);

          if (regex.test(dummyData)) {
            data[dataIndex].answer = dummyData;
            setDummyData("");
            setEditModalOpen(false);
            setmodalOpen(false);
          } else {
            setERR(JSON.parse(data[dataIndex].data).errorMessage);
          }
        } else {
          data[dataIndex].answer = dummyData;
          setDummyData("");
          setEditModalOpen(false);
          setmodalOpen(false);
        }
      } else {
        data[dataIndex].answer = dummyData;
        setDummyData("");
        setEditModalOpen(false);
        setmodalOpen(false);
      }
    }
  }

  function OnClickDoneBtn() {
    console.log(data);
    PostDataToServer();
  }

  const customStyles = {
    content: {
      top: "60%",
      left: "50%",
      right: "auto",
      bottom: "-40%",
      transform: "translate(-50%, -50%)",
      borderRadius: "10px",
      width: "82%",
    },
  };

  function onFocus() {
    setERR("");
  }

  function OnClickCheckBox(id, temp) {
    setDummyData(temp);
    setID(id);
    setClicked(!clicked);
  }

  function OnClickBackBtn() {
    history.push("/home/healthStatus/questions");
  }

  function PostDataToServer() {
    try {
      console.log(data);
      API.post(
        process.env.REACT_APP_EMPLOYEE_SURVEY_RESPONSE,
        {
          empId: assessmentType === "OTHERS" ? otherEmpDetail.empId : empID,
          response: JSON.stringify(data),
          surveyresponse: data,
          hrbpId: assessmentType === "OTHERS" ? empID : null,
          hrbpName: assessmentType === "OTHERS" ? empName : null,
          type: assessmentType,
        },
        {
          headers: {
            Authorization: `Bearer ${jwt}`,
          },
        }
      )
        .then((res) => {
          console.log("response post data");
          console.log(data);

          console.log(res.data);
          history.push("/thankyou");
        })
        .catch((err) => {
          switch (err.response.status) {
            case 500:
              break;
            case 400:
              break;

            case 401:
              history.push("/");

              break;
            case 403:
              history.push("/");
              break;

            default:
              break;
          }

          throw err;
        });
    } catch (err) {
      console.log(err);
    }
  }

  return (
    <div className="page-container">
      <div className="content-wrap">
        <div style={{ height: "94%" }} className="summaryContainer">
          <div
            style={{
              flexDirection: "row",
              display: "flex",
              paddingTop: "50px",
            }}
          >
            <img
              onClick={OnClickBackBtn}
              src="/images/Black Back Button.png"
              alt="back btn"
              style={{ width: "32px", height: "32px" }}
            ></img>
            <h3
              style={{
                color: "#1A1C22",
                fontFamily: "Inter",
                fontStyle: "normal",
                fontWeight: "600px",
                fontSize: "17px",
                paddingLeft: "10px",
                paddingTop: "5px",
                margin: "1px",
              }}
            >
              Summary
            </h3>
          </div>

          <Breadcrumbs></Breadcrumbs>

          <div style={{marginTop:"10px"}}>
          <ProfileCard />
          </div>

          {loading ? (
            "Loading"
          ) : (
            <div>
              {assessmentType === "OTHERS" ? (
                <div>
                  <div className="label_head pad-t10">Name</div>
                  <div className="head_content head_content_edit">
                    {otherEmpDetail.empName}
                  </div>
                  <div className="label_head pad-tb2">EMP ID</div>
                  <div className="head_content head_content_edit">
                    {otherEmpDetail.empId}
                  </div>
                  <div
                    style={{ height: "0px", border: "0.5px solid #E8EDF1" }}
                  ></div>
                </div>
              ) : (
                <div>{null}</div>
              )}
              {data.map((el, id) => (
                <div key={id} id="summary">
                  <div className="label_head pad-t10">{el.quesDesc}</div>
                  <div className="head_content head_content_edit">
                    {el.data
                      ? JSON.parse(data[id].data).quesType === "DATE"
                        ? new Date(el.answer).toLocaleDateString()
                        : el.answer
                      : el.answer}

                    <div className="edit_link">
                      <span
                        onClick={() => {
                          id === 0
                            ? OnClickChangeCategoryButton()
                            : OnClickEdit(id);
                        }}
                      >
                        {id === 0 ? "Change" : "Edit"}
                      </span>
                    </div>
                  </div>

                  <div
                    style={{ height: "0px", border: "0.5px solid #E8EDF1" }}
                  ></div>
                </div>
              ))}
            </div>
          )}

          {role.includes("DASHBOARD") || role.includes("HRBP") ? (
            <NotesShow></NotesShow>
          ) : (
            <div>{null}</div>
          )}
          <EditNotes
            notes={notesDescription}
            modalOpen={editNotesModalOpen}
            getNotesDescription={SetNotesDes}
            onclickClose={() => setEditNotesModalOpen(false)}
            onClickUpdate={EditNotesToServer}
          ></EditNotes>

          <AddNotes
            modalOpen={newNoteModalOpen}
            onclickClose={() => setNewNoteModalOpen(false)}
            getNotesDescription={SetNotesDes}
            onClickAdd={AddNotesToServer}
          ></AddNotes>

          <div style={{ padding: "15% 0" }}>
            <Button
              onClick={OnClickDoneBtn}
              style={{ width: "100%" }}
              children="Submit"
            ></Button>
          </div>
        </div>

        {editModalOpen ? (
          <Modal
            isOpen={editModalOpen}
            //onAfterOpen={afterOpenModal}
            //onRequestClose={editModalOpen}
            style={customStyles}
            shouldCloseOnOverlayClick={false}
            contentLabel="Example Modal"
          >
            <div>
              <div style={{ padding: "35px 0px 20px 0px" }}>
                <h5
                  style={{
                    color: "#1A1C22",
                    fontFamily: "Inter",
                    fontStyle: "normal",
                    fontWeight: "600px",
                    fontSize: "15px",
                    textAlign: "left",
                    width: "100%",
                  }}
                >
                  {data[dataIndex].quesDesc}
                  {JSON.parse(data[dataIndex].data).required ? (
                    <span style={{ color: COLORS.ERR_CLR }}>*</span>
                  ) : (
                    <div>{null}</div>
                  )}
                </h5>
              </div>

              <div style={{ display: "flex", flexDirection: "column" }}>
                {JSON.parse(data[dataIndex].data).quesType === "TEXT" ? (
                  <div>
                    <input
                      className="InputField"
                      style={{
                        borderWidth: 1,
                        borderRadius: 3,
                        fontSize: "15px",
                        backgroundColor: COLORS.WHITE,
                        paddingLeft: "2%",
                        width: "98%",
                        height: "44px",
                      }}
                      value={dummyData}
                      placeholder={data[dataIndex].answer}
                      onChange={getData}
                      onFocus={onFocus}
                      type="text"
                    ></input>
                  </div>
                ) : (
                  <div>{null}</div>
                )}

                {JSON.parse(data[dataIndex].data).quesType === "DATE" ? (
                  <div>
                    <DatePicker
                      placeholderText={data[dataIndex].answer}
                      selected={dummyData}
                      onChange={(date) => setDummyData(date)}
                    />
                  </div>
                ) : (
                  <div>{null}</div>
                )}

                {JSON.parse(data[dataIndex].data).quesType === "CHECKBOX" ? (
                  <div>
                    {JSON.parse(data[dataIndex].data).options.map((el) => (
                      <div
                        style={{
                          padding: " 12px 5px 12px 10px",
                          textAlign: "left",
                          width: "98%",
                          height: "10%",
                          backgroundColor: COLORS.WHITE,
                          marginBottom: "10px",
                        }}
                        key={el.id}
                      >
                        <Checkbox
                          icon={
                            <img
                              src={IMAGES.PATH}
                              style={{ width: "10px", height: "10px" }}
                            />
                          }
                          name="my-input"
                          checked={dummyData.includes(el.optValue)}
                          onChange={(value) => {
                            if (value) {
                              if (dummyData.includes(el.optValue)) {
                              } else {
                                setDummyData(el.optValue + "," + dummyData);
                              }
                            } else {
                              var str = dummyData;
                              var new_str = str.replace(el.optValue + ",", "");
                              setDummyData(new_str);
                            }
                          }}
                          borderColor="#4D59FC"
                          style={{
                            cursor: "pointer",
                            backgroundColor: "#4D59FC",
                          }}
                          labelStyle={{
                            marginLeft: "10px",
                            userSelect: "none",
                          }}
                          label={el.optValue}
                          right={false}
                        />
                      </div>
                    ))}
                  </div>
                ) : (
                  <div>{null}</div>
                )}

                {JSON.parse(data[dataIndex].data).quesType === "RADIO" ? (
                  <div>
                    {JSON.parse(data[dataIndex].data).options.map((el) => (
                      <button
                        style={{
                          width: "100%",
                          height: "6%",
                          padding: "15px",
                          marginBottom: "10px",
                          border: "0px",
                          backgroundColor: COLORS.WHITE,
                          fontSize: "15px",
                          lineHeight: "18px",
                          borderRadius: "5px",
                          fontWeight: "600",
                          textAlign: "left",
                          color: "#252A31",
                        }}
                        key={el.id}
                        onClick={() => OnClickCheckBox(el.id, el.optValue)}
                      >
                        {console.log(dummyData)}
                        {dummyData === el.optValue ? (
                          <img
                            style={{
                              width: "16px",
                              height: "16px",
                              marginRight: "10px",
                              float: "right",
                            }}
                            src={IMAGES.Radio}
                          />
                        ) : (
                          <div>{null}</div>
                        )}

                        {/* {id === el.id ? (
                          <img
                            style={{
                              width: "16px",
                              height: "16px",
                              marginRight: "10px",
                              float: "right",
                            }}
                            src={IMAGES.Radio}
                          />
                        ) : (
                          <div>{null}</div>
                        )} */}
                        {el.optValue}
                      </button>
                    ))}
                  </div>
                ) : (
                  <div>{null}</div>
                )}
              </div>

              <h5
                style={{
                  textAlign: "left",
                  paddingTop: "5px",
                  color: COLORS.ERR_CLR,
                  fontFamily: "Inter",
                  fontStyle: "normal",
                  fontSize: "12px",
                  margin: "5px",
                }}
              >
                {err}
              </h5>

              <div style={{ padding: "20px 0px" }}>
                <Button
                  onClick={OnClickSaveButton}
                  style={{ width: "100%" }}
                  children="Save"
                ></Button>
              </div>
            </div>
          </Modal>
        ) : (
          <div>{null}</div>
        )}

        {modalOpen ? (
          <Modal
            isOpen={modalOpen}
            //onAfterOpen={afterOpenModal}
            //onRequestClose={editModalOpen}
            style={customStyles}
            shouldCloseOnOverlayClick={false}
          >
            <div>
              <div className="popup_txt pad-t10">
                Do you really want to change health status?
              </div>

              <div style={{ padding: "20px 0px" }}>
                <Button
                  style={{ width: "100%" }}
                  onClick={OnClickChangeButton}
                  children="Change"
                ></Button>
              </div>

              <div>
                <a
                  href="#"
                  className="change_button"
                  style={{ width: "100%" }}
                  children="Discard"
                  onClick={OnClickDiscardButton}
                >
                  Cancel
                </a>
              </div>
            </div>
          </Modal>
        ) : null}
      </div>
      <Footer></Footer>
    </div>
  );

  function NotesShow() {
    return (
      <div>
        <div
          style={{
            paddingTop: "30px",
            margin: "auto",
            maxWidth: "650px",
            borderRadius: "5px",
          }}
        >
          <span
            style={{
              color: "#1A1C22",
              fontFamily: "Inter",
              fontStyle: "normal",
              fontWeight: "600",
              fontSize: "17px",
              paddingLeft: "10px",
              paddingTop: "5px",
            }}
          >
            HRBP Notes
          </span>

          <button
            onClick={OpenNewNoteModal}
            className="float_r"
            style={{
              backgroundColor: "#4D59FC",
              color: "#fff",
              fontFamily: "Inter",
              fontStyle: "normal",
              fontWeight: "500",

              height: "28px",
              fontSize: "15px",
              lineHeight: "20px",
              justifyContent: "center",
              alignContent: "center",
              alignItems: "center",
              borderRadius: "50px",
              borderColor: "transparent",
            }}
          >
            New Note
          </button>
        </div>
        <div className="CategoryWithoutPadding pad-t20">
          {notesFromServer.length > 0 ? (
            notesFromServer.map((el, id) => (
              <Notes
                key={id}
                onEditClick={() => OpenEditModal(el.notes, el)}
                header={
                  el.updatedByName +
                  "posted on " +
                  moment(el.updatedAt, "YYYY-MM-DDTHH:mm:ss.000ZZ").fromNow()
                }
                description={el.notes}
              ></Notes>
            ))
          ) : (
            <div style={{ paddingTop: "30px", textAlign: "center" }}>
              no notes to display
            </div>
          )}{" "}
        </div>
      </div>
    );
  }
}
