import React, { useState, useEffect } from "react";
import { Button } from "../components/Button";
import "./css/WelcomeScreen.css";
import { useHistory } from "react-router-dom";
import Footer from "../components/Footer";
import { useSelector, useDispatch } from "react-redux";
import { SetDetails } from "../redux/action";
import YouTube from "react-youtube";
import moment from "moment";
import Modal from "react-modal";
Modal.setAppElement("#root");

import { useMsal } from "@azure/msal-react";
import { loginRequest } from "../sso_components/authConfig";

import API from "../utils/api/API";
import { STRING, IMAGES, COLORS } from "../utils/constants";
export default function WelcomeScreen() {
  const [data, setData] = useState("");
  const [err, setERR] = useState("");

  const empID = useSelector((state) => state.empID);
  const dispatch = useDispatch();
  const history = useHistory();
  const [modalOpen, setmodalOpen] = useState(false);

  const emailCheck =
    /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  const { instance, accounts, inProgress } = useMsal();

  const customStyles = {
    content: {
      top: "95%",
      left: "50%",
      right: "auto",
      bottom: "-40%",
      transform: "translate(-50%, -50%)",
      borderRadius: "10px",
      width: "82%",
    },
  };

  function CloseModal() {
    setmodalOpen(false);
  }

  function getEmpID(val) {
    setData(val.target.value.toLowerCase());
  }

  async function onNextClicked(token) {
    let error;
    try {
      console.log(token);

      await API.post(
        process.env.REACT_APP_EMPLOYEE_SESSION,
        {},
        {
          headers: {
            Accept: "application/json",
            "Content-Type": "application/json",
            Authorization: `Bearer ${token}`,
          },
        }
      )
        .then((res) => {
          let serverData = res.data.employeedetails;

          console.log(res.data);

          dispatch(
            SetDetails(
              serverData.empId,
              serverData.emailAddr,
              serverData.empName,
              serverData.accountName,
              serverData.dob,
              serverData.gender,
              serverData.hrbpName,
              serverData.workLocation,
              serverData.id,
              serverData._id,
              serverData.isLocationUpdated,
              serverData.roles,
              res.data.jwt,
              res.data
            )
          );
          history.push("/home");
        })
        .catch((err) => {
          console.log(err.response.status);
          if (err.response.status === 500) {
            setERR("Please try again later.");
          }
          if (err.response.status === 404) {
            setERR(
              "Oops. We could not find your Employee Email ID.please try again"
            );
            throw new Error(`${err.config.url} not found`);
          }

          if (err.response.status === 400) {
            setERR("400");
            throw new Error(`${err.config.url} not found`);
          }

          if (err.response.status === 401) {
            setERR(
              "Contact admin, employee details not updated in covid support application"
            );
            throw new Error(`${err.config.url} not found`);
          }

          throw err;
        });
    } catch (err) {
      error = err;
    }
  }

  function OnChangeFocus() {
    setERR("");
  }

  const opts = {
    playerVars: {
      // https://developers.google.com/youtube/player_parameters
      autoplay: 0,
    },
  };

  function _onReady(event) {
    // access to player in all event handlers via event.target
    event.target.pauseVideo();
  }

  let accessToken = null;

  useEffect(() => {
    console.log(
      moment(
        "2021-05-06T09:44:42.845+00:00",
        "YYYY-MM-DD hh:mm:ss A Z"
      ).fromNow()
    );

    if (
      inProgress === "none" &&
      accounts.length > 0 &&
      data !== "" &&
      data !== null
    ) {
      // Retrieve an access token
      accessToken = instance
        .acquireTokenSilent({
          account: accounts[0],
          scopes: ["User.Read"],
        })
        .then((response) => {
          if (response.accessToken) {
            if (accounts[0].username.toLowerCase() === data) {
              onNextClicked(response.accessToken);
            } else {
              setERR("Please use the correct email address");
            }

            return response.accessToken;
          }
          return null;
        });
    }
  }, [inProgress, accounts, instance]);

  const handleLogin = (loginType) => {
    if (emailCheck.test(data)) {
      if (data.includes("@altimetrik.com")) {
        if (loginType === "popup") {
          instance.loginPopup(loginRequest).catch((e) => {
            console.log(e);
          });
        } else if (loginType === "redirect") {
          instance.loginRedirect(loginRequest).catch((e) => {
            console.log(e);
          });
        }
      } else {
        setERR("Use your Altimetrik email address");
      }
    } else {
      setERR("Invalid email address");
    }
  };
  return (
    <div className="page-container">
      <div className="content-wrap">
        <div style={{ height: "94%" }} className="WelcomeContainer">
        <span className="float_r">
            <img
              onClick={() => setmodalOpen(true)}
              src="/images/help.png"
              alt="Image"
            />
          </span>
        <div
          style={{
            textAlign: "center",
            padding: "30px 0px 10px 0px",
            borderRadius: "5px",
          }}
        >
          
          <img
            style={{ textAlign: "center" }}
            align="center"
            src="./images/logo.svg"
            alt="Altimetrik"
          ></img>
          <div className="app_name">COVID Care</div>
        </div>

          <h5
            style={{
              color: COLORS.TEXT_CLR,
              fontFamily: "Inter",
              fontStyle: "normal",
              fontWeight: "600px !important",
              fontSize: "13px",
              paddingBottom: "10px",
            }}
          >
            Enter your official email address to begin
          </h5>

          <input
            style={{
              borderWidth: 1,
              borderRadius: 3,
              fontSize: "15px",
              backgroundColor: "#fff",
              width: "98%",
              height: "6%",
              padding: "10px 0px 10px 2%",
              border: "1px solid gray",
            }}
            value={data}
            onFocus={OnChangeFocus}
            placeholder={STRING.EMAIL_PLACEHOLDER_TEXT}
            type="text"
            onChange={getEmpID}
          ></input>

          <h5
            style={{
              textAlign: "left",
              paddingTop: "5px",
              color: COLORS.ERR_CLR,
              fontFamily: "Inter",
              fontStyle: "normal",
              fontSize: "12px",
              margin: "5px",
            }}
          >
            {err}
          </h5>

          <div style={{ paddingBottom: "20px" }}>
            <Button
              onClick={() => handleLogin("popup")}
              children="Let’s Go"
            ></Button>
          </div>

          <div
            style={{
              height: "0px",
              border: "0.5px solid rgba(0, 0, 0, 0.1)",
            }}
          ></div>

          <div className="disclaimer_title">
            {STRING.Disclaimer.DISCLAIMER_TEXT}
          </div>
          <div className="disclaimer_list">
            <ul>
              <li>{STRING.Disclaimer.DISCLAIMER_TEXT_1}</li>
              <li>{STRING.Disclaimer.DISCLAIMER_TEXT_2}</li>
              <li>{STRING.Disclaimer.DISCLAIMER_TEXT_3}</li>
              <li>{STRING.Disclaimer.DISCLAIMER_TEXT_4}</li>
              <li>{STRING.Disclaimer.DISCLAIMER_TEXT_5}</li>
            </ul>
          </div>

          <Modal
            isOpen={modalOpen}
            //onAfterOpen={afterOpenModal}
            onRequestClose={CloseModal}
            style={customStyles}
            shouldCloseOnOverlayClick={true}
          >
            <div
              onClick={() => setmodalOpen(false)}
              className="whitelistbox_ctnt3"
            >
              <span className="check_edit_btn float_r">close</span>
            </div>
            <div>
              <div className="info_popup_txt pad-t10">
                If you encounter any difficulties while using this app, please
                write to us
              </div>

              <div className="pad-t20">
                <span className="change_button" style={{ width: "100%" }}>
                  <img
                    style={{ verticalAlign: "middle", padding: "1px" }}
                    src="./images/mail.svg"
                  ></img>{" "}
                  <a href="mailto:dex@altimetrik.com?subject=Support%20Request">
                    {" "}
                    dex@altimetrik.com{" "}
                  </a>
                </span>
              </div>
            </div>
          </Modal>
        </div>
      </div>
      <Footer></Footer>
    </div>
  );
}
