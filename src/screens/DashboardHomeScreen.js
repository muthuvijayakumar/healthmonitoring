import React, { useState, useEffect } from "react";
import { STRING, IMAGES, COLORS } from "../utils/constants";
import { Card } from "../components/card-component.jsx";
import "./css/DashboardHomeScreen.css";
import Footer from "../components/Footer";
import { HistoryCard } from "../components/HistoryCard";
import { useHistory } from "react-router";
import { TopNavBar } from "../components/TopNavBar";
import API from "../utils/api/API";
import { useDispatch, useSelector } from "react-redux";
import { SetDashboardUID, SetHealthAlert } from "../redux/action";
import moment from "moment";
import ToggleSwitch from "../components/ToggleSwitch";

export default function DashboardHomeScreen() {
  const history = useHistory();
  const [data, setData] = useState();
  const [loading, setLoading] = useState(true);
  const [dataHealthStatus, setDataHealthStatus] = useState(false);
  const [dataHealthAlerts, setDataHealthAlerts] = useState(false);
  const [dataHealthReport, setDataHealthReport] = useState(false);
  const dispatch = useDispatch();
  const jwt = useSelector((state) => state.jwt);

  function OnClickBackBtn() {
    history.push("/home");
  }
  const [val, setVal] = useState(0);

  function SetItemValue(value) {
    setVal(value);
    console.log(value);
  }

  useEffect(() => {
    setLoading(true);
    console.log(jwt);

    API.get(process.env.REACT_APP_EMPLOYEE_DASHBOARD, {
      headers: {
        Authorization: `Bearer ${jwt}`,
      },
    })
      .then((res) => {
        setData(res.data);

        setLoading(false);
      })
      .catch((err) => {
        switch (err.response.status) {
          case 500:
            break;
          case 400:
            break;

          case 401:
            history.push("/");

            break;
          case 403:
            history.push("/");
            break;

          default:
            break;
        }
        console.log(err);

        throw err;
      });
    return () => {
      setLoading(false);
    };
  }, []);

  return (
    <div className="page-container">
      {loading ? (
        "Loading"
      ) : (
        <div className="content-wrap">
          <div style={{ height: "94%" }} className="Dashboard">
            <div style={{ justifyContent: "center", alignItems: "center" }}>
              <div
                style={{
                  flexDirection: "row",
                  display: "flex",
                  paddingTop: "50px",
                }}
              >
                {console.log(data)}
                <img
                  src={IMAGES.BackBtn}
                  alt="back btn"
                  onClick={OnClickBackBtn}
                  style={{ width: "32px", height: "32px" }}
                ></img>
                <span
                  style={{
                    color: "#1A1C22",
                    fontFamily: "Inter",
                    fontStyle: "normal",
                    fontWeight: "600",
                    fontSize: "17px",
                    paddingLeft: "10px",
                    paddingTop: "5px",
                  }}
                >
                  Dashboard
                </span>
              </div>

              <span
                style={{
                  color: "#1A1C22",
                  fontFamily: "Inter",
                  fontStyle: "normal",
                  fontWeight: "bold",
                  fontSize: "17px",
                  paddingLeft: "10px",
                  paddingTop: "5px",
                  margin: "10px 0px",
                  display: "block",
                }}
              >
                Summary
              </span>

              <span
                style={{
                  color: "#000000",
                  fontFamily: "Inter",
                  fontStyle: "normal",
                  fontWeight: "700",
                  fontSize: "13px",
                  paddingLeft: "10px",
                  paddingTop: "5px",
                  margin: "10px 0px",
                  display: "block",
                }}
              >
                User Base
              </span>

              <div className="row">
                <Card
                  backgroundColor="#71CAFC"
                  value={data.totAssessment}
                  label="Total Employees"
                ></Card>
                <Card
                  backgroundColor="#9CDB9C"
                  value={data.empRegistered}
                  label="Registered Employees"
                ></Card>
                <Card
                  backgroundColor="#F6D0C1"
                  value={data.empFamilyRegistered}
                  label="Registered Family Members"
                ></Card>
              </div>

              <span
                style={{
                  color: "#000000",
                  fontFamily: "Inter",
                  fontStyle: "normal",
                  fontWeight: "700",
                  fontSize: "13px",
                  paddingLeft: "10px",
                  paddingTop: "5px",
                  margin: "10px 0px",
                  display: "block",
                }}
              >
                Health Assessments{" "}
              </span>

              <div className="row">
                <Card
                  backgroundColor="#9CDB9C"
                  value={data.last24Hours}
                  label="Last 24 Hours"
                ></Card>
                <Card
                  backgroundColor="#F8CD8D"
                  value={data.last48Hours}
                  label="Last 48 Hours"
                ></Card>
                <Card
                  backgroundColor="#FF97BB"
                  value={data.last7days}
                  label="Last 7 Days"
                ></Card>
              </div>

              <span
                style={{
                  color: "#000000",
                  fontFamily: "Inter",
                  fontStyle: "normal",
                  fontWeight: "700",
                  fontSize: "13px",
                  paddingLeft: "10px",
                  paddingTop: "5px",
                  margin: "10px 0px",
                  display: "block",
                }}
              >
                Employee Health Status{" "}
              </span>

              <ToggleSwitch 
              checked = {dataHealthStatus}
              onChange = {(checked) => setDataHealthStatus(checked) }
              />

              <div className="row">
                <Card
                  backgroundColor="#FF97BB"
                  value= {dataHealthStatus ? "Family" : data.hospitalized }
                  label="Hospitalized"
                ></Card>

                <Card
                  backgroundColor="#F8CD8D"
                  value= {dataHealthStatus ? "Family" : data.quarantined }
                  label="Home Care"
                ></Card>

                <Card
                  backgroundColor="#9CDB9C"
                  value= {dataHealthStatus ? "Family" : data.healthy }
                  label="Healthy"
                ></Card>
              </div>
            </div>
          </div>

          <div className="Dashboard-Without-Padding">
            <span
              style={{
                color: "#1A1C22",
                fontFamily: "Inter",
                fontStyle: "normal",
                fontWeight: "700",
                fontSize: "17px",
                paddingLeft: "16px",
                paddingTop: "5px",
                margin: "10px 0px",
                display: "block",
              }}
            >
              Health Alerts
            </span>

            <ToggleSwitch 
            checked = {dataHealthAlerts}
            onChange = {(checked) => setDataHealthAlerts(checked) }/>

            <div>
              <button
                style={{
                  width: "100%",
                  height: "6%",
                  padding: "15px",
                  marginBottom: "3px",
                  border: "0px",
                  backgroundColor: COLORS.WHITE,
                  fontSize: "15px",
                  lineHeight: "18px",
                  borderRadius: "5px",
                  fontWeight: "600",
                  textAlign: "left",
                  color: "#505A73",
                }}
                onClick={() => {
                  dispatch(SetHealthAlert("TEMPERATURE", "High Temperature"));
                  history.push("/dashboardHealth");
                }}
              >
                <div
                  style={{
                    display: "flex",
                    flexDirection: "row",
                    float: "right",
                  }}
                >
                  <span
                    style={{
                      color: "#505A73",
                      fontFamily: "Inter",
                      fontStyle: "normal",
                      fontWeight: "600px",
                      fontSize: "15px",
                      paddingRight: "10px",
                    }}
                  >
                    {dataHealthAlerts ? "Family" : data.highTemp }
                  </span>
                  <img
                    style={{
                      width: "16px",
                      height: "16px",
                      marginRight: "10px",
                      float: "right",
                    }}
                    src={IMAGES.Button_Arrow}
                  />
                </div>
                High Temperature
              </button>

              <button
                style={{
                  width: "100%",
                  height: "6%",
                  padding: "15px",
                  marginBottom: "3px",
                  border: "0px",
                  backgroundColor: COLORS.WHITE,
                  fontSize: "15px",
                  lineHeight: "18px",
                  borderRadius: "5px",
                  fontWeight: "600",
                  textAlign: "left",
                  color: "#505A73",
                }}
                onClick={() => {
                  dispatch(SetHealthAlert("SPO2", "Low Oxygen Level(Sp02)"));
                  history.push("/dashboardHealth");
                }}
              >
                <div
                  style={{
                    display: "flex",
                    flexDirection: "row",
                    float: "right",
                  }}
                >
                  <span
                    style={{
                      color: "#505A73",
                      fontFamily: "Inter",
                      fontStyle: "normal",
                      fontWeight: "600px",
                      fontSize: "15px",
                      paddingRight: "10px",
                    }}
                  >
                    {dataHealthAlerts ? "Family" : data.lowOxigen }
                  </span>
                  <img
                    style={{
                      width: "16px",
                      height: "16px",
                      marginRight: "10px",
                      float: "right",
                    }}
                    src={IMAGES.Button_Arrow}
                  />
                </div>
                Low Oxygen Level(Sp02)
              </button>

              <button
                style={{
                  width: "100%",
                  height: "6%",
                  padding: "15px",
                  marginBottom: "3px",
                  border: "0px",
                  backgroundColor: COLORS.WHITE,
                  fontSize: "15px",
                  lineHeight: "18px",
                  borderRadius: "5px",
                  fontWeight: "600",
                  textAlign: "left",
                  color: "#505A73",
                }}
                onClick={() => {
                  dispatch(
                    SetHealthAlert("FAMILY_INFECTED", "  Families is Affected")
                  );
                  history.push("/dashboardHealth");
                }}
              >
                <div
                  style={{
                    display: "flex",
                    flexDirection: "row",
                    float: "right",
                  }}
                >
                  <span
                    style={{
                      color: "#505A73",
                      fontFamily: "Inter",
                      fontStyle: "normal",
                      fontWeight: "600px",
                      fontSize: "15px",
                      paddingRight: "10px",
                    }}
                  >
                    {dataHealthAlerts ? "Family" : data.familyInfected }
                  </span>
                  <img
                    style={{
                      width: "16px",
                      height: "16px",
                      marginRight: "10px",
                      float: "right",
                    }}
                    src={IMAGES.Button_Arrow}
                  />
                </div>
                Family is Affected
              </button>

              <button
                style={{
                  width: "100%",
                  height: "6%",
                  padding: "15px",
                  marginBottom: "3px",
                  border: "0px",
                  backgroundColor: COLORS.WHITE,
                  fontSize: "15px",
                  lineHeight: "18px",
                  borderRadius: "5px",
                  fontWeight: "600",
                  textAlign: "left",
                  color: "#505A73",
                }}
                onClick={() => {
                  dispatch(
                    SetHealthAlert(
                      "REQ_HELP",
                      "Seeking Support from Altimetrik"
                    )
                  );
                  history.push("/dashboardHealth");
                }}
              >
                <div
                  style={{
                    display: "flex",
                    flexDirection: "row",
                    float: "right",
                  }}
                >
                  <span
                    style={{
                      color: "#505A73",
                      fontFamily: "Inter",
                      fontStyle: "normal",
                      fontWeight: "600px",
                      fontSize: "15px",
                      paddingRight: "10px",
                    }}
                  >
                    {dataHealthAlerts ? "Family" : data.seekingHelp }
                  </span>
                  <img
                    style={{
                      width: "16px",
                      height: "16px",
                      marginRight: "10px",
                      float: "right",
                    }}
                    src={IMAGES.Button_Arrow}
                  />
                </div>
                Seeking Support from Altimetrik
              </button>
            </div>

            <span
              style={{
                color: "#1A1C22",
                fontFamily: "Inter",
                fontStyle: "normal",
                fontWeight: "700",
                fontSize: "17px",
                paddingLeft: "10px",
                paddingTop: "5px",
                margin: "10px 0px",
                display: "block",
              }}
            >
              Report
            </span>

            <div style={{ backgroundColor: "white" }}  >
            <ToggleSwitch 
            checked = {dataHealthReport}
            onChange = {(checked) => setDataHealthReport(checked) }/>
            <div style={{paddingTop:"10px"}}>
            <TopNavBar
              item1="Hospitalized"
              item2="Home Care"
              item3="Healthy"
              SetItemValue={SetItemValue}
            ></TopNavBar>
            </div>
            </div>
            <Hospitalized
              value={val}
              index={0}
              data={dataHealthReport ? null : data.recenthospitalizedUpdates}
            ></Hospitalized>
            <HomeCare
              value={val}
              index={1}
              data={dataHealthReport ? null : data.recentQuarantinedUpdates}
            ></HomeCare>
            <Healthy
              value={val}
              index={2}
              data={dataHealthReport ? null : data.recentSafeUpdates}
            ></Healthy>

            <div style={{backgroundColor:"white"}} className="empty_btn">
              <a
                onClick={() => {
                  history.push("/dashboardOverview");
                }}
                href="#"
              >
                View All
              </a>
            </div>
          </div>
        </div>
      )}
      <Footer></Footer>
    </div>
  );
}

function Hospitalized(props) {
  const { data, value, index } = props;
  const history = useHistory();
  const dispatch = useDispatch();

  const OnClickProfile = (id) => {
    dispatch(SetDashboardUID(id));
    history.push("/dashboardUserProfile");
  };

  return (
    <div>
      {value === index && (
        <div>
          {data
            ? data.map((el, id) => (
                <HistoryCard
                  key={id}
                  status={el.Status}
                  userName={el.empName}
                  report={
                    "Last reported on " +                         moment(new Date(el.date)).format("DD/MM/YYYY HH:mm")

                  }
                  onPress={() => OnClickProfile(el.id)}
                ></HistoryCard>
              ))
            : "No data to show"}
        </div>
      )}
    </div>
  );
}

function HomeCare(props) {
  const { data, value, index } = props;
  const dispatch = useDispatch();
  const history = useHistory();

  const OnClickProfile = (id) => {
    dispatch(SetDashboardUID(id));
    history.push("/dashboardUserProfile");
  };

  return (
    <div>
      {value === index && (
        <div>
          {data
            ? data.map((el, id) => (
                <HistoryCard
                  onPress={() => OnClickProfile(el.id)}
                  key={id}
                  status={el.Status}
                  userName={el.empName}
                  report={
                    "Last reported on " +  moment(new Date(el.date)).format("DD/MM/YYYY HH:mm")
                  }
                ></HistoryCard>
              ))
            : "No data to show"}
        </div>
      )}
    </div>
  );
}

function Healthy(props) {
  const { data, value, index } = props;
  const history = useHistory();

  const dispatch = useDispatch();

  const OnClickProfile = (id) => {
    dispatch(SetDashboardUID(id));
    history.push("/dashboardUserProfile");
  };

  return (
    <div>
      {value === index && (
        <div>
          {data
            ? data.map((el, id) => (
                <HistoryCard
                  onPress={() => OnClickProfile(el.id)}
                  key={id}
                  status={el.Status}
                  userName={el.empName}
                  report={
                    "Last reported on " +                         moment(new Date(el.date)).format("DD/MM/YYYY HH:mm")

                  }
                ></HistoryCard>
              ))
            : "No data to show"}
        </div>
      )}
    </div>
  );
}
