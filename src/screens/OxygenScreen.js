import React, { useEffect, useRef, useState } from "react";
import "./css/OxygenScreen.css";
import { useHistory } from "react-router";
import { useSelector } from "react-redux";
import Breadcrumbs from "../components/Breadcrumbs";

function OxygenScreen() {
  const history = useHistory();
  const [isClickedWork, setIsClickedWork] = useState(false);
  const jwt = useSelector((state) => state.jwt);

  function OnClickBackBtn() {
    history.push("/home");
  }

  useEffect(() => {
    window.scrollTo(0, 0);

    if (jwt === "") {
      history.push("/");
    }
  }, []);

  return (
    <div className="page-container">
      <div className="content-wrap">
        <div style={{ height: "94%" }} className="App">
          <div
            style={{
              flexDirection: "row",
              display: "flex",
              paddingTop: "50px",
            }}
          >
            <img
              onClick={OnClickBackBtn}
              src="/images/Black Back Button.png"
              alt="back btn"
              style={{ width: "32px", height: "32px" }}
            ></img>
            <h3
              style={{
                color: "#1A1C22",
                fontFamily: "Inter",
                fontStyle: "normal",
                fontWeight: "600px",
                fontSize: "17px",
                paddingLeft: "10px",
                paddingTop: "5px",
                margin: "1px",
              }}
            >
              Back
            </h3>
          </div>
          <Breadcrumbs></Breadcrumbs>
          <div class="pagebg_img">
            <div
              className="bg_img_responsive"
              style={{ backgroundImage: "url(/images/oxygenbg.png)" }}
            >
              <div
                className="bg_inner_img"
                style={{ backgroundImage: "url(/images/oxygeninner.svg)" }}
              ></div>
            </div>
            <div class="top-left1">Information</div>
            <div class="top-left2">Oxygen Concentrators</div>
            <div class="top-left5">
              Now available in Bengaluru, Chennai, Hyderabad and Pune
            </div>
          </div>

          <div id="generalinsurance">
            <div className="quick_link">
              <div id="sidebar-left">
                <div>
                  <a className="quick_head sub_menu_head" href="#section1">
                    Overview
                  </a>
                </div>
                <div>
                  <a className="quick_head sub_menu_head" href="#section2">
                    When can one use them?
                  </a>
                </div>
                <div>
                  <a className="quick_head sub_menu_head" href="#section3">
                    How can I avail the service?
                  </a>
                </div>
                <div>
                  <a className="quick_head sub_menu_head" href="#section4">
                    Contact Details
                  </a>
                </div>
                <div>
                  <a className="quick_head sub_menu_head" href="#section5">
                    Frequently asked questions
                  </a>
                </div>
              </div>
            </div>

            <div
              style={{
                margin: "10px 0px",
                height: "0px",
                border: "0.5px solid rgba(0, 0, 0, 0.1)",
              }}
            ></div>

            <div id="section1">
              <div className="sub_head">Overview</div>
              <div className="txt_styel pad-tb10">
                Altimetrik are now equipped with Oxygen Concentrators at our
                base locations Bengaluru, Chennai, Hyderabad and Pune. Each of
                the locations are now equipped with three Oxygen Concentrators
                and will be available for the use of fellow Altimetrians and
                their dependent family. This is another key advancement in our
                journey to support each one of you during this pandemic
                situation.{" "}
              </div>
            </div>

            <div
              style={{
                margin: "10px 0px",
                height: "0px",
                border: "0.5px solid rgba(0, 0, 0, 0.1)",
              }}
            ></div>

            <div id="section2">
              <div className="sub_head">When can one use them?</div>
              <div className="txt_styel pad-tb10">
                Patients with moderate pneumonia induced by COVID-19 with oxygen
                saturation less than 94 can benefit from supplemental oxygen
                given through oxygen concentrator during pre and post
                hospitalization. In pre hospitalization cases these can to be
                used only till such time a hospital bed is identified and should
                not be used as an alternate to hospital admission. In post
                hospitalization cases this can be used based on the advice given
                by the doctor while discharging from hospital. Based on doctor’s
                recommendations oxygen concentrators can be used only in
                moderate cases of COVID-19 where the oxygen requirement is a
                maximum of 5/10 litres per minute. Using oxygen concentrator
                without medical guidance can be very harmful.{" "}
              </div>
            </div>

            <div
              style={{
                margin: "10px 0px",
                height: "0px",
                border: "0.5px solid rgba(0, 0, 0, 0.1)",
              }}
            ></div>

            <div id="section3">
              <div className="sub_head">How can I avail the service?</div>
              <div className="txt_styel pad-tb10">
                The use of oxygen concentrators should be first prescribed by
                certified doctors (soft copy to be shared as part of the
                request). You can avail this facility in Bangalore, Chennai,
                Pune and Hyderabad, and support will be provided within the city
                limits. You can reach out to anyone of the COVID support team,
                Account HRBP , Altimetrik Helpdesk (contact number shared below)
                to raise this request .Based on the availability , the
                concentrators will be sent on first come first serve basis.{" "}
              </div>
            </div>

            <div
              style={{
                margin: "10px 0px",
                height: "0px",
                border: "0.5px solid rgba(0, 0, 0, 0.1)",
              }}
            ></div>

            <div id="section4">
              <div className="sub_head">Contact Details</div>
              <div className="pry_contact">
                <table className="styled-table">
                  <thead>
                    <tr>
                      <th style={{ width: "50%" }}>Location</th>
                      <th>SPOC</th>
                    </tr>
                  </thead>
                  <tbody>
                    <tr>
                      <td>Bangalore</td>
                      <td>
                        <div className="user_Name">Joyson Pinto </div>
                        <div className="user_cnt">+91 9845809892</div>
                        <div className="user_Name">Santosh Mervyn </div>
                        <div className="user_cnt">+91 9900256978</div>
                      </td>
                    </tr>
                    <tr>
                      <td>Chennai</td>
                      <td>
                        <div className="user_Name">Prabu Sanjeevi </div>
                        <div className="user_cnt">+91 9003279877</div>
                        <div className="user_Name">Suresh Jayaraj </div>
                        <div className="user_cnt">+91 9840913481</div>
                      </td>
                    </tr>
                    <tr>
                      <td>Hyderabad</td>
                      <td>
                        <div className="user_Name">Balaji Venkataswamy </div>
                        <div className="user_cnt">+91 9003159118</div>
                        <div className="user_Name">Bakhtiar Khan </div>
                        <div className="user_cnt">+91 9384840592</div>
                      </td>
                    </tr>
                    <tr>
                      <td>Pune</td>
                      <td>
                        <div className="user_Name">Sudhir Satpute </div>
                        <div className="user_cnt">+91 7875890890</div>
                        <div className="user_Name">Rajendra </div>
                        <div className="user_cnt">+91 9763170359</div>
                      </td>
                    </tr>
                  </tbody>
                </table>
              </div>
            </div>

            <div
              style={{
                margin: "10px 0px",
                height: "0px",
                border: "0.5px solid rgba(0, 0, 0, 0.1)",
              }}
            ></div>

            <div id="section5">
              <div className="sub_head pad-b5">Frequently Asked Questions</div>
              <div>
                <div className="policy_head">
                  {" "}
                  Do I need to get a prescription from Doctor to raise the
                  request for O2 concentrators?
                </div>
                <div className="policy_cnt2 pad-b5">
                  <div className="txt_styel">
                    Yes doctor prescription for usage of Oxygen concentrator is
                    must as this should be used only under medical supervision
                  </div>
                </div>

                <div className="policy_head">
                  {" "}
                  Who is eligible for getting Oxygen concentrator?
                </div>
                <div className="policy_cnt2 pad-b5">
                  <div className="txt_styel">
                    All employees of Altimetrik/ Davinta/ Calibo can use this
                    for self and his/her dependents, spouse & kids
                  </div>
                </div>

                <div className="policy_head">
                  Which are the locations I can avail this facility?
                </div>
                <div className="policy_cnt2 pad-b5">
                  <div className="txt_styel">
                    You can avail this facility within the city limits of
                    Bangalore, Chennai, Pune and Hyderabad
                  </div>
                </div>

                <div className="policy_head">
                  {" "}
                  Will I get this to my door step?
                </div>
                <div className="policy_cnt2 pad-b5">
                  <div className="txt_styel">
                    Yes. We will take care of the delivery and pick-up of oxygen
                    concentrator through our approved vendor
                  </div>
                </div>

                <div className="policy_head">
                  {" "}
                  How long I can keep this with me?
                </div>
                <div className="policy_cnt2 pad-b5">
                  <div className="txt_styel">
                    It can be used by the patient/requester for the interim
                    period and immediate medical attention should be sought for
                    hospitalization.
                  </div>
                </div>

                <div className="policy_head">
                  Do I have to pay for the same?
                </div>
                <div className="policy_cnt2 pad-b5">
                  <div className="txt_styel">
                    No, it’s free for Altimetrik/Davinta/Calibo employees and
                    their dependents
                  </div>
                </div>

                <div className="policy_head">
                  How many units of oxygen concentrators do we have?
                </div>
                <div className="policy_cnt2 pad-b5">
                  <div className="txt_styel">
                    We have 3 each oxygen concentrators in all 4 locations
                  </div>
                </div>

                <div className="policy_head">
                  Will I get it immediately after raising the request for oxygen
                  concentrator?
                </div>
                <div className="policy_cnt2 pad-b5">
                  <div className="txt_styel">
                    Based on the availability it will be given on first come
                    first serve basis
                  </div>
                </div>

                <div className="policy_head">
                  What is the turnaround time for getting the same once the
                  request is raised?
                </div>
                <div className="policy_cnt2 pad-b5">
                  <div className="txt_styel">
                    F&A team will ship the same ASAP & the request will be
                    closed within 24 hours from the time request is received.{" "}
                  </div>
                </div>

                <div className="policy_head">
                  {" "}
                  Would someone be guiding me on the how to operate the O2
                  concentrator?
                </div>
                <div className="policy_cnt2 pad-b5">
                  <div className="txt_styel">
                    Yes we will have the expert delivering this to your
                    residence and they will be setting-up this for you and
                    showing you the operating procedures
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}

export default OxygenScreen;
