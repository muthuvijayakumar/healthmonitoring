import React, { useEffect, useRef } from "react";
import "./css/protocols.css";
import { useHistory } from "react-router";
import { useSelector } from "react-redux";
import Breadcrumbs from "../components/Breadcrumbs";

export default function ProtocolScreen() {
  const history = useHistory();
  const jwt = useSelector((state) => state.jwt);

  function OnClickBackBtn() {
    history.push("/home");
  }
  useEffect(() => {
    window.scrollTo(0, 0);

    if (jwt === "") {
      history.push("/");
    }
  }, []);

  return (
    <div className="page-container">
      <div className="content-wrap">
        <div style={{ height: "94%" }} className="App">
          <div
            style={{
              flexDirection: "row",
              display: "flex",
              paddingTop: "50px",
            }}
          >
            <img
              onClick={OnClickBackBtn}
              src="/images/Black Back Button.png"
              alt="back btn"
              style={{ width: "32px", height: "32px" }}
            ></img>
            <h3
              style={{
                color: "#1A1C22",
                fontFamily: "Inter",
                fontStyle: "normal",
                fontWeight: "600px",
                fontSize: "17px",
                paddingLeft: "10px",
                paddingTop: "5px",
                margin: "1px",
              }}
            >
              Back
            </h3>
          </div>

          <Breadcrumbs></Breadcrumbs>

          <div className="pagebg_img">
            <div className="bg_img_responsive" style={{backgroundImage: 'url(/images/protocolbg.png)'}}>
              <div className="bg_inner_img" style={{backgroundImage: 'url(/images/protocolinner.svg)'}}></div>
            </div>
            <div className="top-left1">Information</div>
            <div className="top-left2">Protocols</div>
            <div className="top-left3">Recommended from doctors</div>
            <div className="top-left4">
              Stages,Symptoms and Preventive Protocols
            </div>
          </div>

          <div id="Protocols">
            <div id="section1">
              <div className="policy_head_grn">Stage 0</div>

              <div className="sub_head">Situation</div>
              <div className="txt_styel pad-tb10">
                When no one in your household has COVID-19 infection or symptoms
                indicative of COVID-19 infection{" "}
              </div>

              <div className="sub_head">Protocols</div>
              <div className="head_content">
                <ul>
                  <li>
                    Update your health status regularly through Altimetrik COVID
                    Care app
                  </li>
                  <li>Strict confinement at home </li>
                  <li>
                    Double masking is highly recommended as the infection is
                    airborne and the mutant strains are highly infectious{" "}
                  </li>
                  <li>Stay calm </li>
                  <li>Practice meditation </li>
                  <li>
                    Physical exercise including breathing exercises -twice a day{" "}
                  </li>
                  <li>
                    More intake of plant products, juices, fruits and salads,
                    and green leafy vegetables{" "}
                  </li>
                  <li>Use warm water with daily intake of 1.5 to 2 litters </li>
                  <li>Keep social distancing in the family </li>
                  <li>
                    Nasal rinsing and gargles with lukewarm water (twice a day){" "}
                  </li>
                  <li>Frequent handwashing for 20 sec every time </li>
                </ul>
              </div>
            </div>

            <div id="section2">
              <div className="policy_head_org">Stage 1A </div>

              <div className="sub_head">Situation</div>
              <div className="txt_styel pad-tb10">
                When you are healthy but one or more members of your family has
                got confirmed COVID-19 infection or the symptoms suggestive of
                COVID-19{" "}
              </div>

              <div className="sub_head">Protocols</div>
              <div className="head_content">
                <ul>
                  <li>
                    Update your health status regularly through Altimetrik COVID
                    Care app{" "}
                  </li>
                  <li>Strictly isolate the person with symptoms </li>
                  <li>Use mask except for while eating and sleeping </li>
                  <li>Get Covid-19 RTPCR to confirm the diagnosis </li>
                  <li>
                    Measure body temperature twice a day and as when feel
                    feverish{" "}
                  </li>
                  <li>Monitor Pulse and Oxygen level 2-3 times a day </li>
                </ul>
              </div>
            </div>

            <div id="section3">
              <div className="policy_head_org">Stage 1B </div>

              <div className="sub_head">Situation</div>
              <div className="txt_styel pad-tb10">
                When you have one or more of the following{" "}
              </div>
              <div>Symptoms:</div>
              <div className="head_content">
                <ul>
                  <li>Sore throat </li>
                  <li>Fever or chills</li>
                  <li>Dry Cough </li>
                  <li>Cough with sputum </li>
                  <li>Loss of taste or smell</li>
                  <li>Cold/ runny nose</li>
                  <li>Diarrhoea </li>
                  <li>Nausea/ vomiting </li>
                  <li>Abdominal pain</li>
                  <li>Headache </li>
                  <li>Body ache </li>
                  <li>Fatigue/ lethargy </li>
                  <li>Watery eyes/ conjunctivitis </li>
                  <li>Skin rashes </li>
                  <li>Dry mouth </li>
                  <li>Burning palms </li>
                </ul>
              </div>
              <div className="sub_head">Protocols</div>
              <div className="head_content">
                <ul>
                  <li>
                    Update your health status regularly through Altimetrik COVID
                    Care app{" "}
                  </li>
                  <li>Inform your HRBP or People Leader </li>
                  <li>Strictly isolate the person with symptoms </li>
                  <li>Use mask except for while eating and sleeping </li>
                  <li>Get Covid-19 RTPCR to confirm the diagnosis </li>
                  <li>
                    Measure body temperature twice a day and as when feel
                    feverish{" "}
                  </li>
                  <li>Monitor Pulse and Oxygen level 2-3 times a day </li>
                  <li>Seek medical consultation </li>
                </ul>
              </div>
            </div>

            <div id="section4">
              <div className="policy_head_org">Stage 2 </div>

              <div className="sub_head">Situation</div>
              <div className="txt_styel pad-tb10">
                When your symptoms continue for more than 5 days and you have
                the following additional symptoms:{" "}
              </div>

              <div className="head_content">
                <ul>
                  <li>Gradually increasing weakness </li>
                  <li>
                    High grade fever not responding to paracetamol (Medicine
                    given for controlling fever{" "}
                  </li>
                  <li>Shortness of breath/ Difficulty breathing </li>
                  <li>Persistent pain/ pressure in the chest </li>
                  <li>Dizziness/ mental confusion </li>
                  <li>Mental confusion </li>
                  <li>Inability to wake up or stay awake </li>
                  <li>
                    Pale, gray, or bluish-colored skin, lips, or nail beds{" "}
                  </li>
                  <li>
                    Has your oxygen saturation has gone below 90 in last 24 hrs{" "}
                  </li>
                </ul>
              </div>
              <div className="sub_head">Protocols</div>
              <div className="head_content">
                <ul>
                  <li>
                    Update your health status regularly through Altimetrik COVID
                    Care app{" "}
                  </li>
                  <li>Inform your HRBP or People Leader </li>
                  <li>Remain under medical supervision </li>
                  <li>Prepare for access to the hospital </li>
                </ul>
              </div>
            </div>

            <div id="section5">
              <div className="policy_head_org">Stage 3 </div>

              <div className="sub_head pad-t10">Situation</div>
              <div className="txt_styel pad-tb10">
                When you feel overall deterioration of your health after 3-4
                days of illness and or confirmed covid diagnosis{" "}
              </div>

              <div className="head_content">
                <ul>
                  <li>
                    High fever persistent more than 100 degree F for more than 3
                    days{" "}
                  </li>
                  <li>
                    Increasing breathing difficulty and shortness of breath{" "}
                  </li>
                  <li>
                    If you have harboured respiration and your respiratory rate
                    is more than 24/min{" "}
                  </li>
                  <li>If your oxygen saturation falls below 90% </li>
                </ul>
              </div>
              <div className="sub_head">Protocols</div>
              <div className="head_content">
                <ul>
                  <li>
                    Update your health status regularly through Altimetrik COVID
                    Care app{" "}
                  </li>
                  <li>Inform your HRBP or People Leader </li>
                  <li>Be under medical supervision </li>
                  <li>Arrange access to oxygen </li>
                  <li>Locate a critical care bed in a hospital </li>
                </ul>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}