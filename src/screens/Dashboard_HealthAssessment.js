import React, { useState, useEffect } from "react";
import { STRING, IMAGES, COLORS } from "../utils/constants";
import "./css/Dashboard_HealthAssessment.css";
import Footer from "../components/Footer";

import { useHistory } from "react-router";
import { TopNavBar } from "../components/TopNavBar";
import { HistoryCard } from "../components/HistoryCard";
import API from "../utils/api/API";
import { useDispatch, useSelector } from "react-redux";
import {
  SetDashboardUID,
  SetOtherEmpDetail,
  SetAssessmentType,
} from "../redux/action";
import moment from "moment";
import ToggleSwitch from "../components/ToggleSwitch";

export default function Dashboard_HealthAssessment() {
  const history = useHistory();
  const [data, setData] = useState();
  const jwt = useSelector((state) => state.jwt);

  const [searchTerm, setsearchTerm] = useState();
  const [searchResult, setsearchResult] = useState();
  const [loading, setLoading] = useState(true);
  const [dataHealthReport, setDataHealthReport] = useState(false);
  const [category, setCategory] = useState("HOSPITALIZED");
  const roles = useSelector((state) => state.roles);
  const empID = useSelector((state) => state.empID);

  const [val, setVal] = useState(0);

  function SetItemValue(value) {
    if (value === 0) {
      DataFetch("HOSPITALIZED");

      setCategory("HOSPITALIZED");
    } else if (value === 1) {
      DataFetch("QUARANTINED");

      setCategory("QUARANTINED");
    } else if (value === 2) {
      DataFetch("SAFE");
      setCategory("SAFE");
    }
    setVal(value);
    console.log(value);
  }

  function OnClickBackBtn() {
    if (roles.includes("DASHBOARD")) {
      history.push("/dashboard");
    } else {
      history.push("/home");
    }
  }

  const searchHandler = (val) => {
    setsearchTerm(val.target.value);
    if (val.target.value !== "") {
      const newList = data.filter((userData) => {
        console.log(Object.values(userData)[2]);

        return Object.values(userData)[2]
          .toLowerCase()
          .includes(val.target.value.toLowerCase());
      });

      console.log(newList);

      setsearchResult(newList);

      console.log(searchResult);
    } else {
      setsearchResult(data);
    }
  };

  function DataFetch(tempCat) {
    setLoading(true);

    if (roles.includes("DASHBOARD")) {
      API.get(process.env.REACT_APP_EMPLOYEE_DASHBOARD_STATUS + tempCat, {
        headers: {
          Authorization: `Bearer ${jwt}`,
        },
      })
        .then((res) => {
          console.log(res);
          setData(res.data);
          console.log(data);
          setsearchResult(res.data);
          setLoading(false);
        })
        .catch((err) => {
          switch (err.response.status) {
            case 500:
              break;
            case 400:
              break;

            case 401:
              history.push("/");

              break;
            case 403:
              history.push("/");
              break;

            default:
              break;
          }
          console.log(err);

          throw err;
        });
    } else if (roles.includes("HRBP")) {
      API.get(
        process.env.REACT_APP_EMPLOYEE_HRBP_DASHBOARD_STATUS +
          tempCat +
          "&empId=" +
          empID,
        {
          headers: {
            Authorization: `Bearer ${jwt}`,
          },
        }
      )
        .then((res) => {
          console.log(res);
          setData(res.data);
          console.log(data);
          setsearchResult(res.data);
          setLoading(false);
        })
        .catch((err) => {
          switch (err.response.status) {
            case 500:
              break;
            case 400:
              break;

            case 401:
              history.push("/");

              break;
            case 403:
              history.push("/");
              break;

            default:
              break;
          }
          console.log(err);

          throw err;
        });
    }
  }

  useEffect(() => {
    setLoading(true);

    if (roles.includes("DASHBOARD")) {
      API.get(process.env.REACT_APP_EMPLOYEE_DASHBOARD_STATUS + category, {
        headers: {
          Authorization: `Bearer ${jwt}`,
        },
      })
        .then((res) => {
          console.log(res);
          setData(res.data);
          console.log(data);
          setsearchResult(res.data);
          setLoading(false);
        })
        .catch((err) => {
          switch (err.response.status) {
            case 500:
              break;
            case 400:
              break;

            case 401:
              history.push("/");

              break;
            case 403:
              history.push("/");
              break;

            default:
              break;
          }
          console.log(err);

          throw err;
        });
    } else if (roles.includes("HRBP")) {
      API.get(
        process.env.REACT_APP_EMPLOYEE_HRBP_DASHBOARD_STATUS +
          category +
          "&empId=" +
          empID,
        {
          headers: {
            Authorization: `Bearer ${jwt}`,
          },
        }
      )
        .then((res) => {
          console.log(res);
          setData(res.data);
          console.log(data);
          setsearchResult(res.data);
          setLoading(false);
        })
        .catch((err) => {
          switch (err.response.status) {
            case 500:
              break;
            case 400:
              break;

            case 401:
              history.push("/");

              break;
            case 403:
              history.push("/");
              break;

            default:
              break;
          }
          console.log(err);

          throw err;
        });
    }

    return () => {};
  }, []);

  return (
    <div className="page-container">
      <div className="content-wrap">
        <div style={{ height: "94%" }} className="Dashboard_H">
          <div style={{ justifyContent: "center", alignItems: "center" }}>
            <div
              style={{
                flexDirection: "row",
                display: "flex",
                paddingTop: "50px",
              }}
            >
              <img
                src={IMAGES.BackBtn}
                alt="back btn"
                onClick={OnClickBackBtn}
                style={{ width: "32px", height: "32px" }}
              ></img>
              <h3
                style={{
                  color: "#1A1C22",
                  fontFamily: "Inter",
                  fontStyle: "normal",
                  fontWeight: "600px",
                  fontSize: "17px",
                  paddingLeft: "10px",
                  paddingTop: "5px",
                }}
              >
                Employee Health Status
              </h3>
            </div>

            <div style={{paddingTop:"10px"}}>
            <ToggleSwitch 
              checked = {dataHealthReport}
              onChange = {(checked) => setDataHealthReport(checked) }/>
            </div>

            <div style={{ width: "98%", height: "50%", paddingTop: "15px" }}>
              <input
                style={{
                  borderWidth: 1,
                  borderRadius: 5,
                  fontSize: "15px",
                  backgroundColor: "rgba(36, 36, 83, 0.05)",
                  width: "100%",
                  height: "6%",
                  padding: "10px 0px 10px 2%",
                }}
                placeholder="Search"
                onChange={searchHandler}
                type="text"
              ></input>
            </div>
          </div>
        </div>

        <div className="Dashboard_H_WithoutPadding">
          <TopNavBar
            item1="Hospitalized"
            item2="Home Care"
            item3="Healthy"
            SetItemValue={SetItemValue}
          ></TopNavBar>

          {loading ? (
            "Loading"
          ) : (
            <Hospitalized
              value={val}
              index={0}
              data={dataHealthReport ? null : searchResult}
            ></Hospitalized>
          )}
          {loading ? (
            "."
          ) : (
            <HomeCare value={val} index={1} data={dataHealthReport ? null : searchResult}></HomeCare>
          )}
          {loading ? (
            "."
          ) : (
            <Healthy value={val} index={2} data={dataHealthReport ? null : searchResult}></Healthy>
          )}
        </div>
      </div>
      <Footer></Footer>
    </div>
  );
}

function Hospitalized(props) {
  const { data, value, index } = props;
  const history = useHistory();

  const dispatch = useDispatch();

  const OnClickProfile = (id) => {
    dispatch(SetDashboardUID(id));
    history.push("/dashboardUserProfile");
  };

  function OnClickCheckIn(data) {
    dispatch(SetAssessmentType("OTHERS"));
    dispatch(SetOtherEmpDetail(data));
    console.log(data);
    history.push("/home/healthStatus");
  }

  return (
    <div>
      {value === index && (
        <div>
          {data
            ? data.map((el, id) => (
                <HistoryCard
                  onPress={() => OnClickProfile(el.id)}
                  key={id}
                  status={el.Status}
                  userName={el.empName}
                  CheckInShow={true}
                  report={
                    el.type === "SELF" || el.type === null
                      ? "Last Reported on " +moment( new Date(el.date)).format('DD/MM/YYYY HH:mm')
                      : "Last Checked on " + moment( new Date(el.date)).format('DD/MM/YYYY HH:mm')
                  }
                  CheckIn={() => OnClickCheckIn(el)}
                ></HistoryCard>
              ))
            : "No data to show"}
        </div>
      )}
    </div>
  );
}

function HomeCare(props) {
  const { data, value, index } = props;
  const history = useHistory();

  const dispatch = useDispatch();

  const OnClickProfile = (id) => {
    dispatch(SetDashboardUID(id));
    history.push("/dashboardUserProfile");
  };

  function OnClickCheckIn(data) {
    dispatch(SetAssessmentType("OTHERS"));
    dispatch(SetOtherEmpDetail(data));
    history.push("/about/healthStatus");
  }

  return (
    <div>
      {value === index && (
        <div>
          {data
            ? data.map((el, id) => (
                <HistoryCard
                  onPress={() => OnClickProfile(el.id)}
                  key={id}
                  status={el.Status}
                  userName={el.empName}
                  CheckInShow={true}
                  report={
                    el.type === "SELF" || el.type === null
                      ? "Last Reported on " + moment( new Date(el.date)).format('DD/MM/YYYY HH:mm')
                      : "Last Checked on " + moment( new Date(el.date)).format('DD/MM/YYYY HH:mm')
                  }
                  CheckIn={() => OnClickCheckIn(el)}
                ></HistoryCard>
              ))
            : "No data to show"}
        </div>
      )}
    </div>
  );
}

function Healthy(props) {
  const { data, value, index } = props;
  const history = useHistory();

  const dispatch = useDispatch();

  const OnClickProfile = (id) => {
    dispatch(SetDashboardUID(id));
    history.push("/dashboardUserProfile");
  };

  function OnClickCheckIn(data) {
    dispatch(SetAssessmentType("OTHERS"));
    dispatch(SetOtherEmpDetail(data));
    console.log(data);
    history.push("/home/healthStatus");
  }

  return (
    <div>
      {value === index && (
        <div>
          {data
            ? data.map((el, id) => (
                <HistoryCard
                  onPress={() => OnClickProfile(el.id)}
                  key={id}
                  status={el.Status}
                  userName={el.empName}
                  CheckInShow={true}
                  report={
                    el.type === "SELF" || el.type === null
                      ? "Last Reported on " +moment( new Date(el.date)).format('DD/MM/YYYY HH:mm')
                      : "Last Checked on " + moment( new Date(el.date)).format('DD/MM/YYYY HH:mm')
                  }
                  CheckIn={() => OnClickCheckIn(el)}
                ></HistoryCard>
              ))
            : "No data to show"}
        </div>
      )}
    </div>
  );
}
