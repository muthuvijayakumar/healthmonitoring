import React, { useEffect, useState } from "react";
import "./css/DashboardSumary.css";
import { useDispatch, useSelector } from "react-redux";
import { useHistory } from "react-router";

function DashboardSumary(props) {
  const data = useSelector((state) => state.dashboardSum);
  const history = useHistory();
  const jwt = useSelector((state) => state.jwt);
  const [loading, setLoading] = useState(true);

  function OnClickBackBtn() {
    history.push("/dashboardUserProfile");
  }

  useEffect(() => {
    setLoading(false);
    console.log(jwt);
    if (jwt === "") {
      history.push("/");
    }
  }, []);

  return (
    <div className="page-container">
      <div className="content-wrap">
        {loading ? (
          "Loading..."
        ) : (
          <div style={{ height: "94%" }} className="summaryContainer">
            <div
              style={{
                flexDirection: "row",
                display: "flex",
                paddingTop: "50px",
              }}
            >
              <img
                onClick={OnClickBackBtn}
                src="/images/Black Back Button.png"
                alt="back btn"
                style={{ width: "32px", height: "32px" }}
              ></img>
              <h3
                style={{
                  color: "#1A1C22",
                  fontFamily: "Inter",
                  fontStyle: "normal",
                  fontWeight: "600px",
                  fontSize: "17px",
                  paddingLeft: "10px",
                  paddingTop: "5px",
                  margin: "1px",
                }}
              >
                {new Date(data.date).toLocaleDateString()}
              </h3>
            </div>

            <div id="summary">
              {data.surveyresponse.map((el, id) => (
                <div key={id}>
                  <div className="label_head pad-t10">{el.quesDesc}</div>
                  <div className="head_content"> {el.answer}</div>
                  <div
                    style={{ height: "0px", border: "0.5px solid #E8EDF1" }}
                  ></div>
                </div>
              ))}
            </div>
          </div>
        )}
      </div>
    </div>
  );
}

export default DashboardSumary;
