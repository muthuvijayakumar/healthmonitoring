import React, { useState } from "react";

import { AuthenticatedTemplate, UnauthenticatedTemplate, useMsal } from "@azure/msal-react";
import { loginRequest } from "../sso_components/authConfig";
import { callMsGraph } from "../sso_components/graph";

/**
 * Renders information about the signed-in user or a button to retrieve data about the user
 */
const ProfileContent = () => {
    const { instance, accounts } = useMsal();
    const [graphData, setGraphData] = useState(null);

    function RequestProfileData() {
        // Silently acquires an access token which is then attached to a request for MS Graph data
        instance.acquireTokenSilent({
            ...loginRequest,
            account: accounts[0]
        }).then((response) => {
            console.log(response.accessToken);
            callMsGraph(response.accessToken).then(response => setGraphData(response));
        });
    }

    console.log(accounts);

    return (
        <>
            {console.log(accounts)}
            <h5 className="card-title">Welcome {accounts[0].name}</h5>
            {graphData ? 
                <div>{null}</div>
                :
                <button variant="secondary" onClick={RequestProfileData}>Request Profile Information</button>
            }
        </>
    );
};




export default function App() {

    const { instance } = useMsal();  

    const handleLogin = (loginType) => {
 
         if (loginType === "popup") {
             instance.loginPopup(loginRequest).catch(e => {
                 console.log(e);
             });
         } else if (loginType === "redirect") {
             instance.loginRedirect(loginRequest).catch(e => {
                 console.log(e);
             });
         }
     }

    return (
        <div>
            <UnauthenticatedTemplate>
                    <h5 className="card-title">Please sign-in to see your profile information.</h5>
                    <button onClick={() =>handleLogin("redirect")}>Click To sign in</button>
            </UnauthenticatedTemplate>

           <AuthenticatedTemplate>
                <ProfileContent />
            </AuthenticatedTemplate>
        </div>
       
    );
}
