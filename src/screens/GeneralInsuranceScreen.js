import React, { useEffect, useRef, useState } from "react";
import "./css/GeneralInsuranceScreen.css";
import { useHistory } from "react-router";
import { useSelector } from "react-redux";
import Breadcrumbs from "../components/Breadcrumbs";

function GeneralInsuranceScreen() {
  const history = useHistory();
  const [isClickedWork, setIsClickedWork] = useState(false);
  const jwt = useSelector((state) => state.jwt);

  function OnClickBackBtn() {
    history.push("/home");
  }

  useEffect(() => {
    window.scrollTo(0, 0);

    if (jwt === "") {
      history.push("/");
    }
  }, []);

  return (
    <div className="page-container">
      <div className="content-wrap">
        <div style={{ height: "94%" }} className="App">
          <div
            style={{
              flexDirection: "row",
              display: "flex",
              paddingTop: "50px",
            }}
          >
            <img
              onClick={OnClickBackBtn}
              src="/images/Black Back Button.png"
              alt="back btn"
              style={{ width: "32px", height: "32px" }}
            ></img>
            <h3
              style={{
                color: "#1A1C22",
                fontFamily: "Inter",
                fontStyle: "normal",
                fontWeight: "600px",
                fontSize: "17px",
                paddingLeft: "10px",
                paddingTop: "5px",
                margin: "1px",
              }}
            >
              Back
            </h3>
          </div>
          <Breadcrumbs></Breadcrumbs>
          <div class="pagebg_img">
            <div className="bg_img_responsive" style={{backgroundImage: 'url(/images/generalinsubg.png)'}}>
              <div className="bg_inner_img" style={{backgroundImage: 'url(/images/generalinsuinner.svg)'}}></div>
            </div>
            <div class="top-left1">Group Medical Insurance</div>
            <div class="top-left2">Health Insurance</div>
            <div class="top-left3">Insured with New India Assurance</div>
            <div class="top-left4">
              Covers hospitalization expenses due to sickness or accident
            </div>
          </div>

          <div id="generalinsurance">
            <div className="quick_link">
              <div id="sidebar-left">
                <div>
                  <a className="quick_head sub_menu_head" href="#section1">
                    Overview
                  </a>
                </div>
                <div>
                  <a className="quick_head sub_menu_head" href="#section2">
                    How Insurance Policy works?
                  </a>
                </div>
                <div>
                  <a className="quick_head sub_menu_head" href="#section3">
                    Basic Policy Coverages
                  </a>
                </div>
                <div>
                  <a className="quick_head sub_menu_head" href="#section4">
                    Frequently Asked Questions
                  </a>
                </div>
                <div>
                  <a className="quick_head sub_menu_head" href="#section5">
                    Contact Details
                  </a>
                </div>
              </div>
            </div>

            <div
              style={{
                margin: "10px 0px",
                height: "0px",
                border: "0.5px solid rgba(0, 0, 0, 0.1)",
              }}
            ></div>

            <div id="section1">
              <div className="sub_head">Overview</div>
              <div className="txt_styel pad-tb10">
                The Group Medical policy covers expenses by the insured persons
                & family members covered on account of hospitalization due to
                sickness or accident.{" "}
              </div>
              <div className="txt_styel pad-tb10">
                The policy covers expenses incurred on{" "}
              </div>
              <div className="content_list">
                <ul>
                  <li>
                    Room rent, medicines, surgery etc. Expenses for
                    hospitalization
                  </li>
                  <li>
                    Payable only if a 24 hour hospitalization has been taken
                    [*Except for named day care procedures, which do not require
                    a 24 hour hospitalization.
                  </li>
                  <li>
                    Any hospitalization for 24 hours or more for mainly
                    investigations without any active line of treatment won’t be
                    covered.]
                  </li>
                  <li>
                    Typical expense heads covered are the following:
                    room/boarding expenses as provided by the hospital or
                    nursing home ; nursing expenses ; surgeon, anesthetist ,
                    medical practitioner, consultant , specialist fees;
                    anesthesia, blood, oxygen, operation theater charges,
                    surgical appliance, medicines and drugs. diagnostic material
                    and X-Ray; dialysis, chemotherapy, radiotherapy, cost of
                    pace maker, artificial limbs and cost of organs and similar
                    expenses.
                  </li>
                </ul>
              </div>

              <div className="label_head pad-t10">Policy Holder</div>
              <div className="head_content">Altimetrik India Pvt Ltd</div>

              <div className="label_head pad-t10">Policy Start & End Date</div>
              <div className="head_content">13th Oct 2020 to 12th Oct 2021</div>

              <div className="label_head pad-t10">Insurer</div>
              <div className="head_content">New India Assurance Co. Ltd</div>

              <div className="label_head pad-t10">Insurance Consultant</div>
              <div className="head_content">
                Futurisk Insurance Broking Co Pvt Ltd
              </div>

              <div className="label_head pad-t10">Sum Insured</div>
              <div className="head_content">INR 3,00,000</div>

              <div className="label_head pad-t10">
                Third Party Administrator
              </div>
              <div className="head_content">Mediassist India TPA Pvt Ltd</div>

              <div className="label_head pad-t10">Who can avail this ?</div>
              <div className="head_content">
                (1+5) Self + Spouse + 2 Kids + 2 Parents/In laws
              </div>
            </div>

            <div
              style={{
                margin: "10px 0px",
                height: "0px",
                border: "0.5px solid rgba(0, 0, 0, 0.1)",
              }}
            ></div>

            <div id="section2">
              <div className="sub_head pad-b5">How Insurance Policy works?</div>
              <div>
                <div
                  for="policy1"
                  className="policy_head_grn"
                  onClick={() => setIsClickedWork(!isClickedWork)}
                >
                  Cashless / Planned/Emergency Hospitalization
                </div>

                {isClickedWork ? (
                  <div className="policy_cnt1">
                    <p>Employee Approaches Network Hospital with E-card </p>
                    <p>
                      Hospital intimates TPA & sends Pre-Authorization request
                      with approximate cost of the treatment for approval
                    </p>
                    <p>
                      TPA issues letter of credit (for cashless) with approval
                      for partial / full amount as per eligibility and coverage
                      to the hospital
                    </p>
                    <p>
                      At the time of Discharge hospital sends the final bill and
                      the discharge summary to TPA for the final approval
                    </p>
                    <p>
                      TPA sends the final approval to hospital which allows the
                      employee to get discharged by paying all non-medical /
                      non-payable expenses
                    </p>
                    <p>
                      Hospital sends complete set of claims documents for
                      processing to TPA
                    </p>
                    <p style={{ border: "0px" }}>
                      Claims Processing by TPA & Insurer, release of payments to
                      the hospital
                    </p>
                  </div>
                ) : (
                  <div>{null}</div>
                )}
              </div>
            </div>

            <div
              style={{
                margin: "10px 0px",
                height: "0px",
                border: "0.5px solid rgba(0, 0, 0, 0.1)",
              }}
            ></div>

            <div id="section3">
              <div className="sub_head pad-tb5">Basic Policy Coverages</div>

              <div className="label_head pad-t10">THIRD CHILD COVER</div>
              <div className="head_content">
                Covered in case of twins during second delivery. Also as an
                exception 3rd child will be covered for max. 5 cases in third
                delivery
              </div>

              <div className="label_head pad-t10">AGE LIMIT</div>
              <div className="head_content">0-90 years</div>

              <div className="label_head pad-t10">SUM INSURED</div>
              <div className="head_content">
                <ul>
                  <li>
                    Base Sum Insured of INR 3 Lakhs per family, with the option
                    to Top Up in multiples of 1 Lakh upto 7 Lakhs Room rent for
                    Base + Enhanced sum insured (Up to 6 lakhs) : 1% for Normal
                    & 2% for ICU
                  </li>
                  <li>
                    Room rent for Base + Enhanced sum insured (7 Lakhs - 10
                    lakhs) : INR 6000 for Normal and INR 12000 for ICU
                  </li>
                </ul>
              </div>

              <div className="label_head pad-t10">
                PRE AND POST HOSPITALIZATION COVERAGE
              </div>
              <div className="head_content">
                30 days pre-hospitalization and 60 days post-hospitalization
                respectively{" "}
              </div>

              <div className="label_head pad-t10">
                MATERNITY BENEFITS - LIMITS AND COVERAGES
              </div>
              <div className="head_content">
                INR 60,000 for Normal & C-Section for first two living children,
                waiting period waived off{" "}
              </div>

              <div className="label_head pad-t10">Health Coverages</div>
              <div className="head_content">
                <ul>
                  <li>Congenitial internal disease</li>
                  <li>Congenitial External disease(life threatening cases)</li>
                  <li>Lasik Surgery</li>
                  <li>Oral Chemotherapy</li>
                  <li>Air Ambulance(Not covered within 50km radius)</li>
                </ul>
              </div>

              <div className="label_head pad-t10">
                Re-imbursementclaims reporting / submitting period
              </div>
              <div className="head_content">
                Within 30 days from the date of discharge
              </div>

              <div className="label_head pad-t10">
                Emergency Ambulance Charges
              </div>
              <div className="head_content">1% of the Sum Insured</div>

              <div className="label_head pad-t10">Co-pay Clause</div>
              <div className="head_content">
                Flat 20% Co-pay on all parental claims
              </div>

              <div className="label_head pad-t10">Infertility Treatment</div>
              <div className="head_content">
                within Maternity Limit (50K) for OPD/IPD claims
              </div>

              <div className="label_head pad-t10">Bereavement Benefit</div>
              <div className="head_content">
                Coverage of employees’ dependents in case of his unfortunate
                death/ Up to 100% of sum assured.
              </div>

              <div className="label_head pad-t10">
                Room rent/Room type restriction
              </div>
              <div className="head_content">
                <ul>
                  <li>
                    Room rent for Base + Enhanced sum insured (Up to 6 lakhs) :
                    1% for Normal & 2% for ICU
                  </li>
                  <li>
                    Room rent for Base + Enhanced sum insured (7 Lakhs - 10
                    lakhs) : INR 6000 for Normal and INR 12000 for ICU
                  </li>
                  <li>No proportionate deduction clause</li>
                </ul>
              </div>

              <div className="label_head pad-t10">
                Coverage for 3rd and 4th child
              </div>
              <div className="head_content">
                Covered if 3rd and 4th Child (in case of twins/ triplets)
              </div>

              <div className="label_head pad-t10">Animal/Serpant attack</div>
              <div className="head_content">Covered</div>

              <div className="label_head pad-t10">
                Cover for disabled dependant children
              </div>
              <div className="head_content">
                Covered without any upper age limit
              </div>
            </div>

            <div
              style={{
                margin: "10px 0px",
                height: "0px",
                border: "0.5px solid rgba(0, 0, 0, 0.1)",
              }}
            ></div>

            <div id="section4">
              <div className="sub_head pad-b5">Frequently Asked Questions</div>
              <div>
                <div className="policy_head"> What is a Mediclaim policy?</div>
                <div className="policy_cnt2">
                  <div className="txt_styel">
                    Mediclaim policy reimburses hospitalization expenses
                    incurred as an inpatient for the treatment of sickness or
                    accident occurring during the period of insurance.
                  </div>
                </div>

                <div className="policy_head">
                  {" "}
                  What is the duration of the policy?
                </div>
                <div className="policy_cnt2">
                  <div className="txt_styel">
                    One year effective from the date of inception of policy ,
                    i.e., Date 13-10-2020 to 12-10-2021
                  </div>
                </div>

                <div className="policy_head">
                  Is there a minimum time limit for stay within the hospital
                  under Mediclaim?
                </div>
                <div className="policy_cnt2">
                  <div className="txt_styel">
                    Under Mediclaim, the minimum stay within the hospital must
                    be for a minimum of 24 hours. However for dialysis,
                    chemotherapy, eye surgery, etc (as per the defined list) –
                    the stay can be for less than 24 hours.
                  </div>
                </div>

                <div className="policy_head">
                  {" "}
                  Who is a Third Party Administrator (TPA)?
                </div>
                <div className="policy_cnt2">
                  <div className="txt_styel">
                    TPA is an agency appointed by the insurance company to take
                    care of claim settlements in health insurance. , Mediassist
                    is the TPA for Altimetrik India.
                  </div>
                </div>

                <div className="policy_head"> Who is an Insurance Broker ?</div>
                <div className="policy_cnt2">
                  <div className="txt_styel">
                    An Insurance broker is appointed by Altimetrik to assist you
                    in all your Insurance related requirements. FUTURISK is your
                    Insurance Broker.
                  </div>
                </div>

                <div className="policy_head">What is the Role of Futurisk?</div>
                <div className="policy_cnt2">
                  <div className="txt_styel">
                    Futurisk is your SPOC for any & all insurance related
                    matters. The dedicated SPOC for your convenience to assist
                    you with claims/planned hospitalization/retail policies etc.
                    In short, an Insurance concierge right within your reach!!
                  </div>
                </div>

                <div className="policy_head"> What is a floater policy?</div>
                <div className="policy_cnt2">
                  <div className="txt_styel">
                    A Floater policy is a single policy that takes care of the
                    hospitalization expenses of your entire family who has been
                    declared at the time of taking the policy. Any member of
                    your family or all put together can claim up to the maximum
                    sum insured.
                  </div>
                </div>

                <div className="policy_head"> What is Sum Insured?</div>
                <div className="policy_cnt2">
                  <div className="txt_styel">
                    Sum insured is the maximum amount that can be claimed under
                    the policy. This is the limit for the policy period.{" "}
                  </div>
                </div>

                <div className="policy_head">
                  {" "}
                  Where does claimant have to submit documents for
                  “Reimbursement cases”?
                </div>
                <div className="policy_cnt2">
                  <div className="txt_styel">
                    Claimant have to submit all required documents in case of
                    REIMBURSEMENT CASES to Futurisk Bangalore Office only, and
                    not to the TPA - Mediassist
                  </div>
                </div>

                <div className="policy_head">
                  {" "}
                  What is the duration of claim documents submissionin case of
                  “REIMBURSEMENT CASES” ?
                </div>
                <div className="policy_cnt2">
                  <div className="txt_styel">
                    Within 30 days from Date of Discharge (Hospital)
                  </div>
                </div>

                <div className="policy_head">
                  {" "}
                  Who will be the SPOC for all queries / Grievances ?
                </div>
                <div className="policy_cnt2">
                  <div className="txt_styel">
                    From Futurisk Mr.M.Vignesh, e-mail ID :
                    m.vignesh@futurisk.in , Mob: +91 9900237667
                  </div>
                </div>

                <div className="policy_head"> Parent is covered?</div>
                <div className="policy_cnt2">
                  <div className="txt_styel">Covered if enrolled</div>
                </div>

                <div className="policy_head">
                  {" "}
                  Is Mid term inclusion is allowed?
                </div>
                <div className="policy_cnt2">
                  <div className="txt_styel">
                    Mid-term inclusion of dependents for existing employees not
                    allowed (Exception: Newly Married Spouse & new born baby)
                  </div>
                </div>

                <div className="policy_head"> Sum insured enhancement?</div>
                <div className="policy_cnt2">
                  <div className="txt_styel">
                    Covered if opted during the enrolment drive, base policy
                    members covered will be enrolled in top-up as well.
                  </div>
                </div>
              </div>
            </div>

            <div
              style={{
                margin: "10px 0px",
                height: "0px",
                border: "0.5px solid rgba(0, 0, 0, 0.1)",
              }}
            ></div>

            <div id="section5">
              <div className="sub_head pad-b10">Contact Details</div>

              <div className="sub_head pad-b10">Futurisk</div>
              <div className="cnt_head pad-b5">First Point of Contact</div>
              <div className="cnt_txt">Mr. M Vignesh</div>
              <div className="cnt_txt">Email ID : m.vignesh@futurisk.in</div>
              <div className="cnt_txt">
                Assistant Manager-Customer Experience
              </div>
              <div className="cnt_txt">Mobile:+91-9900237667</div>

              <div className="cnt_head pad-t20 pad-b5">
                First point of Escalation
              </div>
              <div className="cnt_txt">Ms. Indu</div>
              <div className="cnt_txt">Email ID: indu@futurisk.in</div>
              <div className="cnt_txt">Manager- Customer Experience</div>
              <div className="cnt_txt">Mobile +91 – 9972822003</div>

              <div className="cnt_txt pad-t20">Mr. John Antony</div>
              <div className="cnt_txt">Email ID: john@futurisk.in</div>
              <div className="cnt_txt">Senior AVP- Customer Experience </div>
              <div className="cnt_txt">Mobile +91 – 9900013118</div>

              <div className="sub_head pad-t20">Mediassist</div>

              <div class="page_img">
                <img
                  src="/images/covidhdbg2.png"
                  width="100%"
                  height="90"
                  alt="Image"
                />
                <div class="top-center toll_free_txt">Toll free number</div>
                <div class="top-center toll_free_contact">1800 419 9449</div>
              </div>

              <div className="cnt_head pad-b5">First Point of Contact</div>
              <div className="cnt_txt">Ms.Ushakiran</div>
              <div className="cnt_txt">
                Email ID :ushakiran.lukram@mediassist.in
              </div>
              <div className="cnt_txt">Mobile:+91- 6366765434</div>

              <div className="cnt_head pad-t20 pad-b5">
                First point of Escalation
              </div>
              <div className="cnt_txt">Noor Ahmed Shariff</div>
              <div className="cnt_txt">
                Email ID: noor.shariff@Mediassist.in{" "}
              </div>
              <div className="cnt_txt">Mobile +91 – 7022969920</div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}

export default GeneralInsuranceScreen;