import React, { useEffect, useState, useRef } from "react";
import "./css/DashboardGeneralInfo.css";
import { useHistory } from "react-router";
import { STRING, IMAGES, COLORS } from "../utils/constants";
import { useDispatch, useSelector } from "react-redux";
import API from "../utils/api/API";

export default function DashboardGeneralInfo() {
  const history = useHistory();
  const [accName, setAccName] = useState("");
  const [workLocation, setWorkLocation] = useState("");

  const [err, setERR] = useState("");
  const data = useSelector((state) => state.dashboardSum);

  let WORK_LOCATION = ["Chennai", "Hyderabad", "Pune", "Bangalore"];
  const jwt = useSelector((state) => state.jwt);

  function OnClickBackBtn() {
    history.push("/dashboardUserProfile");
  }
  const roles = useSelector((state) => state.roles);

  const [isEditBtnClicked, setIsEditBtnClicked] = useState(false);

  function OnClickSaveBtn() {
    API.put(
      process.env.REACT_APP_EMPLOYEE_DETAILS + data.employeeDetails.id,
      {
        _id: `${data.employeeDetails._id}`,
        accountName: `${accName}`,
        alternativeContactNo1: data.employeeDetails.alternativeContactNo1,
        alternativeContactNo2: data.employeeDetails.alternativeContactNo2,
        currentLocation: `${data.employeeDetails.currentLocation}`,
        dob: `${data.employeeDetails.dob}`,
        emailAddr: `${data.employeeDetails.emailAddr}`,
        emergencyContactName: `${data.employeeDetails.emergencyContactName}`,
        emergencyContactNumber: data.employeeDetails.emergencyContactNumber,
        emergencyContactRelation: `${data.employeeDetails.emergencyContactRelation}`,
        empId: data.employeeDetails.empId,
        empName: `${data.employeeDetails.empName}`,
        gender: `${data.employeeDetails.gender}`,
        hrbpName: `${data.employeeDetails.hrbpName}`,
        id: `${data.employeeDetails.id}`,
        isLocationUpdated: true,
        primaryContactNo: data.employeeDetails.primaryContactNo,
        workLocation: `${workLocation}`,
        roles: `${data.employeeDetails.roles}`,
      },
      {
        headers: {
          Authorization: `Bearer ${jwt}`,
        },
      }
    )
      .then((res) => {
        console.log(res);
        setIsEditBtnClicked(false);
        history.push("/dashboardUserProfile");
      })
      .catch((err) => {
        history.push("/");

        console.log(err.res);
        setERR("Internal Error");
        console.log(err);
      });
  }

  function OnClickEditBtn() {
    setIsEditBtnClicked(true);
  }

  function getAccName(val) {
    setAccName(val.target.value);
  }

  function OnClickCancelBtn() {
    setIsEditBtnClicked(false);
    setAccName(data.employeeDetails.accountName);
    setWorkLocation(data.employeeDetails.workLocation);

  }

  function onFocus() {
    setERR("");
  }

  useEffect(() => {
    setAccName(data.employeeDetails.accountName);
    setWorkLocation(data.employeeDetails.workLocation);
    return () => {};
  }, []);

  function handleChange(e) {
    setWorkLocation(e.target.value);
  }

  return (
    <div className="page-container">
      <div className="content-wrap">
        <div style={{ height: "94%" }} className="generalinfo_container">
          <div id="generalinfo">
            <div className="backbtn_block">
              <div onClick={OnClickBackBtn} className="backbtn_img">
                <img src="/images/Black Back Button.png" alt="back btn" />
              </div>
              <div className="backbtn_txt">General Info</div>
              {roles.includes("HRBP") ? (
                <div>
                  {isEditBtnClicked ? (
                    <div>{null}</div>
                  ) : (
                    <div onClick={OnClickEditBtn} className="backbtn_link">
                      <span className="check_edit_btn pad-lr10 float_r">
                        Edit
                      </span>
                    </div>
                  )}
                </div>
              ) : (
                <div>{null}</div>
              )}
            </div>

            <div className="generalinfo_body">
              <div className="label_head pad-t10">Name</div>
              <div className="head_content head_content_edit">
                {data.employeeDetails.empName}
              </div>

              <div className="label_head pad-t10">EMP ID</div>
              <div className="head_content head_content_edit">{data.employeeDetails.empId}</div>

              <div className="label_head pad-t10">Email Address</div>
              <div className="head_content head_content_edit">
                {data.employeeDetails.emailAddr}
              </div>

              <div className="label_head pad-t10">HRBP Name</div>
              <div className="head_content head_content_edit">
                {" "}
                {data.employeeDetails.hrbpName}
              </div>

              <div className="clm-2">
                <div className="clm1">
                  <div className="label_head pad-t10">Base Location</div>
                  <div className="head_content head_content_edit">
                    <select
                      onChange={handleChange}
                      disabled={!isEditBtnClicked}
                      className="dropdown_selection"
                    >
                      {console.log(data.employeeDetails.workLocation)}

                      {WORK_LOCATION.map(
                        (el, id) =>
                          data.employeeDetails.workLocation === el ? (
                            <option
                              key={id}
                              className="head_content"
                              value={workLocation}
                              selected="selected"
                            >
                              {el}
                            </option>
                          ) : (
                            <option key={id} className="head_content">
                              {el}
                            </option>
                          )

                        // <option className="head_content">{el}</option>
                      )}
                    </select>
                  </div>
                </div>
                <div className="clm2">
                  <div className="label_head pad-t10">DOB</div>
                  <div className="head_content head_content_edit">
                    {" "}
                    {data.employeeDetails.dob}
                  </div>
                </div>
              </div>

              <div className="clm-2">
                <div className="clm1">
                  <div className="label_head pad-t10">Account Name</div>
                  <input
                    className="head_content head_content_edit"
                    style={{
                      borderWidth: 1,
                      borderRadius: 3,
                      fontSize: "10px",
                      backgroundColor: COLORS.WHITE,
                      paddingLeft: "5px",
                      width: "70%",
                      height: "20px",
                    }}
                    disabled={!isEditBtnClicked}
                    onFocus={onFocus}
                    value={accName}
                    placeholder="Enter Account Name"
                    type="text"
                    onChange={getAccName}
                  ></input>
                </div>
                <div className="clm2">
                  <div className="label_head pad-t10">Gender</div>
                  <div className="head_content head_content_edit">
                    {" "}
                    {data.employeeDetails.gender}
                  </div>
                </div>
              </div>

              {isEditBtnClicked ? (
                <div
                  style={{
                    display: "flex",
                    flexDirection: "row",
                    justifyContent: "center",
                    paddingTop: "20px",
                  }}
                >
                  <div style={{ margin: "10px" }} onClick={OnClickCancelBtn}>
                    <span
                      style={{ backgroundColor: "#95979F" }}
                      className="check_edit_btn pad-lr10 "
                    >
                      Cancel
                    </span>
                  </div>
                  <div style={{ margin: "10px" }} onClick={OnClickSaveBtn}>
                    <span className="check_edit_btn pad-lr10 ">Save</span>
                  </div>
                </div>
              ) : (
                <div>{null}</div>
              )}
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}
