import React, { useEffect, useState } from "react";
import "./css/DashboardSumary.css";
import "./css/Dashboard_HealthAssessment.css";
import { useDispatch, useSelector } from "react-redux";
import { STRING, IMAGES, COLORS } from "../utils/constants";
import { useHistory } from "react-router";
import moment from "moment";
import { Notes } from "../components/Notes";
import { AddNotes } from "../components/AddNotes";
import { EditNotes } from "../components/EditNotes";
import Modal from "react-modal";
import API from "../utils/api/API";

Modal.setAppElement("#root");

export default function ProfileNotesScreen(props) {
  const data = useSelector((state) => state.dashboardSum);
  const history = useHistory();
  const jwt = useSelector((state) => state.jwt);
  const [loading, setLoading] = useState(true);
  const [notesData, setNotesData] = useState([]);

  const [editNotesModalOpen, setEditNotesModalOpen] = useState(false);
  const [newNoteModalOpen, setNewNoteModalOpen] = useState(false);
  const [notesFromServer, setNotesFromServer] = useState([]);
  const [notesDescription, setNotesDescription] = useState("");
  const empID = useSelector((state) => state.empID);
  const empName = useSelector((state) => state.empName);
  const otherEmpDetail = useSelector((state) => state.otherEmpDetail);
  const role = useSelector((state) => state.roles);

  function OnClickBackBtn() {
    history.push("/dashboardUserProfile");
  }

  const customStyles = {
    content: {
      top: "60%",
      left: "50%",
      right: "auto",
      bottom: "-40%",
      transform: "translate(-50%, -50%)",
      borderRadius: "10px",
      width: "82%",
    },
  };

  function OpenEditModal(desc, data) {
    setNotesData(data);

    setNotesDescription(desc);
    setEditNotesModalOpen(true);
  }

  function OpenNewNoteModal() {
    setNewNoteModalOpen(true);
  }

  function SetNotesDes(val) {
    console.log(val.target.value);
    setNotesDescription(val.target.value);
  }

  function AddNotesToServer() {
    API.post(
      process.env.REACT_APP_EMPLOYEE_HRBP_NOTES_POST,
      {
        personId: otherEmpDetail.empId,
        createdBy: empID,
        updatedBy: empID,
        notes: notesDescription,
      },
      {
        headers: {
          Authorization: `Bearer ${jwt}`,
        },
      }
    )
      .then((res) => {
        console.log(res.data);
      })
      .catch((err) => {
        switch (err.response.status) {
          case 500:
            break;
          case 400:
            break;

          case 401:
            history.push("/");

            break;
          case 403:
            history.push("/");
            break;

          default:
            break;
        }

        throw err;
      });
  }

  function EditNotesToServer() {
    API.put(
      process.env.REACT_APP_EMPLOYEE_HRBP_NOTES_POST + "/" + notesData._id,
      {
        _id: notesData._id,
        personId: otherEmpDetail.empId,
        createdBy: notesData.createdBy,
        createdAt: notesData.createdAt,
        updatedBy: empID,
        notes: notesDescription,
      },
      {
        headers: {
          Authorization: `Bearer ${jwt}`,
        },
      }
    )
      .then((res) => {
        console.log(res.data);
      })
      .catch((err) => {
        switch (err.response.status) {
          case 500:
            break;
          case 400:
            break;

          case 401:
            history.push("/");

            break;
          case 403:
            history.push("/");
            break;

          default:
            break;
        }

        throw err;
      });
  }

  useEffect(() => {
    setLoading(false);

    if (role.includes("DASHBOARD") || role.includes("HRBP")) {
      API.get(
        process.env.REACT_APP_EMPLOYEE_HRBP_NOTES_GET + otherEmpDetail.empId,
        {
          headers: {
            Authorization: `Bearer ${jwt}`,
          },
        }
      )
        .then((res) => {
          console.log(res.data);
          setNotesFromServer(res.data);
          //setData(res.data[0].options);
        })
        .catch((err) => {
          switch (err.response.status) {
            case 500:
              break;
            case 400:
              break;

            case 401:
              history.push("/");

              break;
            case 403:
              history.push("/");
              break;

            default:
              break;
          }

          throw err;
        });
    }
    if (jwt === "") {
      history.push("/");
    }
    return () => {};
  }, []);

  return (
    <div className="page-container">
      <div className="content-wrap">
        {loading ? (
          "Loading..."
        ) : (
          <div style={{maxWidth:"650px",margin:"0 auto"}}>
            <EditNotes
              notes={notesDescription}
              modalOpen={editNotesModalOpen}
              getNotesDescription={SetNotesDes}
              onclickClose={() => setEditNotesModalOpen(false)}
              onClickUpdate={EditNotesToServer}
            ></EditNotes>

            <AddNotes
              modalOpen={newNoteModalOpen}
              onclickClose={() => setNewNoteModalOpen(false)}
              getNotesDescription={SetNotesDes}
              onClickAdd={AddNotesToServer}
            ></AddNotes>

            <div
              style={{
                padding: "20px 5% 5px 5%",
                margin: "auto",
                maxWidth: "650px",
                borderRadius: "5px",
                display:"inline-block",
                width: "90%",
              }}
            >
              <img
              src={IMAGES.BackBtn}
              alt="back btn"
              onClick={OnClickBackBtn}
              className="backbtn_img"
              
              style={{ width: "32px", height: "32px" }}
            ></img>
              <span
                style={{
                  color: "#1A1C22",
                  fontFamily: "Inter",
                  fontStyle: "normal",
                  fontWeight: "600",
                  fontSize: "17px",
                  paddingLeft: "10px",
                  paddingTop: "5px",
                }}
                className="backbtn_txt"
              >
                HRBP Notes
              </span>

              <button
                onClick={OpenNewNoteModal}
                className="float_r"
                style={{
                  backgroundColor: "#4D59FC",
                  color: "#fff",
                  fontFamily: "Inter",
                  fontStyle: "normal",
                  fontWeight: "500",
                  padding: "0px 10px 0px 10px",
                  height: "28px",
                  fontSize: "14px",
                  lineHeight: "20px",
                  justifyContent: "center",
                  alignContent: "center",
                  alignItems: "center",
                  borderRadius: "50px",
                  borderColor: "transparent",
                }}
              >
                New Note
              </button>
            </div>
            <div className="CategoryWithoutPadding1">
              {notesFromServer.length > 0 ? (
                notesFromServer.map((el, id) => (
                  <Notes
                    key={id}
                    onEditClick={() => OpenEditModal(el.notes, el)}
                    header={
                      el.updatedByName +
                      "posted on " +
                      moment(
                        el.createdAt,
                        "YYYY-MM-DDTHH:mm:ss.000ZZ"
                      ).fromNow()
                    }
                    description={el.notes}
                  ></Notes>
                ))
              ) : (
                <div style={{ paddingTop: "30px", textAlign: "center" }}>
                  no notes to display
                </div>
              )}{" "}
            </div>
          </div>
        )}
      </div>
    </div>
  );
}
