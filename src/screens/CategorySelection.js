import React, { useState, useEffect } from "react";
import "./css/CategorySelection.css";
import { useHistory } from "react-router";
import { useDispatch, useSelector } from "react-redux";
import { SetQuizModuleName, SetQuizCategory } from "../redux/action";
import Footer from "../components/Footer";
import API from "../utils/api/API";
import { STRING, IMAGES, COLORS } from "../utils/constants";
import Breadcrumbs from "../components/Breadcrumbs";
import { Notes } from "../components/Notes";
import { AddNotes } from "../components/AddNotes";
import { EditNotes } from "../components/EditNotes";

import Modal from "react-modal";
Modal.setAppElement("#root");
import moment from "moment";
import { ProfileCard } from "../components/ProfileCard";

export default function CategorySelection() {
  const [data, setData] = useState([]);
  const [question, setQuestion] = useState("");
  const [category, setCategory] = useState("INITIAL");
  const [editModalOpen, setEditModalOpen] = useState(false);
  const [newNoteModalOpen, setNewNoteModalOpen] = useState(false);
  const [notesFromServer, setNotesFromServer] = useState([]);
  const [notesDescription, setNotesDescription] = useState("");
  const [notesData, setNotesData] = useState([]);

  const dispatch = useDispatch();
  const jwt = useSelector((state) => state.jwt);
  const role = useSelector((state) => state.roles);
  const empID = useSelector((state) => state.empID);
  const empName = useSelector((state) => state.empName);
  const empMail = useSelector((state) => state.empMail);
  const assessmentType = useSelector((state) => state.assessmentType);

  const otherEmp = useSelector((state) => state.otherEmpDetail);

  const history = useHistory();

  function OpenEditModal(description, data) {
    setNotesData(data);
    setNotesDescription(description);
    setEditModalOpen(true);
  }

  function OpenNewNoteModal() {
    setNewNoteModalOpen(true);
  }

  function SetNotesDes(val) {
    console.log(val.target.value);
    setNotesDescription(val.target.value);
  }

  function AddNotesToServer() {
    API.post(
      process.env.REACT_APP_EMPLOYEE_HRBP_NOTES_POST,
      {
        personId: otherEmp.empId,
        createdBy: empID,
        updatedBy: empID,
        notes: notesDescription,
      },
      {
        headers: {
          Authorization: `Bearer ${jwt}`,
        },
      }
    )
      .then((res) => {
        console.log(res.data);
      })
      .catch((err) => {
        switch (err.response.status) {
          case 500:
            break;
          case 400:
            break;

          case 401:
            history.push("/");

            break;
          case 403:
            history.push("/");
            break;

          default:
            break;
        }

        throw err;
      });
  }

  function EditNotesToServer() {
    API.put(
      process.env.REACT_APP_EMPLOYEE_HRBP_NOTES_POST + "/" + notesData._id,
      {
        _id: notesData._id,
        personId: otherEmp.empId,
        createdBy: notesData.createdBy,
        createdAt: notesData.createdAt,
        updatedBy: empID,
        notes: notesDescription,
      },
      {
        headers: {
          Authorization: `Bearer ${jwt}`,
        },
      }
    )
      .then((res) => {
        console.log(res.data);
      })
      .catch((err) => {
        switch (err.response.status) {
          case 500:
            break;
          case 400:
            break;

          case 401:
            history.push("/");

            break;
          case 403:
            history.push("/");
            break;

          default:
            break;
        }

        throw err;
      });
  }

  useEffect(() => {
    if (role.includes("DASHBOARD") || role.includes("HRBP")) {
      API.get(process.env.REACT_APP_EMPLOYEE_HRBP_NOTES_GET + otherEmp.empId, {
        headers: {
          Authorization: `Bearer ${jwt}`,
        },
      })
        .then((res) => {
          console.log(res.data);
          setNotesFromServer(res.data);
          //setData(res.data[0].options);
        })
        .catch((err) => {
          switch (err.response.status) {
            case 500:
              break;
            case 400:
              break;

            case 401:
              history.push("/");

              break;
            case 403:
              history.push("/");
              break;

            default:
              break;
          }

          throw err;
        });
    }

    API.get(process.env.REACT_APP_EMPLOYEE_CATEGORY + category, {
      headers: {
        Authorization: `Bearer ${jwt}`,
      },
    })
      .then((res) => {
        setQuestion(res.data[0]);
        setData(res.data[0].options);
      })
      .catch((err) => {
        switch (err.response.status) {
          case 500:
            break;
          case 400:
            break;

          case 401:
            history.push("/");

            break;
          case 403:
            history.push("/");
            break;

          default:
            break;
        }

        throw err;
      });
    return () => {};
  }, []);

  function OnClickBackBtn() {
    history.push("/home");
  }

  function OnClickHealth(temp) {
    console.log(temp);
    dispatch(
      SetQuizCategory(
        question.questId + "~" + question.quesDesc + "~" + temp.optValue
      )
    );
    dispatch(SetQuizModuleName(temp.optId));
    history.push("/home/healthStatus/questions");
  }
  return (
    <div className="page-container">
      <div className="content-wrap">
        <div style={{ height: "94%" }} className="Category">
          <div
            style={{
              flexDirection: "row",
              display: "flex",
              paddingTop: "50px",
            }}
          >
            <img
              src={IMAGES.BackBtn}
              alt="back btn"
              onClick={OnClickBackBtn}
              style={{ width: "32px", height: "32px" }}
            ></img>
            <h3
              style={{
                color: "#1A1C22",
                fontFamily: "Inter",
                fontStyle: "normal",
                fontWeight: "600px",
                fontSize: "17px",
                paddingLeft: "10px",
                paddingTop: "5px",
              }}
            >
              Health Assessment
            </h3>
          </div>

          <Breadcrumbs />

            {console.log(otherEmp)}
            
          <div style={{ marginTop: "10px" }}>
            {assessmentType === "SELF" ? (
              <ProfileCard Header={empName} subHeader={empMail} />
            ) : (
              <ProfileCard
                Header={otherEmp.empName}
                subHeader={otherEmp.emailAddr}
              />
            )}
          </div>

          <div
            style={{
              margin:"20px 0px",
              height: "0px",
              border: "0.5px solid rgba(0, 0, 0, 0.1)",
              marginLeft: "-16px",
              marginRight: "-16px",
            }}>
        </div>

          <h5
            style={{
              color: "#1A1C22",
              fontFamily: "Inter",
              fontStyle: "normal",
              fontWeight: "600px",
              fontSize: "15px",
              paddingBottom: "10px",
            }}
          >
            Your Current Health Status
          </h5>

          <div>
            {data.map((el) => (
              <button
                style={{
                  width: "98%",
                  height: "6%",
                  padding: "10px 2%",
                  marginBottom: 20,
                  border: "0px",
                  backgroundColor: "#fff",
                  fontSize: "15px",
                  lineHeight: "18px",
                  borderRadius: "5px",
                  fontWeight: "600",
                  textAlign: "left",
                  color: "#252A31",
                  border: ".5px solid rgba(0, 0, 0, 0.1)",
                }}
                key={el.id}
                onClick={() => OnClickHealth(el)}
              >
                {el.optValue}
              </button>
            ))}
          </div>
          {console.log(role)}

          {role.includes("DASHBOARD") || role.includes("HRBP") ? (
            <NotesShow></NotesShow>
          ) : (
            <div>{null}</div>
          )}

          <EditNotes
            notes={notesDescription}
            modalOpen={editModalOpen}
            getNotesDescription={SetNotesDes}
            onclickClose={() => setEditModalOpen(false)}
            onClickUpdate={EditNotesToServer}
          ></EditNotes>

          <AddNotes
            modalOpen={newNoteModalOpen}
            onclickClose={() => setNewNoteModalOpen(false)}
            getNotesDescription={SetNotesDes}
            onClickAdd={AddNotesToServer}
          ></AddNotes>
        </div>
      </div>
      <Footer></Footer>
    </div>
  );

  function NotesShow() {
    return (
      <div>
        <div style={{ margin: "auto", maxWidth: "650px", borderRadius: "5px" }}>
          <span
            style={{
              color: "#1A1C22",
              fontFamily: "Inter",
              fontStyle: "normal",
              fontWeight: "600",
              fontSize: "17px",

              paddingTop: "5px",
            }}
          >
            HRBP Notes
          </span>

          <button
            onClick={OpenNewNoteModal}
            className="float_r mar-r10"
            style={{
              backgroundColor: "#4D59FC",
              color: "#fff",
              fontFamily: "Inter",
              fontStyle: "normal",
              fontWeight: "500",

              height: "28px",
              fontSize: "15px",
              lineHeight: "20px",
              justifyContent: "center",
              alignContent: "center",
              alignItems: "center",
              borderRadius: "50px",
              borderColor: "transparent",
            }}
          >
            New Note
          </button>
        </div>
        <div className="pad-t20" style={{marginLeft:"-16px", marginRight:"-16px"}}>
          {notesFromServer.length > 0 ? (
            notesFromServer.map((el, id) => (
              <Notes
                key={id}
                onEditClick={() => OpenEditModal(el.notes, el)}
                header={
                  el.updatedByName +
                  " posted on " +
                  moment(el.updatedAt, "YYYY-MM-DDTHH:mm:ss.000ZZ").fromNow()
                }
                description={el.notes}
              ></Notes>
            ))
          ) : (
            <div style={{ paddingTop: "30px", textAlign: "center" }}>
              no notes to display
            </div>
          )}{" "}
        </div>
      </div>
    );
  }
}
