import React, { useState, useEffect } from "react";
import { Button } from "../components/Button";
import "./css/QuizScreen.css";
import { useHistory } from "react-router";
import Checkbox from "react-custom-checkbox";
import { useSelector, useDispatch } from "react-redux";
import Footer from "../components/Footer";
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";
import API from "../utils/api/API";
import { SetQuizSummary } from "../redux/action";

import { STRING, IMAGES, COLORS } from "../utils/constants";
import Breadcrumbs from "../components/Breadcrumbs";

export default function QuizScreen() {
  const [data, setData] = useState([]);
  const [loading, setLoading] = useState(true);
  const [clicked, setClicked] = useState(false);
  const [id, setID] = useState([]);
  const [err, setERR] = useState("");

  const [questionIndex, setQuestionIndex] = useState(0);
  const history = useHistory();
  const dispatch = useDispatch();

  const quizModuleName = useSelector((state) => state.quizModuleName);
  const empID = useSelector((state) => state.empID);
  const uniqueID = useSelector((state) => state.uniqueID);
  const _id = useSelector((state) => state._uniqueID);
  const initialQuestion = useSelector((state) => state.categoryID);
  const jwt = useSelector((state) => state.jwt);

  const [answerData, setAnswerData] = useState("");
  const [dummyData, setDummyData] = useState("");
  const [success, setSuccess] = useState(false);

  function getData(val) {
    setDummyData(val.target.value);
  }

  function OnClickCheckBox(id, temp) {
    setDummyData(temp);
    setID(id);
    setClicked(!clicked);
  }

  function AnsValidation() {
    var regex = new RegExp(data[questionIndex].regex);

    console.log(data[questionIndex].regex);
    if (data[questionIndex].regex !== null) {
      if (regex.test(dummyData)) {
        setLoading();
        if (dummyData !== null && dummyData !== "") {
          setDummyData("null");
        }

        var jsonServerData = data[questionIndex];

        if (questionIndex < data.length - 1) {
          if (questionIndex == 0) {
            setAnswerData(
              data[questionIndex].questId +
                "~" +
                data[questionIndex].quesDesc +
                "~" +
                dummyData +
                "~" +
                JSON.stringify(jsonServerData)
            );
          } else {
            setAnswerData(
              data[questionIndex].questId +
                "~" +
                data[questionIndex].quesDesc +
                "~" +
                dummyData +
                "~" +
                JSON.stringify(jsonServerData) +
                "&" +
                answerData
            );
          }
          console.log(answerData);

          setDummyData("");
          setQuestionIndex(questionIndex + 1);
        } else {
          setAnswerData(
            data[questionIndex].questId +
              "~" +
              data[questionIndex].quesDesc +
              "~" +
              dummyData +
              "~" +
              JSON.stringify(jsonServerData) +
              "&" +
              answerData +
              "&" +
              initialQuestion
          );
          setSuccess(true);

          setDummyData("");

          //console.log(answerData);
          //API
        }
      } else {
        setERR(data[questionIndex].errorMessage);
        console.log("nope");
      }
    }else{
      AnsSendingNoVal();
    }
  }

  function AnsSendingNoVal() {
    setLoading();
    if (dummyData !== null && dummyData !== "") {
      setDummyData("null");
    }

    var jsonServerData = data[questionIndex];

    if (questionIndex < data.length - 1) {
      if (questionIndex == 0) {
        setAnswerData(
          data[questionIndex].questId +
            "~" +
            data[questionIndex].quesDesc +
            "~" +
            dummyData +
            "~" +
            JSON.stringify(jsonServerData)
        );
      } else {
        setAnswerData(
          data[questionIndex].questId +
            "~" +
            data[questionIndex].quesDesc +
            "~" +
            dummyData +
            "~" +
            JSON.stringify(jsonServerData) +
            "&" +
            answerData
        );
      }
      console.log(answerData);

      setDummyData("");
      setQuestionIndex(questionIndex + 1);
    } else {
      setAnswerData(
        data[questionIndex].questId +
          "~" +
          data[questionIndex].quesDesc +
          "~" +
          dummyData +
          "~" +
          JSON.stringify(jsonServerData) +
          "&" +
          answerData +
          "&" +
          initialQuestion
      );
      setSuccess(true);

      setDummyData("");
    }
  }

  function OnClickNextBtn() {
    setERR("");

    if (data[questionIndex].required) {
      if (dummyData !== null && dummyData !== "") {
        AnsValidation();
      } else {
        setERR("Cannot leave field empty");
      }
    } else {
      if (dummyData !== null && dummyData !== "") {
        AnsValidation();
      } else {
        AnsSendingNoVal();
      }
    }
  }
  function OnClickBackBtn() {
    setDummyData("");
    if (questionIndex > 0) {
      setQuestionIndex(questionIndex - 1);
    } else {
      history.push("/home/healthStatus");
    }
  }

  function onFocus() {
    setERR("");
  }

  function MakeJson() {
    var array = answerData.split("&");
    var array2;
    var eachItem = {
      id: uniqueID,
      _id: _id,
      empId: empID,
      date: `${new Date(
        new Date().toString().split("GMT")[0] + " UTC"
      ).toISOString()}`,
      response: [],
    };

    eachItem.response = array.map((el, i) => {
      array2 = array[i].split("~");
      return {
        quesId: array2[0],
        quesDesc: array2[1],
        answer: array2[2],
        data: array2[3],
      };
    });

    console.log(eachItem.response);

    dispatch(SetQuizSummary(eachItem.response.reverse()));
    history.push("/home/healthStatus/questions/summary");
  }

  useEffect(() => {
    if (success) {
      MakeJson();
    }
    if (data.length === 0) {
      setLoading(true);
      API.get(process.env.REACT_APP_EMPLOYEE_CATEGORY + quizModuleName, {
        headers: {
          Authorization: `Bearer ${jwt}`,
        },
      })
        .then((res) => {
          console.log(res);
          setData(res.data);
          setLoading(false);
        })
        .catch((err) => {
          switch (err.response.status) {
            case 500:
              break;
            case 400:
              break;

            case 401:
              history.push("/");

              break;
            case 403:
              history.push("/");
              break;

            default:
              break;
          }

          throw err;
        });
    }
  }, [answerData]);

  return (
    <div className="page-container">
      <div className="content-wrap">
        <div style={{ height: "94%" }} className="QuizApp">
          {loading ? (
            <div>{null}</div>
          ) : (
            <div>
              <div
                style={{
                  flexDirection: "row",
                  display: "flex",
                  paddingTop: "50px",
                }}
              >
                <img
                  src={IMAGES.BackBtn}
                  alt="back btn"
                  onClick={OnClickBackBtn}
                  style={{ width: "32px", height: "32px" }}
                ></img>

                <h3
                  style={{
                    color: "#1A1C22",
                    fontFamily: "Inter",
                    fontStyle: "normal",
                    fontWeight: "600px",
                    fontSize: "17px",
                    paddingLeft: "10px",
                    paddingTop: "5px",
                    margin: "1px",
                  }}
                >
                  Health Assessment
                </h3>
              </div>

              <Breadcrumbs />

              {/* <p style={{
               color:"#1A1C22",
               fontFamily:"Inter", 
               fontStyle:"normal",
               fontWeight:"500px",
               fontSize:"16px"}}>QUESTION {questionIndex+1} OF {data.length}</p> */}

              <div style={{ padding: "20px 0px 20px 0px" }}>
                <h5
                  style={{
                    color: "#1A1C22",
                    fontFamily: "Inter",
                    fontStyle: "normal",
                    fontWeight: "600px",
                    fontSize: "15px",
                    textAlign: "left",
                    width: "100%",
                  }}
                >
                  {data[questionIndex].quesDesc}

                  {data[questionIndex].required ? (
                    <span style={{ color: COLORS.ERR_CLR }}>*</span>
                  ) : (
                    <div>{null}</div>
                  )}
                </h5>
              </div>

              <div style={{ display: "flex", flexDirection: "column" }}>
                {data[questionIndex].quesType === "TEXT" ? (
                  <div>
                    <input
                      className="InputField"
                      style={{
                        borderWidth: 1,
                        borderRadius: 3,
                        fontSize: "15px",
                        backgroundColor: COLORS.WHITE,
                        paddingLeft: "2%",
                        width: "98%",
                        height: "44px",
                        border: "0.5px solid rgba(0, 0, 0, 0.1)",
                      }}
                      value={dummyData}
                      placeholder="Enter text"
                      onChange={getData}
                      onFocus={onFocus}
                      type="text"
                    ></input>
                  </div>
                ) : (
                  <div>{null}</div>
                )}

                {data[questionIndex].quesType === "DATE" ? (
                  <div>
                    <DatePicker
                      placeholderText="Select a date"
                      selected={dummyData}
                      onChange={(date) =>{setDummyData(date)}}
                    />
                  </div>
                ) : (
                  <div>{null}</div>
                )}

                {data[questionIndex].quesType === "CHECKBOX" ? (
                  <div>
                    {data[questionIndex].options.map((el) => (
                      <div
                        style={{
                          padding: " 12px 5px 12px 10px",
                          textAlign: "left",
                          width: "98%",
                          height: "10%",
                          backgroundColor: COLORS.WHITE,
                          marginBottom: "10px",
                        }}
                        key={el.id}
                      >
                        <Checkbox
                          icon={
                            <img
                              src={IMAGES.PATH}
                              style={{ width: "10px", height: "10px" }}
                            />
                          }
                          name="my-input"
                          checked={false}
                          onChange={(value) => {
                            if (value) {
                              if (dummyData.includes(el.optValue)) {
                              } else {
                                setDummyData(el.optValue + "," + dummyData);
                              }
                            } else {
                              var str = dummyData;
                              var new_str = str.replace(el.optValue + ",", "");
                              setDummyData(new_str);
                            }
                          }}
                          borderColor="#4D59FC"
                          style={{
                            cursor: "pointer",
                            backgroundColor: "#4D59FC",
                          }}
                          labelStyle={{
                            marginLeft: "10px",
                            userSelect: "none",
                          }}
                          label={el.optValue}
                          right={false}
                        />
                      </div>
                    ))}
                  </div>
                ) : (
                  <div>{null}</div>
                )}

                {data[questionIndex].quesType === "RADIO" ? (
                  <div>
                    {data[questionIndex].options.map((el) => (
                      <button
                        style={{
                          width: "100%",
                          height: "6%",
                          padding: "15px",
                          marginBottom: "10px",
                          border: "0px",
                          backgroundColor: COLORS.WHITE,
                          fontSize: "15px",
                          lineHeight: "18px",
                          borderRadius: "5px",
                          fontWeight: "600",
                          textAlign: "left",
                          color: "#252A31",
                          border: "0.5px solid rgba(0, 0, 0, 0.1)",
                        }}
                        key={el.id}
                        onClick={() => OnClickCheckBox(el.id, el.optValue)}
                      >
                        {id === el.id ? (
                          <img
                            style={{
                              width: "16px",
                              height: "16px",
                              marginRight: "10px",
                              float: "right",
                            }}
                            src={IMAGES.Radio}
                          />
                        ) : (
                          <div>{null}</div>
                        )}
                        {el.optValue}
                      </button>
                    ))}
                  </div>
                ) : (
                  <div>{null}</div>
                )}
              </div>

              <h5
                style={{
                  textAlign: "left",
                  paddingTop: "5px",
                  color: COLORS.ERR_CLR,
                  fontFamily: "Inter",
                  fontStyle: "normal",
                  fontSize: "12px",
                  margin: "5px",
                }}
              >
                {err}
              </h5>

              <div style={{ paddingTop: "30px" }}>
                <Button
                  onClick={OnClickNextBtn}
                  children={
                    questionIndex + 1 === data.length
                      ? `${STRING.NEXT_BTN}`
                      : `${STRING.NEXT_BTN}`
                  }
                ></Button>
              </div>
            </div>
          )}
        </div>
      </div>
      <Footer></Footer>
    </div>
  );
}
