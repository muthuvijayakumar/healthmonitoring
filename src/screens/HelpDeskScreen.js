import React, { useEffect, useRef } from "react";
import "./css/HelpDeskScreen.css";
import { useHistory } from "react-router";
import { useSelector } from "react-redux";
import Breadcrumbs from "../components/Breadcrumbs";

function HelpDeskScreen() {
  const history = useHistory();
  const jwt = useSelector((state) => state.jwt);

  function OnClickBackBtn() {
    history.push("/home");
  }

  useEffect(() => {
    window.scrollTo(0, 0);

    if (jwt === "") {
      history.push("/");
    }
  }, []);

  return (
    <div className="page-container">
      <div className="content-wrap">
        <div style={{ height: "94%" }} className="App">
          <div
            style={{
              flexDirection: "row",
              display: "flex",
              paddingTop: "50px",
            }}
          >
            <img
              onClick={() => OnClickBackBtn()}
              src="/images/Black Back Button.png"
              alt="back btn"
              style={{ width: "32px", height: "32px" }}
            ></img>
            <h3
              style={{
                color: "#1A1C22",
                fontFamily: "Inter",
                fontStyle: "normal",
                fontWeight: "600px",
                fontSize: "17px",
                paddingLeft: "10px",
                paddingTop: "5px",
                margin: "1px",
              }}
            >
              Back
            </h3>
          </div>

          <Breadcrumbs></Breadcrumbs>

          <div class="pagebg_img">
            <div className="bg_img_responsive" style={{backgroundImage: 'url(/images/helpdeskbg.png)'}}>
              <div className="bg_inner_img" style={{backgroundImage: 'url(/images/helpdeskinner.svg)'}}></div>
            </div>
            <div class="top-left1">Company-paid service</div>
            <div class="top-left2">Doctor on Call</div>
            <div class="top-left3">In partnership with Raphacure</div>
            <div class="top-left4">
              Medical Doctors are available 24x7 Psychologists are available 9am
              - 8pm
            </div>
          </div>

          <div class="page_img">
            <img
              src="/images/covidhdbg2.png"
              width="100%"
              height="90"
              alt="Image"
            />
            <div class="top-center toll_free_txt">Toll free number</div>
            <div class="top-center toll_free_contact">9555166000</div>
          </div>

          <div className="helptxt">
            <div className="para">
              COVID Care Helpdesk in partnership with ‘Raphacure’, a service
              partner established by a group of certified doctors. This will act
              as a key instrument to support you and your family for all medical
              clarifications and advices whenever required.
            </div>
          </div>

          <div className="head_content">
            Additional services available only in Bengaluru on self paid basis :{" "}
          </div>
          <div className="li_light">
            <ul>
              <li>COVID screening </li>
              <li>Ambulance services</li>
            </ul>
          </div>

          <div
            style={{
              margin: "10px 0px 20px 0px",
              height: "0px",
              border: "0.5px solid rgba(0, 0, 0, 0.1)",
            }}
          ></div>

          <div class="page_img">
            <div className="bg_img_responsive80" style={{backgroundImage: 'url(/images/altihelpdeskbg.png)'}}>
              <div className="bg_inner_img80" style={{backgroundImage: 'url(/images/altihelpdeskinner.svg)'}}></div>
            </div>
            <div class="top-left img_head">Altimetrik Helpdesk</div>
            <div class="top-left-sml-txt img_sml_txt">
              Contact the following SPOCs for any emergency and escalations.
            </div>
          </div>

          <div id="emp_cnt_details">
            <div className="pry_contact">
              <div className="tbl_head">Single Point of Contact</div>
              <table className="styled-table">
                <thead>
                  <tr>
                    <th style={{ width: "55%" }}>Location</th>
                    <th>SPOC</th>
                  </tr>
                </thead>
                <tbody>
                  <tr>
                    <td>Bangalore</td>
                    <td>
                      <div className="user_Name">Joyson Pinto </div>
                      <div className="user_cnt">+91 9845809892</div>
                      <div className="user_Name">Santosh Mervyn </div>
                      <div className="user_cnt">+91 9900256978</div>
                    </td>
                  </tr>
                  <tr>
                    <td>Chennai</td>
                    <td>
                      <div className="user_Name">Prabu Sanjeevi </div>
                      <div className="user_cnt">+91 9003279877</div>
                      <div className="user_Name">Suresh Jayaraj </div>
                      <div className="user_cnt">+91 9840913481</div>
                    </td>
                  </tr>
                  <tr>
                    <td>Hyderabad</td>
                    <td>
                      <div className="user_Name">Balaji Venkataswamy </div>
                      <div className="user_cnt">+91 9003159118</div>
                      <div className="user_Name">Bakhtiar Khan </div>
                      <div className="user_cnt">+91 9384840592</div>
                    </td>
                  </tr>
                  <tr>
                    <td>Pune</td>
                    <td>
                      <div className="user_Name">Sudhir Satpute </div>
                      <div className="user_cnt">+91 7875890890</div>
                      <div className="user_Name">Rajendra </div>
                      <div className="user_cnt">+91 9763170359</div>
                    </td>
                  </tr>
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}

export default HelpDeskScreen;