import React, { useEffect, useRef } from "react";
import "./css/ThankYouScreen.css";
import { Button } from "../components/Button";
import lottie from "lottie-web";

import { useMsal } from "@azure/msal-react";
import { useHistory } from "react-router";
import { useDispatch, useSelector } from "react-redux";
import moment from "moment";
import { ProfileCard } from "../components/ProfileCard";


function ThankYouScreen() {
  const container = useRef(null);
  const { instance, accounts, inProgress } = useMsal();
  const history = useHistory();
  const jwt = useSelector((state) => state.jwt);
  const empRole = useSelector((state) => state.roles);
  const otherEmpDetail = useSelector((state) => state.otherEmpDetail);
  const assessmentType = useSelector((state) => state.assessmentType);

  useEffect(() => {
    if (jwt === "") {
      history.push("/");
    }

    lottie.loadAnimation({
      container: container.current,
      renderer: "svg",
      loop: true,
      autoplay: true,
      animationData: require("../assets/json/tnq.json"),
    });

    if (inProgress === "none" && accounts.length === 0) {
      console.log(accounts.length);
      history.push("/");
    }
  }, [inProgress, accounts, instance]);

  function OnClickLogout() {
    instance.logout();
  }
  function OnClickHomeBtn() {
    history.push("/home");
  }

  return (
    <div className="page-container">
      <div className="content-wrap">
        <div style={{ height: "94%" }} className="App">
          <div id="tnq_content">
            <div style={{ textAlign: "center", paddingTop: "30px" }}>
              <img
                style={{ textAlign: "center", width: "100%" }}
                align="center"
                src="./images/thankyou.svg"
                alt="Thank You"
              ></img>
            </div>

            <div className="tnq1 pad-b20">
              <div>Thanks for completing your health Assessment.</div>
            </div>

            <div className="nopad_whitebox bg_white">
            <div className="wb_evnt1" style={{textAlign:"center"}}>
             HI
            </div>
            <div className="wb_evnt2 pad-t5" style={{textAlign:"center"}}>
             Hello
            </div>
            </div>

            {assessmentType !== "OTHERS" ? (
              <div id="tnq_body1">
                <div className="tnq2">
                  <div>
                    <div className="container" ref={container}></div>
                  </div>
                  <div style={{ marginLeft: "30px" }}>
                    It is recommended that you take the test everyday once or
                    when there is a change in your health status.
                  </div>
                </div>

                <div
                  style={{
                    height: "0px",
                    border: "0.5px solid rgba(0, 0, 0, 0.1)",
                  }}
                ></div>

                <div className="tnq3">
                  <div>
                    Altimetrik is commited to support you and your family during
                    the pandemic.
                  </div>
                </div>
              </div>
            ) : (
              <div>{null}</div>
            )}

            {assessmentType === "OTHERS" ? (
              <div id="tnq_body2">
                <div
                  style={{
                    height: "0px",
                    border: "0.5px solid rgba(0, 0, 0, 0.1)",
                  }}
                ></div>

                <div className="label_head pad-t10">Name</div>
                <div className="head_content head_content_edit">
                  {otherEmpDetail.empName}
                </div>

                <div className="label_head pad-t10">EMP ID</div>
                <div className="head_content head_content_edit">
                  {" "}
                  {otherEmpDetail.empId}
                </div>

                <div className="label_head pad-t10">Checked-in</div>
                <div className="head_content head_content_edit">
                 {moment().format('DD/MM/yyyy HH:mm')}
                </div>

                <div
                  style={{
                    marginBottom: "5px",
                    height: "0px",
                    border: "0.5px solid rgba(0, 0, 0, 0.1)",
                  }}
                ></div>
              </div>
            ) : (
              <div>{null}</div>
            )}

            <div className="tnq4">
              <div>
                <span>Stay safe!!. </span>Follow prcautionary majors to keep
                yourself and your family safe from COVID-19.
              </div>
            </div>
            {assessmentType !== "OTHERS" ? (
              <div onClick={OnClickLogout} style={{ paddingBottom: "20px" }}>
                <a
                  href="#"
                  className="logout"
                  style={{ width: "100%" }}
                  children="Logout"
                >
                  <img
                    style={{ textAlign: "center" }}
                    align="center"
                    src="/images/logout.svg"
                    alt="logout"
                  ></img>{" "}
                  Logout
                </a>
              </div>
            ) : (
              <div>{null}</div>
            )}

            <div style={{ paddingBottom: "20px" }}>
              <Button
                onClick={OnClickHomeBtn}
                style={{ width: "100%" }}
                children="Back to Home"
              ></Button>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}

export default ThankYouScreen;
