import React, { useState, useEffect } from "react";
import { STRING, IMAGES, COLORS } from "../utils/constants";
import Footer from "../components/Footer";
import { useDispatch, useSelector } from "react-redux";

import { useHistory } from "react-router";
import { TwoNavBar } from "../components/TwoNavBar";
import { HistoryCard } from "../components/HistoryCard";
import API from "../utils/api/API";
import "./css/UserProfileDashboard.css";

import {
  SetDashboardSummary,
  SetOtherEmpDetail,
  SetAssessmentType,
} from "../redux/action";
import moment from "moment";
import { Button1, Button } from "../components/Button";

export default function UserProfileDashboard() {
  const history = useHistory();
  function OnClickBackBtn() {
    if (data.employeeDetails.empName === empName) {
      history.push("/home");
    } else {
      history.push("/dashboardOverview");
    }
    console.log(data);
  }
  const jwt = useSelector((state) => state.jwt);

  const [data, setData] = useState();
  const [initial, setInitial] = useState(true);
  const [myFamily, setMyFamily] = useState([]);

  const [loading, setLoading] = useState(true);

  const empID = useSelector((state) => state.dashboardUID);
  const empName = useSelector((state) => state.empName);
  const roles = useSelector((state) => state.roles);

  const dispatch = useDispatch();

  useEffect(() => {
    if (initial) {
      setLoading(true);

      API.get(
        process.env.REACT_APP_EMPLOYEE_DASHBOARD_SURVEY_EMPLOYEE_ID + empID,
        {
          headers: {
            Authorization: `Bearer ${jwt}`,
          },
        }
      )
        .then((res) => {
          console.log(res);
          setData(res.data);
          
          dispatch(SetOtherEmpDetail(res.data.employeeDetails));

          setLoading(false);
          setInitial(false);
        })
        .catch((err) => {
          switch (err.response.status) {
            case 500:
              break;
            case 400:
              break;

            case 401:
              history.push("/");

              break;
            case 403:
              history.push("/");
              break;

            default:
              break;
          }
          console.log(err);

          throw err;
        });
    }

    return () => {
      setLoading(false);
      // setInitial(true);
    };
  });

  function OnClickGeneralInfo() {
    dispatch(SetDashboardSummary(data));
    history.push("/dashboardGeneralInfo");
  }

  function OnClickContactInfo() {
    console.log(data);
    dispatch(SetDashboardSummary(data));
    history.push("/dashboardContactInfo");
  }

  function OnClickNotes() {
    console.log(data);
    history.push("/HRBP Notes");
  }

  function SetItemValue(value) {
    setVal(value);
    console.log(value);
  }

  const [val, setVal] = useState(0);

  function OnClickCheckIn() {
    dispatch(SetAssessmentType("OTHERS"));
    dispatch(SetOtherEmpDetail(data.employeeDetails));
    console.log(data);
    history.push("/home/healthStatus");
  }

  return (
    <div className="page-container">
      <div className="content-wrap">
        {loading ? (
          "Loading..."
        ) : (
          <div>
            <div style={{ height: "94%" }} className="profileContainer">
              <div className="backbtn_block">
                <div onClick={OnClickBackBtn} className="backbtn_img">
                  <img src="/images/Black Back Button.png" alt="back btn" />
                </div>
                <div className="backbtn_txt">Profile</div>
                {roles.includes("HRBP") &&
                data.employeeDetails.empName !== empName ? (
                  <div onClick={OnClickCheckIn} className="backbtn_link">
                    <span className="check_edit_btn float_r">Check-in</span>
                  </div>
                ) : (
                  <div>{null}</div>
                )}
              </div>

              <div>
                <div className="pagebg_img">
                  <img
                    src="/images/profilebg.png"
                    width="100%"
                    height="140"
                    alt="Image"
                  />
                  <div className="profile_img">
                    <img src="/images/profile.png" alt="Image" />
                  </div>
                  <div className="profile_name">
                    {data.employeeDetails.empName}
                  </div>
                  <div className="profile_details1 profile_details_txt">
                    Account Name - {data.employeeDetails.accountName}
                  </div>
                  <div className="profile_details2 profile_details_txt">
                    Primary Contact Number :{" "}
                    {data.employeeDetails.primaryContactNo}
                  </div>
                </div>
              </div>

              <div className="no_margin16 pad-tb10">
                <div onClick={OnClickGeneralInfo} className="whitebox_btn">
                  <span>
                    General Info{" "}
                    <img
                      className="float_r"
                      src="/images/iconright.png"
                      alt="Image"
                    />
                  </span>
                </div>
                <div onClick={OnClickContactInfo} className="whitebox_btn">
                  <span>
                    Contact Info
                    <img
                      className="float_r"
                      src="/images/iconright.png"
                      alt="Image"
                    />
                  </span>
                </div>
                <div onClick={OnClickNotes} className="whitebox_btn">
                  <span>
                    Notes
                    <img
                      className="float_r"
                      src="/images/iconright.png"
                      alt="Image"
                    />
                  </span>
                </div>
              </div>
            </div>

            <div className="profileContainerWithoutPadding">
              <TwoNavBar
                item1="My Health"
                item2="My Family"
                SetItemValue={SetItemValue}
                itemCount={2}
              ></TwoNavBar>
            </div>

            {val === 0 ? (
              <div>
                <div className="profileContainer" style={{backgroundImage:"none"}}>
                  <Button children="Take Health Assessment"></Button>
                </div>
                <div className="profileContainerWithoutPadding" style={{backgroundImage:"none"}}>
                  <Wellness data={data}></Wellness>
                </div>
              </div>
            ) : (
              <div>{null}</div>
            )}

            {val === 1 ? (
              <div className="profileContainer pad-t50">
                <img
                  className="center"
                  src="/images/FamilyPic.svg"
                  alt="family"
                />

                <Button1 style={{borderRadius:"20px !important"}} onClick={()=>history.push("/family")} children="Add Members"></Button1>  
              </div>
            ) : (
              <div>{null}</div>
            )}
          </div>
        )}
      </div>
    </div>
  );
}

function BasicInfo(props) {
  const { data, loading } = props;

  return (
    <div
      style={{
        paddingLeft: "16px",
      }}
    >
      {loading ? (
        "Loading"
      ) : (
        <div>
          <h5
            style={{
              textAlign: "left",
              paddingTop: "5px",
              color: COLORS.DARK_TEXT_CLR,
              fontFamily: "Inter",
              fontStyle: "normal",
              fontSize: "13px",
              margin: "5px",
              opacity: 0.8,
            }}
          >
            Name
          </h5>

          <h5
            style={{
              textAlign: "left",
              paddingTop: "5px",
              color: COLORS.DARK_TEXT_CLR,
              fontFamily: "Inter",
              fontStyle: "normal",
              fontSize: "15px",
              margin: "5px",
            }}
          >
            {data.employeeDetails.empName}
          </h5>

          <h5
            style={{
              textAlign: "left",
              paddingTop: "5px",
              color: COLORS.DARK_TEXT_CLR,
              fontFamily: "Inter",
              fontStyle: "normal",
              fontSize: "13px",
              margin: "5px",
              opacity: 0.8,
            }}
          >
            EMP ID
          </h5>

          <h5
            style={{
              textAlign: "left",
              paddingTop: "5px",
              color: COLORS.DARK_TEXT_CLR,
              fontFamily: "Inter",
              fontStyle: "normal",
              fontSize: "15px",
              margin: "5px",
            }}
          >
            {data.employeeDetails.empId}
          </h5>

          <h5
            style={{
              textAlign: "left",
              paddingTop: "5px",
              color: COLORS.DARK_TEXT_CLR,
              fontFamily: "Inter",
              fontStyle: "normal",
              fontSize: "13px",
              margin: "5px",
              opacity: 0.8,
            }}
          >
            Email Address
          </h5>

          <h5
            style={{
              textAlign: "left",
              paddingTop: "5px",
              color: COLORS.DARK_TEXT_CLR,
              fontFamily: "Inter",
              fontStyle: "normal",
              fontSize: "15px",
              margin: "5px",
            }}
          >
            {data.employeeDetails.emailAddr}
          </h5>

          <h5
            style={{
              textAlign: "left",
              paddingTop: "5px",
              color: COLORS.DARK_TEXT_CLR,
              fontFamily: "Inter",
              fontStyle: "normal",
              fontSize: "13px",
              margin: "5px",
              opacity: 0.8,
            }}
          >
            HRBP Name
          </h5>

          <h5
            style={{
              textAlign: "left",
              paddingTop: "5px",
              color: COLORS.DARK_TEXT_CLR,
              fontFamily: "Inter",
              fontStyle: "normal",
              fontSize: "15px",
              margin: "5px",
            }}
          >
            {data.employeeDetails.hrbpName}
          </h5>

          <div style={{ display: "flex", flexDirection: "row" }}>
            <div style={{ marginRight: 50 }}>
              <h5
                style={{
                  textAlign: "left",
                  paddingTop: "5px",
                  color: COLORS.DARK_TEXT_CLR,
                  fontFamily: "Inter",
                  fontStyle: "normal",
                  fontSize: "13px",
                  margin: "5px",
                  opacity: 0.8,
                }}
              >
                Base Location
              </h5>
              <h5
                style={{
                  textAlign: "left",
                  paddingTop: "5px",
                  color: COLORS.DARK_TEXT_CLR,
                  fontFamily: "Inter",
                  fontStyle: "normal",
                  fontSize: "15px",
                  margin: "5px",
                }}
              >
                {data.employeeDetails.workLocation}
              </h5>
            </div>

            <div>
              <h5
                style={{
                  textAlign: "left",
                  paddingTop: "5px",
                  color: COLORS.DARK_TEXT_CLR,
                  fontFamily: "Inter",
                  fontStyle: "normal",
                  fontSize: "13px",
                  margin: "5px",
                  opacity: 0.8,
                }}
              >
                DOB
              </h5>
              <h5
                style={{
                  textAlign: "left",
                  paddingTop: "5px",
                  color: COLORS.DARK_TEXT_CLR,
                  fontFamily: "Inter",
                  fontStyle: "normal",
                  fontSize: "15px",
                  margin: "5px",
                }}
              >
                {data.employeeDetails.dob}
              </h5>
            </div>
          </div>

          <div style={{ display: "flex", flexDirection: "row" }}>
            <div style={{ marginRight: 50 }}>
              <h5
                style={{
                  textAlign: "left",
                  paddingTop: "5px",
                  color: COLORS.DARK_TEXT_CLR,
                  fontFamily: "Inter",
                  fontStyle: "normal",
                  fontSize: "13px",
                  margin: "5px",
                  opacity: 0.8,
                }}
              >
                Account Name
              </h5>

              <h5
                style={{
                  textAlign: "left",
                  paddingTop: "5px",
                  color: COLORS.DARK_TEXT_CLR,
                  fontFamily: "Inter",
                  fontStyle: "normal",
                  fontSize: "15px",
                  margin: "5px",
                }}
              >
                {data.employeeDetails.accountName}
              </h5>
            </div>

            <div>
              <h5
                style={{
                  textAlign: "left",
                  paddingTop: "5px",
                  color: COLORS.DARK_TEXT_CLR,
                  fontFamily: "Inter",
                  fontStyle: "normal",
                  fontSize: "13px",
                  margin: "5px",
                  opacity: 0.8,
                }}
              >
                Gender
              </h5>

              <h5
                style={{
                  textAlign: "left",
                  paddingTop: "5px",
                  color: COLORS.DARK_TEXT_CLR,
                  fontFamily: "Inter",
                  fontStyle: "normal",
                  fontSize: "15px",
                  margin: "5px",
                }}
              >
                {data.employeeDetails.gender}
              </h5>
            </div>
          </div>

          <h5
            style={{
              textAlign: "left",
              paddingTop: "5px",
              color: COLORS.DARK_TEXT_CLR,
              fontFamily: "Inter",
              fontStyle: "normal",
              fontSize: "13px",
              margin: "5px",
              opacity: 0.8,
            }}
          >
            Current Location
          </h5>

          <h5
            style={{
              textAlign: "left",
              paddingTop: "5px",
              color: COLORS.DARK_TEXT_CLR,
              fontFamily: "Inter",
              fontStyle: "normal",
              fontSize: "15px",
              margin: "5px",
            }}
          >
            {data.employeeDetails.currentLocation}
          </h5>

          <h5
            style={{
              textAlign: "left",
              paddingTop: "5px",
              color: COLORS.DARK_TEXT_CLR,
              fontFamily: "Inter",
              fontStyle: "normal",
              fontSize: "13px",
              margin: "5px",
              opacity: 0.8,
            }}
          >
            Primary Contact Number
          </h5>

          <h5
            style={{
              textAlign: "left",
              paddingTop: "5px",
              color: COLORS.DARK_TEXT_CLR,
              fontFamily: "Inter",
              fontStyle: "normal",
              fontSize: "15px",
              margin: "5px",
            }}
          >
            {data.employeeDetails.primaryContactNo}
          </h5>

          <h5
            style={{
              textAlign: "left",
              paddingTop: "5px",
              color: COLORS.DARK_TEXT_CLR,
              fontFamily: "Inter",
              fontStyle: "normal",
              fontSize: "13px",
              margin: "5px",
              opacity: 0.8,
            }}
          >
            Alternate Contact Number
          </h5>

          <h5
            style={{
              textAlign: "left",
              paddingTop: "5px",
              color: COLORS.DARK_TEXT_CLR,
              fontFamily: "Inter",
              fontStyle: "normal",
              fontSize: "15px",
              margin: "5px",
            }}
          >
            {data.employeeDetails.alternativeContactNo1}
          </h5>

          <h5
            style={{
              textAlign: "left",
              paddingTop: "5px",
              color: COLORS.DARK_TEXT_CLR,
              fontFamily: "Inter",
              fontStyle: "normal",
              fontSize: "13px",
              margin: "5px",
              opacity: 0.8,
            }}
          >
            Alternate Contact Number 2
          </h5>

          <h5
            style={{
              textAlign: "left",
              paddingTop: "5px",
              color: COLORS.DARK_TEXT_CLR,
              fontFamily: "Inter",
              fontStyle: "normal",
              fontSize: "15px",
              margin: "5px",
            }}
          >
            {data.employeeDetails.alternativeContactNo2}
          </h5>

          <h5
            style={{
              textAlign: "left",
              paddingTop: "5px",
              color: COLORS.DARK_TEXT_CLR,
              fontFamily: "Inter",
              fontStyle: "normal",
              fontSize: "13px",
              margin: "5px",
              opacity: 0.8,
            }}
          >
            Emergency Contact Name{" "}
          </h5>

          <h5
            style={{
              textAlign: "left",
              paddingTop: "5px",
              color: COLORS.DARK_TEXT_CLR,
              fontFamily: "Inter",
              fontStyle: "normal",
              fontSize: "15px",
              margin: "5px",
            }}
          >
            {data.employeeDetails.emergencyContactName}
          </h5>

          <h5
            style={{
              textAlign: "left",
              paddingTop: "5px",
              color: COLORS.DARK_TEXT_CLR,
              fontFamily: "Inter",
              fontStyle: "normal",
              fontSize: "13px",
              margin: "5px",
              opacity: 0.8,
            }}
          >
            Emergency Contact Relation
          </h5>

          <h5
            style={{
              textAlign: "left",
              paddingTop: "5px",
              color: COLORS.DARK_TEXT_CLR,
              fontFamily: "Inter",
              fontStyle: "normal",
              fontSize: "15px",
              margin: "5px",
            }}
          >
            {data.employeeDetails.emergencyContactRelation}
          </h5>

          <h5
            style={{
              textAlign: "left",
              paddingTop: "5px",
              color: COLORS.DARK_TEXT_CLR,
              fontFamily: "Inter",
              fontStyle: "normal",
              fontSize: "13px",
              margin: "5px",
              opacity: 0.8,
            }}
          >
            Primary Contact Number
          </h5>

          <h5
            style={{
              textAlign: "left",
              paddingTop: "5px",
              color: COLORS.DARK_TEXT_CLR,
              fontFamily: "Inter",
              fontStyle: "normal",
              fontSize: "15px",
              margin: "5px",
            }}
          >
            {data.employeeDetails.primaryContactNo}
          </h5>
        </div>
      )}
    </div>
  );
}

function Wellness(props) {
  const { data } = props;
  const empName = useSelector((state) => state.empName);

  const dispatch = useDispatch();
  const history = useHistory();

  const OnClickProfile = (data) => {
    dispatch(SetDashboardSummary(data));
    history.push("/dashboardSummary");
  };

  return (
    <div>
      <h3 style={{ paddingLeft: "16px", paddingBottom: "20px" }}>
        Health Assessment History
      </h3>
      {console.log(data)}
      {data ? (
        data.responses.length === 0 ? (
          <div style={{ textAlign: "center" }}> No data to show </div>
        ) : (
          data.responses.map((el, id) => (
            <HistoryCard
              key={id}
              status={el.Status}
              userName={
                "Report by " +
                (el.type === "SELF" || el.type === null
                  ? data.employeeDetails.empName
                  : el.hrbpName)
              }
              report={
                el.type === "SELF" || el.type === null
                  ? "Last Reported on " +
                    moment(new Date(el.date)).format("DD/MM/YYYY HH:mm")
                  : "Last Checked on " +
                    moment(new Date(el.date)).format("DD/MM/YYYY HH:mm")
              }
              onPress={() => OnClickProfile(el)}
              CheckInShow={false}
            ></HistoryCard>
          ))
        )
      ) : (
        "No data to show"
      )}
    </div>
  );
}

function FamilyContainer(props) {}
