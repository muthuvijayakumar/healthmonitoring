import React, { useState, useEffect } from "react";
import { useHistory } from "react-router";
import "./css/BasicInformationScreen.css";
import { useSelector, useDispatch } from "react-redux";
import Footer from "../components/Footer";
import API from "../utils/api/API";
import YouTube from "react-youtube";
import { TopNavBar } from "../components/TopNavBar";
import { HistoryCard } from "../components/HistoryCard";
import { Card } from "../components/card-component.jsx";
import {
  SetDashboardUID,
  SetAssessmentType,
  SetOtherEmpDetail,
} from "../redux/action";
import Modal from "react-modal";
Modal.setAppElement("#root");
import { STRING, IMAGES, COLORS } from "../utils/constants";
import ToggleSwitch from "../components/ToggleSwitch";

import moment from "moment";
import { Button } from "../components/Button";

export default function BasicInformationScreen() {
  const [data, setData] = useState();
  const [modalOpen, setmodalOpen] = useState(false);
  const [notifyModalOpen, setNotifyModalOpen] = useState(false);
  const [dataHealthStatus, setDataHealthStatus] = useState(false);
  const [dataHealthReport, setDataHealthReport] = useState(false);
  const [currentLoc, setCurrentLoc] = useState("");
  const [altPhnNo1, setAltPhnNo1] = useState("");
  const [altPhnNo2, setAltPhnNo2] = useState("");
  const [emergencyConName, setEmergencyConName] = useState("");
  const [emergencyConNo, setEmergencyConNo] = useState("");
  const [emergencyConRelation, setEmergencyConRelation] = useState("");
  const [PrimaryConNo, setPrimaryConNo] = useState("");
  const [isEditBtnClicked, setIsEditBtnClicked] = useState(false);
  const [loading, setLoading] = useState(true);
  const dispatch = useDispatch();
  const [recentSurveyDate, setRecentSurveyDate] = useState();
  const [err, setERR] = useState("");

  const history = useHistory();

  const empID = useSelector((state) => state.empID);

  const Id = useSelector((state) => state.uniqueID);
  const _Id = useSelector((state) => state._uniqueID);
  const empMail = useSelector((state) => state.empMail);
  const empACName = useSelector((state) => state.empAccountName);
  const empGender = useSelector((state) => state.gender);
  const empdob = useSelector((state) => state.dob);
  const empHrbpName = useSelector((state) => state.hrbpName);
  const empWorkLocation = useSelector((state) => state.workLocation);
  const isLocationUpdated = useSelector((state) => state.isLocationUpdated);

  const empName = useSelector((state) => state.empName);
  const jwt = useSelector((state) => state.jwt);
  const roles = useSelector((state) => state.roles);
  const initialEmpData = useSelector((state) => state.initalEmpData);

  const customStyles = {
    content: {
      top: "95%",
      left: "50%",
      right: "auto",
      bottom: "-40%",
      transform: "translate(-50%, -50%)",
      borderRadius: "10px",
      width: "82%",
    },
  };

  const NotificationModalStyle = {
    content: {
      top: "60%",
      left: "50%",
      right: "auto",
      bottom: "-40%",
      transform: "translate(-50%, -50%)",
      borderRadius: "10px",
      width: "82%",
    },
  };

  function getLocation(val) {
    setCurrentLoc(val.target.value);
  }

  function getAltPhnNo(val) {
    const re = /^[0-9\b]+$/;
    if (val.target.value === "" || re.test(val.target.value)) {
      setAltPhnNo1(val.target.value);
    }
  }
  function getAltPhnNo2(val) {
    const re = /^[0-9\b]+$/;
    if (val.target.value === "" || re.test(val.target.value)) {
      setAltPhnNo2(val.target.value);
    }
  }
  function getEmergencyConName(val) {
    setEmergencyConName(val.target.value);
  }
  function getEmergencyConNo(val) {
    const re = /^[0-9\b]+$/;
    if (val.target.value === "" || re.test(val.target.value)) {
      setEmergencyConNo(val.target.value);
    }
  }
  function getEmergencyConRelation(val) {
    setEmergencyConRelation(val.target.value);
  }
  function getPrimaryConNo(val) {
    const re = /^[0-9\b]+$/;
    if (val.target.value === "" || re.test(val.target.value)) {
      setPrimaryConNo(val.target.value);
    }
  }

  function onFocus() {
    setERR("");
  }

  const [val, setVal] = useState(0);

  function SetItemValue(value) {
    setVal(value);
    console.log(value);
  }
  function onNextClicked() {
    dispatch(SetAssessmentType("SELF"));
    history.push("/home/healthStatus");
  }

  function OnClickBackBtn() {
    history.push("/home/dashboard");
  }

  function OnClickHelpDesk() {
    // console.log("Help desk");
    history.push("/home/helpdesk");
    amplitude.getInstance().logEvent("COVID-19 Heldesk");
  }
  function OnClickPolicy() {
    //console.log("Policy");

    history.push("/home/CoronaKavachPolicy");
    amplitude.getInstance().logEvent("Corona Kawach Policy");
  }
  function OnClickRtcpr() {
    history.push("/home/rtpcrTesting");
    amplitude.getInstance().logEvent("RT-PCR Testing Support");
  }
  function OnClickInsurance() {
    history.push("/home/generalInsurance");
    amplitude.getInstance().logEvent("Health Insurance Policy");
  }

  function OnClickProtocol() {
    history.push("/home/protocol");
    amplitude.getInstance().logEvent("Protocols");
  }

  function OnClickOxygen() {
    history.push("/home/oxygen");
    amplitude.getInstance().logEvent("Oxygen Concentrators")
  }

  const OnClickProfile = (id) => {
    dispatch(SetDashboardUID(id));
    history.push("/dashboardUserProfile");
  };

  function get_time_difference(recentDate) {
    try {
      var serverDate = new Date(recentDate);
      var localDate = new Date();

      var diff = localDate.getTime() - serverDate.getTime();

      var msec = diff;
      var hh = Math.floor(msec / 1000 / 60 / 60);
      msec -= hh * 1000 * 60 * 60;
      var mm = Math.floor(msec / 1000 / 60);
      msec -= mm * 1000 * 60;
      var ss = Math.floor(msec / 1000);
      msec -= ss * 1000;

      return hh;
    } catch (e) {
      console.log(e);
    }
  }

  function CloseModal() {
    setmodalOpen(false);
  }

  useEffect(() => {
    console.log(jwt);
    if (jwt === "") {
      history.push("/");
    }

    setLoading(true);
    console.log(roles);

    async function fetchAPI() {
      if (roles.includes("HRBP")) {
        await API.get(process.env.REACT_APP_EMPLOYEE_HRBP_DASHBOARD + empID, {
          headers: {
            Authorization: `Bearer ${jwt}`,
          },
        })
          .then((res) => {
            setData(res.data);
            console.log(res.data);
          })
          .catch((err) => {
            switch (err.response.status) {
              case 500:
                break;
              case 400:
                break;

              case 401:
                history.push("/");

                break;
              case 403:
                history.push("/");
                break;

              default:
                break;
            }
            console.log(err);

            throw err;
          });
      }

      await API.get(
        process.env.REACT_APP_EMPLOYEE_DASHBOARD_SURVEY_EMPLOYEE_ID + empID,
        {
          headers: {
            Authorization: `Bearer ${jwt}`,
          },
        }
      )
        .then((res) => {
          setRecentSurveyDate(res.data.recentSurveyDate);
          console.log(res.data.recentSurveyDate);
          setLoading(false);
        })
        .catch((err) => {
          switch (err.response.status) {
            case 500:
              break;
            case 400:
              break;

            case 401:
              history.push("/");

              break;
            case 403:
              history.push("/");
              break;

            default:
              break;
          }
          console.log(err);

          throw err;
        });
    }
    fetchAPI();
    return () => {
      setLoading(true);
    };
  }, []);
  const opts = {
    playerVars: {
      // https://developers.google.com/youtube/player_parameters
      autoplay: 0,
    },
  };
  return (
    <div className="page-container">
      {loading
        ? "Loading ... "
        : (console.log(loading),
          (
            <div className="content-wrap">
              <div
                style={{ height: "94%", padding: "0px" }}
                className="HomeContainer"
              >
                <div className="homepagebg_img">
                  <img
                    src="/images/hometopbg.png"
                    width="100%"
                    height="330px"
                    alt="Image"
                  />
                  <div className="homepg_bg_txt1">
                    <span
                      style={{
                        display: "block",
                        float: "left",
                        cursor:"pointer",
                      }}
                    >
                      <img
                        onClick={() => OnClickProfile(empID)}
                        src="/images/profile.png"
                        alt="Image"
                      />
                    </span>
                    <span className="homepg_bg_head">
                      Fight against COVID-19
                    </span>
                    <span className="float_r" style={{cursor:"pointer", padding: "5px 0px" }}>
                      <img
                        onClick={() => setmodalOpen(true)}
                        src="/images/help.svg"
                        alt="Image"
                      />
                    </span>
                  </div>

                  <div className="homepg_bg_empname">Hello {empName},</div>

                  <div className="homepg_bg_txt2">
                    <div className="greenbg_box greenbox">
                      <div>
                        <img src="/images/info.svg"></img>
                      </div>
                      {recentSurveyDate === null ? (
                        <div className="pinkbg_box pad-tb5">
                          Take your first health assessment now
                        </div>
                      ) : (
                        <div>
                          {get_time_difference(recentSurveyDate) < 24 ? (
                            <div className="greenbox_cnt pad-tb5">
                              Your next health assessment is due in{" "}
                              {24 - get_time_difference(recentSurveyDate)} hrs
                            </div>
                          ) : (
                            <div>{null}</div>
                          )}

                          {get_time_difference(recentSurveyDate) > 24 &&
                          get_time_difference(recentSurveyDate) < 48 ? (
                            <div className="orangebg_box pad-tb5">
                              Your health assessment was due before{" "}
                              {get_time_difference(recentSurveyDate) - 24} hrs
                            </div>
                          ) : (
                            <div>{null}</div>
                          )}

                          {get_time_difference(recentSurveyDate) > 48 ? (
                            <div className="pinkbg_box pad-tb5">
                              Your health assessment was due before{" "}
                              {Math.round(
                                get_time_difference(recentSurveyDate) / 24
                              )}{" "}
                              days
                            </div>
                          ) : (
                            <div>{null}</div>
                          )}
                        </div>
                      )}
                    </div>
                  </div>

                  {recentSurveyDate === null ? (
                    <div className="homepg_bg_txt3">
                      <div>
                        <img src="/images/homebgimg1.png" alt="Image" />
                      </div>
                      <div className="homepg_bg_txt3cnt">
                        <div className="homepg_bg_txt3cnt1">
                          Are you safe? We are eager to know!
                        </div>
                        <div className="homepg_bg_txt3cnt2 pad-tb5">
                          It’s time to take your first health assessment{" "}
                        </div>
                        <div style={{cursor:"pointer"}}
                          onClick={onNextClicked}
                          className="homepg_bg_txt3cnt3"
                        >
                          Health Assessment
                        </div>
                      </div>
                    </div>
                  ) : (
                    <div>
                      {get_time_difference(recentSurveyDate) < 24 ? (
                        <div className="homepg_bg_txt3">
                          <div>
                            <img src="/images/homebgimg1.png" alt="Image" />
                          </div>
                          <div className="homepg_bg_txt3cnt">
                            <div className="homepg_bg_txt3cnt1">
                              You look safe from COVID-19!
                            </div>
                            <div className="homepg_bg_txt3cnt2 pad-tb5">
                              Thank you for taking the health assessment on
                              time!{" "}
                            </div>
                            <div
                              onClick={onNextClicked}
                              className="homepg_bg_txt3cnt3"
                            >
                              Health Assessment
                            </div>
                          </div>
                        </div>
                      ) : (
                        <div>{null}</div>
                      )}

                      {get_time_difference(recentSurveyDate) >= 24 &&
                      get_time_difference(recentSurveyDate) < 48 ? (
                        <div className="homepg_bg_txt3">
                          <div>
                            <img src="/images/homebgimg1.png" alt="Image" />
                          </div>
                          <div className="homepg_bg_txt3cnt">
                            <div className="homepg_bg_txt3cnt1">
                              It's time to re-assess your health situation!
                            </div>
                            <div className="homepg_bg_txt3cnt2 pad-tb5">
                              A regular health assessment helps you to fight
                              COVID-19.{" "}
                            </div>

                            <div
                              onClick={onNextClicked}
                              className="homepg_bg_txt3cnt3"
                            >
                              Health Assessment
                            </div>
                          </div>
                        </div>
                      ) : (
                        <div>{null}</div>
                      )}

                      {get_time_difference(recentSurveyDate) >= 48 ? (
                        <div className="homepg_bg_txt3">
                          <div>
                            <img src="/images/homebgimg1.png" alt="Image" />
                          </div>
                          <div className="homepg_bg_txt3cnt">
                            <div className="homepg_bg_txt3cnt1">
                              Are you safe? We are eager to know!
                            </div>
                            <div className="homepg_bg_txt3cnt2 pad-tb5">
                              It's been a while since you took your last health
                              assessment.{" "}
                            </div>
                            <div
                              onClick={onNextClicked}
                              className="homepg_bg_txt3cnt3"
                            >
                              Health Assessment
                            </div>
                          </div>
                        </div>
                      ) : (
                        <div>{null}</div>
                      )}
                    </div>
                  )}
                </div>
              </div>

              <div style={{ height: "94%" }} className="HomeContainer">
                {roles.includes("DASHBOARD") ? (
                  <div id="h_clm_2">
                    <div className="pad-b10">
                      <span className="careassistance_head">
                        Management Dashboard
                      </span>
                      <span className="float_r">
                        <a
                          onClick={() => {
                            history.push("/dashboard");
                          }}
                          className="view_more_txtlink"
                          href="#"
                        >
                          View More
                        </a>
                      </span>
                    </div>

                    <ToggleSwitch 
                      checked = {dataHealthStatus}
                      onChange = {(checked) => setDataHealthStatus(checked) }
                    />

                    <div className="homerow_tiles">
                      <Card
                        backgroundColor="#fac8db"
                        value={dataHealthStatus ? 0 : initialEmpData.healthreportoverview.hospitalized}
                        label="Hospitalized"
                      ></Card>
                      <Card
                        backgroundColor="#f7e3c5"
                        value={dataHealthStatus ? 0 : initialEmpData.healthreportoverview.quarantined}
                        label="Home Care"
                      ></Card>
                      <Card
                        backgroundColor="#c9eacc"
                        value={dataHealthStatus ? 0 : initialEmpData.healthreportoverview.healthy}
                        label="Healthy"
                      ></Card>
                    </div>
                  </div>
                ) : (
                  <div>{null}</div>
                )}
              </div>

              {roles.includes("HRBP") ? (
                <>
                <div className="HomeContainer" style={{ height: "94%" }}>
                <span className="careassistance_head">
                Employee Health Status
                </span>
                <span className="float_r">
                  <a
                    onClick={() => {
                      history.push("/dashboardOverview");
                    }}
                    className="view_more_txtlink"
                    href="#"
                  >
                    View More
                  </a>
                </span>
              </div>
                <div className="HomeContainerWithoutPadding">
                 

                  <div className="HomeContainerWithoutPadding">
                  <div style={{ backgroundColor: "white" }}  >
                  <ToggleSwitch 
                    checked = {dataHealthReport}
                    onChange = {(checked) => setDataHealthReport(checked) }/>
                    <div style={{paddingTop:"10px"}}>
                    <TopNavBar
                      item1="Hospitalized"
                      item2="Home Care"
                      item3="Healthy"
                      SetItemValue={SetItemValue}
                    ></TopNavBar>

                    <Hospitalized
                      value={val}
                      index={0}
                      data={dataHealthReport ? null : data.recenthospitalizedUpdates}
                    ></Hospitalized>
                    <HomeCare
                      value={val}
                      index={1}
                      data={dataHealthReport ? null : data.recentQuarantinedUpdates}
                    ></HomeCare>
                    <Healthy
                      value={val}
                      index={2}
                      data={dataHealthReport ? null : data.recentSafeUpdates}
                    ></Healthy>
                  </div>
                  </div>
                  </div>
                </div>
                </>
              ) : (
                <div>{null}</div>
              )}

              <div className="HomeContainer">
                <div onClick={() => OnClickProtocol()} style={{cursor:"pointer"}} className="pagebg_img">
                  <div className="bg_img_responsive80" style={{backgroundImage: 'url(/images/homeprotocolbg.png)'}}>
                    <div className="bg_inner_img80" style={{backgroundImage: 'url(/images/homeprotocolinner.svg)'}}></div>
                  </div>

                  <div className="Protocols_cnt1">Protocols</div>
                  <div className="Protocols_cnt2">
                    (Recommended by doctors)
                  </div>
                  <div className="Protocols_cnt3">
                    Stages,Symptoms and Preventive Protocols
                  </div>
                </div>
                
                <div onClick={() => OnClickOxygen()} style={{cursor:"pointer"}} className="pagebg_img">
                  <div className="bg_img_responsive80" style={{backgroundImage: 'url(/images/homeoxygenbg.png)'}}>
                    <div className="bg_inner_img80" style={{backgroundSize:"260px 75px",backgroundImage: 'url(/images/homeoxygeninner.svg)'}}></div>
                  </div>
 
                  <div className="Protocols_cnt1">Oxygen Concentrators</div>
                  <div className="Protocols_cnt3">
                  Available in Bengaluru, Chennai, Hyderabad and Pune
                  </div>
                 </div>

                 <div id="h_clm_4">
                  <div className="careassistance_head pad-t10">
                    Care & Assistance
                  </div>

                  <div className="homerow">
                    <div
                      onClick={() => OnClickHelpDesk()}
                      className="homecolumn float_l"
                    >
                      <div className="careassistance_page_img">
                        <img
                          src="/images/careassistance2.png"
                          width="100%"
                          height="auto"
                          alt="Image"
                        />
                        <div className="careassistance_top-left careassistance">
                        Helpdesk & Doctor On- Call
                        </div>
                      </div>
                    </div>
                    <div
                      onClick={() => OnClickRtcpr()}
                      className="homecolumn float_r"
                    >
                      <div className="careassistance_page_img">
                        <img
                          src="/images/careassistance4.png"
                          width="100%"
                          height="auto"
                          alt="Image"
                        />
                        <div className="careassistance_top-left careassistance">
                          RAT & RT-PCR Testing Facility
                        </div>
                      </div>
                    </div>
                  </div>

                  <div className="homerow">
                  <div
                      onClick={() => OnClickPolicy()}
                      className="homecolumn float_l"
                    >
                      <div className="careassistance_page_img">
                        <img
                          src="/images/CoronaKavach.svg"
                          width="100%"
                          height="auto"
                          alt="Image"
                        />
                        <div className="careassistance_top-left careassistance">
                          Corona Kavach Insurance Policy
                        </div>
                      </div>
                    </div>
                    <div
                      onClick={() => OnClickInsurance()}
                      className="homecolumn float_r"
                    >
                      <div className="careassistance_page_img">
                        <img
                          src="/images/careassistance3.png"
                          width="100%"
                          height="auto"
                          alt="Image"
                        />
                        <div className="careassistance_top-left careassistance">
                        Health Insurance Policy
                        </div>
                      </div>
                    </div>
                    
                  </div>
                </div>

                <div id="h_clm_5">
                  <div className="yvideo_head">
                    Simple Yoga for a better life During COVID19
                  </div>
                  <ul className="y_videos_container example">
                    <li>
                      <YouTube videoId="Opw9G1qKCcM" opts={opts} />
                    </li>
                    <li>
                      <YouTube videoId="q5m6tMjcF8k" opts={opts} />
                    </li>
                    <li>
                      <YouTube videoId="ERDVCeHP3vg" opts={opts} />
                    </li>
                  </ul>
                </div>

                <Modal
                  isOpen={modalOpen}
                  //onAfterOpen={afterOpenModal}
                  onRequestClose={CloseModal}
                  style={customStyles}
                  shouldCloseOnOverlayClick={true}
                >
                  <div
                    onClick={() => setmodalOpen(false)}
                    className="whitelistbox_ctnt3"
                  >
                    <span className="check_edit_btn float_r">close</span>
                  </div>
                  <div>
                    <div className="info_popup_txt pad-t10">
                      If you encounter any difficulties while using this app,
                      please write to us
                    </div>

                    <div className="pad-t20">
                      <span className="change_button" style={{ width: "100%" }}>
                        <img
                          style={{ verticalAlign: "middle", padding: "1px" }}
                          src="./images/mail.svg"
                        ></img>{" "}
                        <a href="mailto:dex@altimetrik.com?subject=Support%20Request">
                          {" "}
                          dex@altimetrik.com{" "}
                        </a>
                      </span>
                    </div>
                  </div>
                </Modal>

                <Modal
                  isOpen={notifyModalOpen}
                  //onAfterOpen={afterOpenModal}
                  onRequestClose={CloseModal}
                  style={NotificationModalStyle}
                  shouldCloseOnOverlayClick={true}
                >
                  <div id="contactInfo">
                    <div className="backbtn_block">
                      <div className="backbtn_txt" style={{padding:"0px"}}>
                        Update your Contact Info
                      </div>
                      <div onClick={OnClickBackBtn} className="float_r">
                        <img src="/images/CloseBtn.svg" alt="back btn" />
                      </div>
                    </div>

                    <div
                    style={{
                      margin:"10px 0px",
                      height: "0px",
                      border: "0.5px solid rgba(0, 0, 0, 0.1)",
                      marginLeft: "-16px",
                      marginRight: "-16px",
                    }}>
                    </div>

                    <div className="contactInfo_body">
                      <div className="popup-contact pad-t10">
                        Current Location{" "}
                        <span style={{ color: COLORS.ERR_CLR }}>*</span>
                      </div>

                      <input
                        className="head_content head_content_edit"
                        style={{
                          borderWidth: 1,
                          borderRadius: 3,
                          fontSize: "15px",
                          backgroundColor: COLORS.WHITE,
                          paddingLeft: "15px",
                          width: "98%",
                          height: "30px",
                        }}
                        disabled={!isEditBtnClicked}
                        onFocus={onFocus}
                        value={currentLoc}
                        placeholder="Enter Current Location"
                        type="text"
                        onChange={getLocation}
                      ></input>

                      <div className="popup-contact pad-t10">
                        Primary Contact Number{" "}
                        <span style={{ color: COLORS.ERR_CLR }}>*</span>
                      </div>

                      <input
                        className="head_content head_content_edit"
                        style={{
                          borderWidth: 1,
                          borderRadius: 3,
                          fontSize: "15px",
                          backgroundColor: COLORS.WHITE,
                          paddingLeft: "15px",
                          width: "98%",
                          height: "30px",
                        }}
                        disabled={!isEditBtnClicked}
                        onFocus={onFocus}
                        value={PrimaryConNo}
                        placeholder="Enter Primary Contact Number"
                        type="text"
                        onChange={getPrimaryConNo}
                      ></input>

                      <div className="popup-contact pad-t10">
                        Alternate Contact Number
                      </div>

                      <input
                        className="head_content head_content_edit"
                        style={{
                          borderWidth: 1,
                          borderRadius: 3,
                          fontSize: "15px",
                          backgroundColor: COLORS.WHITE,
                          paddingLeft: "15px",
                          width: "98%",
                          height: "30px",
                        }}
                        disabled={!isEditBtnClicked}
                        onFocus={onFocus}
                        value={altPhnNo1}
                        placeholder="Enter Alternate Contact Number"
                        type="text"
                        onChange={getAltPhnNo}
                      ></input>

                      <div className="popup-contact pad-t10">
                        Alternate Contact Number 2
                      </div>
                      <input
                        className="head_content head_content_edit"
                        style={{
                          borderWidth: 1,
                          borderRadius: 3,

                          fontSize: "15px",
                          backgroundColor: COLORS.WHITE,
                          paddingLeft: "15px",
                          width: "98%",
                          height: "30px",
                        }}
                        disabled={!isEditBtnClicked}
                        onFocus={onFocus}
                        value={altPhnNo2}
                        placeholder="Enter Alternate Contact Number"
                        type="text"
                        onChange={getAltPhnNo2}
                      ></input>
                      <div className="popup-contact pad-t10">
                        Emergency Contact Name{" "}
                        <span style={{ color: COLORS.ERR_CLR }}>*</span>
                      </div>
                      <input
                        className="head_content head_content_edit"
                        style={{
                          borderWidth: 1,
                          borderRadius: 3,
                          fontSize: "15px",
                          backgroundColor: COLORS.WHITE,
                          paddingLeft: "15px",
                          width: "98%",
                          height: "30px",
                        }}
                        disabled={!isEditBtnClicked}
                        onFocus={onFocus}
                        value={emergencyConName}
                        placeholder="Enter Emergency Contact Name"
                        type="text"
                        onChange={getEmergencyConName}
                      ></input>
                      <div className="popup-contact pad-t10">
                        Emergency Contact Relation{" "}
                        <span style={{ color: COLORS.ERR_CLR }}>*</span>
                      </div>
                      <input
                        className="head_content head_content_edit"
                        style={{
                          borderWidth: 1,
                          borderRadius: 3,
                          fontSize: "15px",
                          backgroundColor: COLORS.WHITE,
                          paddingLeft: "15px",
                          width: "98%",
                          height: "30px",
                        }}
                        disabled={!isEditBtnClicked}
                        onFocus={onFocus}
                        placeholder="Enter Emergency Contact Relation"
                        type="text"
                        value={emergencyConRelation}
                        onChange={getEmergencyConRelation}
                      ></input>

                      <div className="popup-contact pad-t10">
                        Emergency Contact Number{" "}
                        <span style={{ color: COLORS.ERR_CLR }}>*</span>
                      </div>
                      <input
                        className="head_content head_content_edit"
                        style={{
                          borderWidth: 1,
                          borderRadius: 3,
                          fontSize: "15px",
                          backgroundColor: COLORS.WHITE,
                          paddingLeft: "15px",
                          width: "98%",
                          height: "30px",
                        }}
                        disabled={!isEditBtnClicked}
                        onFocus={onFocus}
                        value={emergencyConNo}
                        placeholder="Enter Emergency Contact Number"
                        type="text"
                        onChange={getEmergencyConNo}
                      ></input>
                    </div>

                    <h5
                      style={{
                        textAlign: "left",
                        paddingTop: "5px",
                        color: COLORS.ERR_CLR,
                        fontFamily: "Inter",
                        fontStyle: "normal",
                        fontSize: "12px",
                        margin: "5px",
                      }}
                    >
                      {err}
                    </h5>
                    <Button
                      onClick={() => console.log("On Click Save Button")}
                      children="Save"
                    ></Button>
                  </div>
                </Modal>
              </div>
            </div>
          ))}

      <Footer></Footer>
    </div>
  );
}

function Hospitalized(props) {
  const { data, value, index } = props;
  const history = useHistory();
  const dispatch = useDispatch();

  function OnClickCheckIn(data) {
    dispatch(SetAssessmentType("OTHERS"));
    dispatch(SetOtherEmpDetail(data));
    console.log(data);
    history.push("/home/healthStatus");
  }
  const OnClickProfile = (id,data) => {
    dispatch(SetDashboardUID(id));
    dispatch(SetOtherEmpDetail(data));

    history.push("/dashboardUserProfile");
  };

  function OnClickViewAllBtn() {
    history.push("/home/hrbp_EmployeeSts");
  }

  return (
    <div>
      {value === index && (
        <div>
          {data
            ? data.map((el, id) => (
                <HistoryCard
                  key={id}
                  status={el.Status}
                  userName={el.empName}
                  report={
                    el.type === "SELF" || el.type === null
                      ? "Last Reported on " +
                        moment(new Date(el.date)).format("DD/MM/YYYY HH:mm")
                      : "Last Checked on " +
                        moment(new Date(el.date)).format("DD/MM/YYYY HH:mm")
                  }
                  CheckIn={() => OnClickCheckIn(el)}
                  onPress={() => OnClickProfile(el.id,el)}
                  CheckInShow={true}
                ></HistoryCard>
              ))
            : "No data to show"}

          <div style={{ backgroundColor: "white" }} className="empty_btn">
            <a
              onClick={() => {
                OnClickViewAllBtn();
              }}
              href="#"
            >
              View All
            </a>
          </div>
        </div>
      )}
    </div>
  );
}

function HomeCare(props) {
  const { data, value, index } = props;
  const dispatch = useDispatch();
  const history = useHistory();

  function OnClickCheckIn(data) {
    dispatch(SetAssessmentType("OTHERS"));
    dispatch(SetOtherEmpDetail(data));
    console.log(data);
    history.push("/home/healthStatus");
  }
  const OnClickProfile = (id,data) => {
    dispatch(SetDashboardUID(id));
    dispatch(SetOtherEmpDetail(data));

    history.push("/dashboardUserProfile");
  };

  function OnClickViewAllBtn() {
    history.push("/home/hrbp_EmployeeSts");
  }

  return (
    <div>
      {value === index && (
        <div>
          {data
            ? data.map((el, id) => (
                <HistoryCard
                  onPress={() => OnClickProfile(el.id,el)}
                  key={id}
                  status={el.Status}
                  userName={el.empName}
                  report={
                    el.type === "SELF" || el.type === null
                      ? "Last Reported on " +
                        moment(new Date(el.date)).format("DD/MM/YYYY HH:mm")
                      : "Last Checked on " +
                        moment(new Date(el.date)).format("DD/MM/YYYY HH:mm")
                  }
                  CheckIn={() => OnClickCheckIn(el)}
                  CheckInShow={true}
                ></HistoryCard>
              ))
            : "No data to show"}

          <div style={{ backgroundColor: "white" }} className="empty_btn">
            <a
              onClick={() => {
                OnClickViewAllBtn();
              }}
              href="#"
            >
              View All
            </a>
          </div>
        </div>
      )}
    </div>
  );
}

function Healthy(props) {
  const { data, value, index } = props;
  const history = useHistory();

  const dispatch = useDispatch();

  const OnClickProfile = (id,data) => {
    dispatch(SetDashboardUID(id));
    dispatch(SetOtherEmpDetail(data));

    history.push("/dashboardUserProfile");
  };

  function OnClickCheckIn(data) {
    dispatch(SetAssessmentType("OTHERS"));
    dispatch(SetOtherEmpDetail(data));
    console.log(data);
    history.push("/home/healthStatus");
  }

  function OnClickViewAllBtn() {
    history.push("/home/hrbp_EmployeeSts");
  }

  return (
    <div>
      {value === index && (
        <div>
          {data
            ? data.map((el, id) => (
                <HistoryCard
                  onPress={() => OnClickProfile(el.id,el)}
                  key={id}
                  status={el.Status}
                  userName={el.empName}
                  report={
                    el.type === "SELF" || el.type === null
                      ? "Last Reported on " +
                        moment(new Date(el.date)).format("DD/MM/YYYY HH:mm")
                      : "Last Checked on " +
                        moment(new Date(el.date)).format("DD/MM/YYYY HH:mm")
                  }
                  CheckIn={() => OnClickCheckIn(el)}
                  CheckInShow={true}
                ></HistoryCard>
              ))
            : "No data to show"}

          <div style={{ backgroundColor: "white" }} className="empty_btn">
            <a
              onClick={() => {
                OnClickViewAllBtn();
              }}
              href="#"
            >
              View All
            </a>
          </div>
        </div>
      )}
    </div>
  );
}