import React, { useEffect, useRef } from "react";

export default function TextTest() {
  const MAX_LENGTH = 25;

  const text =
    "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.";

  return (
    <div>
      {text.length > MAX_LENGTH ? (
        <div>{`${text.substring(0, MAX_LENGTH)}...`}</div>
      ) : (
        <p>{text}</p>
      )}
    </div>
  );
}
