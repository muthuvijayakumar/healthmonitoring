export const SetDetails = (
  EmpId,
  mail,
  name,
  Account,
  dob,
  gender,
  hrbpName,
  workLocation,
  id,
  _id,
  isLocationUpdated,
  roles,
  jwt,
  serverEmpData
) => {
  return {
    type: "empID",
    empID: EmpId,
    empMail: mail,
    empName: name,
    empAccountName: Account,
    dob: dob,
    gender: gender,
    hrbpName: hrbpName,
    workLocation: workLocation,
    uniqueId: id,
    _uniqueId: _id,
    isLocationUpdated: isLocationUpdated,
    jwt: jwt,
    roles: roles,
    initalEmpData: serverEmpData,
  };
};

export const SetQuizModuleName = (moduleName) => {
  return {
    type: "quiz",
    moduleName: moduleName,
  };
};

export const SetQuizCategory = (id) => {
  return {
    type: "quizCategory",
    categoryID: id,
  };
};

export const SetDashboardUID = (id) => {
  return {
    type: "setUId",
    dashboardUID: id,
  };
};

export const SetDashboardSummary = (data) => {
  return {
    type: "setDashboardSummary",
    dashboardSum: data,
  };
};

export const SetQuizSummary = (data) => {
  return {
    type: "setQuizSummary",
    quizSum: data,
  };
};

export const SetHealthAlert = (data, topic) => {
  return {
    type: "setHealthAlert",
    healthAlertsCategory: data,
    healthAlertsCategoryTopic: topic,
  };
};

export const SetAssessmentType = (data) => {
  return {
    type: "setAssessmentType",
    assessmentType: data,
  };
};

export const SetOtherEmpDetail = (data) => {
  return {
    type: "setOtherEmpDetail",
    otherEmpDetail: data,
  };
};
