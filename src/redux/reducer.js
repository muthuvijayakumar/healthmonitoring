const initialState = {
  empID: "0",
  empName: "Robert",
  empMail: "",
  quizModuleName: "",
  empAccountName: "",
  dob: "",
  gender: "",
  hrbpName: "",
  workLocation: "",
  uniqueID: "",
  _uniqueID: "",
  isLocationUpdated: false,
  categoryID: "",
  jwt: "",
  roles: "",
  dashboardUID: "",
  dashboardSum: [],
  quizSum: [],
  healthAlertsCategory: "",
  healthAlertsCategoryTopic: "",
  initalEmpData: [],
  assessmentType: "",
  otherEmpDetail: []
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case "empID":
      return {
        ...state,
        empID: action.empID,
        empName: action.empName,
        empMail: action.empMail,
        empAccountName: action.empAccountName,
        dob: action.dob,
        gender: action.gender,
        hrbpName: action.hrbpName,
        workLocation: action.workLocation,
        uniqueID: action.uniqueId,
        _uniqueID: action._uniqueId,
        isLocationUpdated: action.isLocationUpdated,
        jwt: action.jwt,
        roles: action.roles,
        initalEmpData: action.initalEmpData,
      };

    case "quiz":
      return { ...state, quizModuleName: action.moduleName };
    case "quizCategory":
      return { ...state, categoryID: action.categoryID };
    case "setUId":
      return { ...state, dashboardUID: action.dashboardUID };
    case "setDashboardSummary":
      return { ...state, dashboardSum: action.dashboardSum };
    case "setQuizSummary":
      return { ...state, quizSum: action.quizSum };
    case "setHealthAlert":
      return {
        ...state,
        healthAlertsCategory: action.healthAlertsCategory,
        healthAlertsCategoryTopic: action.healthAlertsCategoryTopic,
      };

    case "setAssessmentType":
      return { ...state, assessmentType: action.assessmentType };

    case "setOtherEmpDetail":
      return { ...state, otherEmpDetail: action.otherEmpDetail };
  }
  return state;
};

export default reducer;
